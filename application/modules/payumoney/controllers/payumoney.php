<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Payumoney class this is a sample code to integrate payumoney in codeigniter 3
*/
class Payumoney extends MX_Controller
{
	public function __construct(){
		parent::__construct();
		$this->load->helper(array('string', 'url'));
		$this->load->library('session');
		$this->load->library('user_agent');
		$this->load->model('services/mdl_general');
	}

	//Method to show payment form
	public function index(){
		$sess_pay_u_id = $this->session->userdata('sess_pay_u_id');
		$sess_pay_services_id = $this->session->userdata('sess_pay_services_id');
		// $this->load->view('pay_form');
		$main_title=$this->mdl_general->GetInfoByRow('gh_configuration','config_id',array('config_id'=>1));
    	$page_details['page_title']= $main_title->website_title.' | Payment';
        $this->load->Module('header')->index($page_details);
        $data['payment_list'] = $this->mdl_general->GetInfoByRow('payment_list','payment_id',array('services_id' => $sess_pay_services_id,'u_id'=>$sess_pay_u_id));
        $data['list'] = $this->mdl_general->GetInfoByRow('services','services_id',array('services_id' => $data['payment_list']->services_id,'status'=>1));
        $data['getUserLogDetails'] = $this->mdl_general->GetInfoByRow('gh_user','u_id',array('u_id' => $data['payment_list']->u_id));
        $data['action'] = '';
		$this->load->view('payment_form',$data);
		$this->load->Module('footer')->index();

	}

	public function success(){
		if(empty($_POST)){
			redirect('services/services_view');
		}

		$status=$this->input->post('status');
		$firstname=$this->input->post('firstname');
		$amount=$this->input->post('amount');
		$txnid=$this->input->post('txnid');
		$posted_hash=$this->input->post('hash');
		$key=$this->input->post('key');
		$productinfo=$this->input->post('productinfo');
		$email= $this->input->post('email');
		$salt = "e5iIg1jwi8";
		$sno = $this->input->post('udf1');

		$retHashSeq = $salt.'|'.$status.'||||||||||'.$sno.'|'.$email.'|'.$firstname.'|'.$productinfo.'|'.$amount.'|'.$txnid.'|'.$key;

		$hash = strtolower(hash("sha512", $retHashSeq));

		if ($hash != $posted_hash) {
	       $this->session->set_flashdata('msg_error', "An Error occured while processing your payment. Try again..");
		}

		else{
			$this->session->set_flashdata('msg_success', "Payment was successful..");
		}
		unset($_POST);
		redirect('payumoney/index');
	}

	//Method that handles when payment was failed
	public function error(){
		unset($_POST);
		$this->session->set_flashdata('msg_error', "Your payment was failed. Try again..");
		$payment_session = array(
            'sess_pay_u_id' => '',
            'sess_pay_trans_id' => '',
            'sess_pay_services_id' => '',
            'sess_pay_amount' => ''
        );
        $this->session->unset_userdata($payment_session);
        $this->session->unset_userdata('sess_pay_u_id');
        $this->session->unset_userdata('sess_pay_trans_id');
        $this->session->unset_userdata('sess_pay_services_id');
        $this->session->unset_userdata('sess_pay_amount');
        unset($payment_session);
		redirect('payumoney/index');
	}

	//Method that handles when payment was cancelled.
	public function cancel(){
		$payment_session = array(
            'sess_pay_u_id' => '',
            'sess_pay_trans_id' => '',
            'sess_pay_services_id' => '',
            'sess_pay_amount' => ''
        );
        $this->session->unset_userdata($payment_session);
		unset($_POST);
		$this->session->set_flashdata('msg_error', "Your payment was cancelled. Try again..");
		redirect('payumoney/index');
	}
}