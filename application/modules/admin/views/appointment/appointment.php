 <!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">Appointment</h4>

                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="#">Products</a></li>
                            <li class="breadcrumb-item active">Appointment</li>
                        </ol>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <!-- Modal -->
                <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <form role="form" method="post" action="<?php echo base_url('admin/product/seminar_process/')?>" autocomplete="off" data-parsley-validate novalidate>
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="mySmallModalLabel">Appointment</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="col-xs-12">
                                     <div class="p-20">
                                        <div class="form-group row">
                                            <label for="services_type" class="col-sm-3 form-control-label">Services Type<span class="text-danger">*</span></label>
                                            <div class="col-sm-9">
                                                <select name="services_type" class="form-control">
                                                    <?php foreach($services_list as $s){?>
                                                    <option value="<?php echo $s['services_id']?>"><?php echo $s['services_name']?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="a_name" class="col-sm-3 form-control-label">Name<span class="text-danger"></span></label>
                                            <div class="col-sm-9">
                                                <input type="text" name="a_name" id="a_name" required placeholder="Name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="a_company_name" class="col-sm-3 form-control-label">Company Name<span class="text-danger"></span></label>
                                            <div class="col-sm-9">
                                                <input type="text" name="a_company_name" id="a_company_name" required placeholder="Company Name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="a_email" class="col-sm-3 form-control-label">Email<span class="text-danger"></span></label>
                                            <div class="col-sm-9">
                                                <input type="email" name="a_email" id="a_email" required placeholder="Email" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="a_contact_no" class="col-sm-3 form-control-label">Contact No<span class="text-danger"></span></label>
                                            <div class="col-sm-9">
                                                <input type="number" name="a_contact_no" id="a_contact_no" required placeholder="Contact No" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="location_cat" class="col-lg-3 form-control-label">Location<span class="text-danger"></span></label>
                                            <div class="radio radio-info radio-inline">
                                                <input type="radio" id="location_cat_GH" value="GH" name="location_cat">
                                                <label for="location_cat_GH" class="col-sm-6 form-control-label"> GH </label>
                                            </div>
                                            <div class="radio radio-info radio-inline">
                                                <input type="radio" id="location_cat_O" value="Others" name="location_cat">
                                                <label for="location_cat_O" class="col-sm-6 form-control-label">   Others   </label>
                                            </div>
                                        </div>
                                        <div class="form-group row" id="location_specify" style="display: none">
                                            <label for="location" class="col-sm-3 form-control-label">&nbsp;<span class="text-danger"></span></label>
                                            <div class="col-sm-9">
                                                <textarea name="location" id="location" class="form-control" rows="4" placeholder="Enter Location"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="from_date" class="col-sm-3 form-control-label">From Date<span class="text-danger"></span></label>
                                            <div class="col-sm-9">
                                                <div class="input-daterange input-group" id="date-range">
                                                    <input type="text" class="form-control" name="from_date">
                                                    <span class="input-group-addon bg-custom b-0">to</span>
                                                    <input type="text" class="form-control" name="to_date">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="start_time" class="col-sm-3 form-control-label">Start Time</label>
                                            <div class="col-sm-3">
                                                <div class="input-group">
                                                    <input id="start_time" type="text" name="start_time" class="form-control">
                                                    <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                                </div>
                                            </div>
                                            <label for="end_time" class="col-sm-2 form-control-label">End Time</label>
                                            <div class="col-sm-3">
                                                <div class="input-group">
                                                    <input id="end_time" type="text" name="end_time" class="form-control">
                                                    <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="status" class="col-sm-3 form-control-label">Status<span class="text-danger">*</span></label>
                                            <div class="col-sm-9">
                                                <select name="status" class="form-control">
                                                    <option value="">--Select--</option>
                                                    <option value="pending">Pending</option>
                                                    <option value="paid">Paid</option>
                                                    <option value="unpaid">Unpaid</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="action" id="action" value="add">
                                <input type="hidden" name="hddSeminar_id" id="hddSeminar_id">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" name="bnt_save" id="bnt_save" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            <div class="row">
                <div class="col-12">
                    <div class="card-box table-responsive">
                        <h4 class="m-t-0 header-title"><b>Appointment</b></h4>
                        <div class="card-block">
                            <a href="javascript:void(0)" onclick="add_data()" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-sm"><i class="fa fa-plus"></i>  Add</a>
                        </div>
                        <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Appointment ID</th>
                                <th>Service Type</th>
                                <th>Name</th>
                                <th>Company</th>
                                <th>Location</th>
                                <th>Appointment Date</th>
                                <th>Appointment Time</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach($list as $row){ 
                                    if($row['a_status']=='pending'){
                                        $status = '<span class="label-danger label label-default">Pending</span>';
                                    }else{
                                        $status = '<span class="label-default label">Inactive</span>';
                                    }
                                ?>
                            <tr>
                                <td><?php echo $row['a_id']?></td>
                                <td><?php echo $row['services_name']?></td>
                                <td><?php echo $row['a_name']?></td>
                                <td><?php echo $row['a_company']?></td>
                                <td><?php echo $row['a_location']?></td>
                                <td><?php echo $row['a_date']?></td>
                                <td><?php echo $row['a_time']?></td>
                                <td><?php echo $status?></td>
                                <td>
                                    <a href="javascript:void(0)" onclick="edit_data('<?php echo $row["a_id"]?>')" class="btn-sm btn-info waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-sm"><i class="fa fa-edit"></i></a>
                                    <a href="javascript:void(0)" onclick="delete_data('<?php echo $row["a_id"]?>')" class="btn-sm btn-danger waves-effect waves-light"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end row -->


        </div> <!-- container -->

    </div> <!-- content -->

</div>
<!-- End content-page -->
<?php echo $this->load->view('admin_layout/footer');?>

<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
<script type="text/javascript">
$('#start_time').timepicker({
    defaultTIme: false,
    icons: {
        up: 'zmdi zmdi-chevron-up',
        down: 'zmdi zmdi-chevron-down'
    }
});
$('#end_time').timepicker({
    defaultTIme: false,
    icons: {
        up: 'zmdi zmdi-chevron-up',
        down: 'zmdi zmdi-chevron-down'
    }
});
$('#date-range').datepicker({
        toggleActive: true
    });
$('.input-daterange-datepicker').daterangepicker({
    format: 'MM/DD/YYYY',
    buttonClasses: ['btn', 'btn-sm'],
    applyClass: 'btn-custom',
    cancelClass: 'btn-secondary'
});
$(".radio-inline").find('[name="location_cat"]').click(function(){
    if($(this).val() == 'Others'){
        $("#location_specify").show();
    }else{
        $("#location_specify").hide();
    }
})

function add_data(){
    $("#mySmallModalLabel").text('Appointment');
   
}
function edit_data(id){
    $('#action').val('edit');
    // alert(id);
    $("#mySmallModalLabel").text('Appointment');
    $("#bnt_save").text('Update');
        $.ajax({
            data:{
            id:id
            },
            type: "post",
            url: "<?php echo base_url()?>"+"admin/product/getSeminarEdit",
            dataType: "json",
            success: function(data) {
                jQuery.each(data, function(key, list) {
                 // console.log(list);
                    $('#seminar_name').val(list['seminar_name']);
                    $('#date_seminar').val(list['date_seminar']);
                    $('#name_of_speaker').val(list['name_of_speaker']);
                    $('#passcode').val(list['passcode']);
                    if(list['status']==1){
                        $('#status').prop('checked', true);
                    }else{
                        $('#status').prop('checked', false);
                    }
                    $('#hddSeminar_id').val(list['seminar_id']);
                });
            }
        });
}
function delete_data(id){
    var r = confirm("Do you want to delete this record?");
    if(r== true){
        $.ajax({
            data:{
                id:id
            },
            type: "post",
            url: "<?php echo base_url()?>"+"admin/product/deleteSeminar",
            success:function(data){
                location.reload();
                
                
            }
        });

    }else{
        return false;
    }
}
$(document).ready(function() {
    $('#datatable').DataTable();
    //Buttons examples
    var table = $('#datatable-buttons').DataTable({
        lengthChange: false,
        buttons: ['copy', 'excel', 'pdf', 'colvis']
    });

    table.buttons().container()
            .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
} );

</script>

</body>
</html>