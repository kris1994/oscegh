CREATE DATABASE  IF NOT EXISTS `oscegh` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `oscegh`;
-- MySQL dump 10.13  Distrib 5.5.57, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: oscegh
-- ------------------------------------------------------
-- Server version	5.5.57-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gh_configuration`
--

DROP TABLE IF EXISTS `gh_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gh_configuration` (
  `config_id` int(11) NOT NULL,
  `website_title` varchar(30) NOT NULL,
  `address` varchar(150) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `fax` varchar(15) NOT NULL,
  `email` varchar(254) NOT NULL,
  `website` varchar(30) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `approval_disabled` tinyint(2) NOT NULL,
  `signatureimage` varchar(255) NOT NULL,
  `config_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gh_configuration`
--

LOCK TABLES `gh_configuration` WRITE;
/*!40000 ALTER TABLE `gh_configuration` DISABLE KEYS */;
INSERT INTO `gh_configuration` VALUES (1,'GH-Ordering System and Custome','18 Holybrook Street, Glasgow , G42 7EH','07854327040','','info@onyxtech.co.uk','www.onyxtech.co.uk','favicon.ico',1,'RajakSign.jpg','2014-08-23');
/*!40000 ALTER TABLE `gh_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gh_menu`
--

DROP TABLE IF EXISTS `gh_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gh_menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(50) NOT NULL,
  `menu_url` varchar(50) NOT NULL,
  `menu_order` int(11) NOT NULL,
  `mt_id` int(11) NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gh_menu`
--

LOCK TABLES `gh_menu` WRITE;
/*!40000 ALTER TABLE `gh_menu` DISABLE KEYS */;
INSERT INTO `gh_menu` VALUES (1,'configuration','configuration',1,2);
/*!40000 ALTER TABLE `gh_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gh_menutype`
--

DROP TABLE IF EXISTS `gh_menutype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gh_menutype` (
  `mt_id` int(11) NOT NULL AUTO_INCREMENT,
  `mt_name` varchar(15) NOT NULL,
  `mt_url` varchar(45) NOT NULL,
  PRIMARY KEY (`mt_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gh_menutype`
--

LOCK TABLES `gh_menutype` WRITE;
/*!40000 ALTER TABLE `gh_menutype` DISABLE KEYS */;
INSERT INTO `gh_menutype` VALUES (1,'Dashboard','dashboard'),(2,'Products','product'),(3,'Setup','setup');
/*!40000 ALTER TABLE `gh_menutype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gh_user`
--

DROP TABLE IF EXISTS `gh_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gh_user` (
  `u_id` int(11) NOT NULL,
  `u_logname` varchar(15) NOT NULL,
  `u_password` varchar(50) NOT NULL,
  `u_name` varchar(20) NOT NULL,
  `u_email` varchar(254) NOT NULL,
  `u_remarks` varchar(200) NOT NULL,
  `u_cellno` varchar(15) NOT NULL,
  `u_loginfdate` date NOT NULL,
  `u_logintodate` date NOT NULL,
  `su_id` int(11) NOT NULL,
  `u_enabled` tinyint(2) NOT NULL,
  `skin_id` int(11) NOT NULL,
  `source` varchar(100) NOT NULL,
  `header_bg` varchar(20) NOT NULL,
  `footer_bg` varchar(20) NOT NULL,
  `psite_id` int(14) DEFAULT NULL,
  `country_id` int(14) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gh_user`
--

LOCK TABLES `gh_user` WRITE;
/*!40000 ALTER TABLE `gh_user` DISABLE KEYS */;
INSERT INTO `gh_user` VALUES (3,'admin','d033e22ae348aeb5660fc2140aec35850c4da997','admin','admin@gmail.com','admin','','0000-00-00','0000-00-00',0,1,0,'','','',NULL,NULL),(21,'guest','084e0343a0486ff05530df6c705c8bb4','guest','test@gmail.com','guest','902390234','2017-10-22','2017-10-22',17,1,0,'','','',0,0);
/*!40000 ALTER TABLE `gh_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gh_userd`
--

DROP TABLE IF EXISTS `gh_userd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gh_userd` (
  `u_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `u_add` tinyint(2) NOT NULL,
  `u_edit` tinyint(2) NOT NULL,
  `u_del` tinyint(2) NOT NULL,
  `u_view` tinyint(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gh_userd`
--

LOCK TABLES `gh_userd` WRITE;
/*!40000 ALTER TABLE `gh_userd` DISABLE KEYS */;
INSERT INTO `gh_userd` VALUES (3,1,1,1,1,1),(21,1,1,1,1,1);
/*!40000 ALTER TABLE `gh_userd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'oscegh'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-01-02 11:38:30
