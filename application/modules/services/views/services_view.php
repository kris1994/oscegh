    <link href="<?php echo base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/plugins/bootstrap-daterangepicker/daterangepicker.css')?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.min.css')?>" rel="stylesheet">
    <!--Header-->
     <style type="text/css">  
    #payUmoney {
        width: 150px;
        height: 150px;
        background-image: url(<?php echo base_url()?>assets_services/images/payUmoneyIcon.png);
        background-repeat: no-repeat;
        background-size: 100%;
        top: -50px
    }
     </style>
    <div class="header" id="home">
        <?php  $this->load->Module('sidebar')->index();?>
        <!--top-bar-w3-agile-->
        <!--//top-bar-w3-agile-->
    </div><!--/inner_banner-->
    <div class="inner_banner">
    </div>
    <!--//inner_banner-->
    <!--/short-->
    <div class="services-breadcrumb">
        <div class="inner_breadcrumb">

            <ul class="short">
                <li><a href="index.html">Home</a><span>|</span></li>
                <li>Services</li>
            </ul>
        </div>
    </div>
    <?php
    if (empty($list)) {
        redirect('services');
    }

    ?>
<!--//short-->
<!-- /inner_content -->
<div class="banner_bottom">
    <div class="container">
        <div class="tittle_head">
            <h3 class="tittle"><?php echo $list->services_name?></h3>
        </div>
        <div class="inner_sec_info_wthree_agile">
            <div class="col-md-8 blog_section">
                <div class="blog_img single">
                    <div class="single-left1">
                        <img src="<?php echo base_url('uploaded_image/services').'/'.$list->services_image?>" alt=" " class="img-responsive">
                        <h5><?php echo $list->services_name?></h5>
                        <!-- <ul class="blog_list single">
                            <li><span class="fa fa-user" aria-hidden="true"></span><a href="#">Jan Mark</a><i>|</i></li>
                            <li><span class="fa fa-heart" aria-hidden="true"></span><a href="#">10</a><i>|</i></li>
                            <li><span class="fa fa-tag" aria-hidden="true"></span><a href="#">2</a></li>

                        </ul> -->
                        <p><?php echo rawurldecode($list->services_description)?></p>
                    </div>
                    <div class="comments">
                        <h3>Our Recent <span>Comments</span></h3>
                        <div class="comments-grids">
                            <?php foreach ($getComments as $cm) { ?>
                            <div class="comments-grid">
                                <div class="comments-grid-left">
                                    <img src="<?php echo base_url('use_image/default-img1.png')?>" alt=" " class="img-responsive">
                                </div>
                                <div class="comments-grid-right">
                                    <h4><a href="#"><?php echo $cm['comment_name']?></a></h4>
                                    <ul>
                                        <li><?php echo date('F j, Y',strtotime($cm['comment_datetime']))?> <i>|</i></li>
                                        <li><a href="#">Reply</a></li>
                                    </ul>
                                    <p><?php echo $cm['message']?></p>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        <?php }?>
                        </div>
                        
                        <div class="clearfix"> </div>
                    </div>
                    <div class="contact-form left_bar">
                        <h3>Leave a <span>Comment</span></h3>
                        <form method="post" action="<?php echo base_url('services/Lcomments')?>">
                            <div class="left_form">
                                <div>
                                    <span><label>Name</label></span>
                                    <span><input name="userName" type="text" class="textbox" required=""></span>
                                </div>
                                <div>
                                    <span><label>E-mail</label></span>
                                    <span><input name="userEmail" type="text" class="textbox" required=""></span>
                                </div>
                                <div>
                                    <span><label>Contact No.</label></span>
                                    <span><input name="userPhone" type="text" class="textbox" required=""></span>
                                </div>
                            </div>
                            <div class="right_form">
                                <div>
                                    <span><label>Message</label></span>
                                    <span><textarea name="Message" required=""> </textarea></span>
                                </div>
                                <div>
                                    <input type="hidden" name="hddServicesID" value="<?php echo $list->services_id?>">
                                    <span><input type="submit" value="Submit" class="myButton"></span>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>

            </div>

            <div class="col-md-4 blog_left">
                <?php if($list->services_type == 'Other Services'){?>
                <div class="search left_bar" style="width: 130%; text-align: center">
                    <h3>Create Appointment <span></span></h3>
                    <!-- <form action="#" method="post">
                        <input class="email" type="email" name="Email" placeholder="Email here" required="">
                        <input type="submit" value="Send">
                    </form> -->
                </div>
                <div class="faq left_bar" style="width: 130%">
                    <!-- <h3>FAQ's</h3> -->
                    <form class="form-horizontal" method="post">
                        <div class="form-group">
                            <label for="selector1" class="col-sm-4 control-label">Services Type</label>
                            <div class="col-sm-8">
                                <select name="services_type" id="services_type" class="form-control1" required="required">
                                <option value="">Select Services</option>
                                <?php foreach($Oservices as $s){?>
                                <option value="<?php echo $s['services_id']?>" <?php echo ($services_id_view ==$s['services_id'])?'selected':'' ?> ><?php echo $s['services_name']?></option>
                                <?php }?>
                            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="focusedinput" class="col-sm-4 control-label">Name</label>
                            <div class="col-sm-8">
                                <input type="text" name="appointment_name" class="form-control1" id="appointment_name" value="<?php echo $this->session->userdata('sess_user_name')?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="focusedinput" class="col-sm-4 control-label">Company Name</label>
                            <div class="col-sm-8">
                                <input type="text" name="appointment_company" na class="form-control1" id="appointment_company">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="focusedinput" class="col-sm-4 control-label">Email</label>
                            <div class="col-sm-8">
                                <input type="email" name="appointment_email" class="form-control1" id="appointment_email">
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="focusedinput" class="col-sm-4 control-label">Contact No.</label>
                            <div class="col-sm-8">
                                <input type="text" name="appointment_contact_no" class="form-control1" id="appointment_contact_no">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="radio" class="col-sm-4 control-label">Location</label>
                            <div class="col-sm-8">
                                <div class="radio-inline radio "><label><input type="radio" name="location_rad" id="location_rad" value="GH" checked="checked"> GH <i></i></label></div>
                                <div class="radio-inline radio "><label><input type="radio" name="location_rad" id="location_rad" value="Others"> Others <i></i></label></div>
                            </div>
                        </div>
                        <div class="form-group location_specify" style="display: none;">
                            <label for="selector1" class="col-sm-4 control-label">&nbsp;</label>
                            <div class="col-sm-8">
                                <textarea name="appointment_location" id="appointment_location" cols="50" rows="4" class="form-control1" placeholder="Enter Location" style="height: 100px;"></textarea>
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="focusedinput" class="col-sm-4 control-label">Date</label>
                            <div class="col-sm-8">
                                <input type="text" name="appointment_date" class="form-control1" placeholder="yyyy-mm-dd" id="appointment_date">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="focusedinput" class="col-sm-4 control-label">Time</label>
                            <div class="col-sm-8">
                                <input id="appointment_time" name="appointment_time" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <label for="selector1" class="col-sm-4 control-label">&nbsp;</label>
                            <div class="col-sm-8">
                                <button type="button" name="btn_set_appointment" id="btn_set_appointment" class="btn btn-primary">Set Appointment</button>
                            </div>
                        </div>
                    </form>
                </div>
                <?php }else{ ?>
                <div class="search left_bar" style="width: 130%; text-align: center">
                    <h3>Avail Services <span></span></h3>
                    <!-- <form action="#" method="post">
                        <input class="email" type="email" name="Email" placeholder="Email here" required="">
                        <input type="submit" value="Send">
                    </form> -->
                </div>

                <div class="faq left_bar" style="width: 130%">
                    <form class="form-horizontal" action="<?php echo base_url('services/avail_services')?>" method="post">
                        <?php
                        $disabled_btn = '';
                        if($this->session->userdata('sess_logged_in')!=true){
                            $disabled_btn = 'disabled="disabled"';
                        ?>
                        <div class="form-group">
                            <label for="radio" class="col-sm-9 control-label">Login First To Avail This Services <a href="<?php echo base_url('login')?>">Login</a></label>
                        </div>
                        <?php }?>
                        <hr>
                        <div id="create_new_div">
                            <div class="form-group">
                                <label for="focusedinput" class="col-sm-4 control-label">Company Name</label>
                                <div class="col-sm-8">
                                    <input type="text" name="av_company_name" na class="form-control1" id="av_company_name" readonly="readonly" value="<?php echo @$getUserLogDetails->company_name?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="focusedinput" class="col-sm-4 control-label">Address</label>
                                <div class="col-sm-8">
                                    <input type="text" name="av_address" na class="form-control1" id="av_address" readonly="readonly" value="<?php echo @$getUserLogDetails->u_address?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="focusedinput" class="col-sm-4 control-label">Contact No.</label>
                                <div class="col-sm-8">
                                    <input type="text" name="av_contact_no" class="form-control1" id="av_contact_no" readonly="readonly" value="<?php echo @$getUserLogDetails->contact_no?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="focusedinput" class="col-sm-4 control-label">Email</label>
                                <div class="col-sm-8">
                                    <input type="email" name="av_email" class="form-control1" id="av_email" readonly="readonly" value="<?php echo @$getUserLogDetails->u_email?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="focusedinput" class="col-sm-4 control-label">Mobile #</label>
                                <div class="col-sm-8">
                                    <input type="text" name="av_mobile_no" class="form-control1" id="av_mobile_no" readonly="readonly" value="<?php echo @$getUserLogDetails->u_cellno?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="focusedinput" class="col-sm-4 control-label">Company TIN #</label>
                                <div class="col-sm-8">
                                    <input type="text" name="av_company_tin_no" class="form-control1" id="av_company_tin_no" readonly="readonly" value="<?php echo @$getUserLogDetails->contact_no?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="focusedinput" class="col-sm-4 control-label">Transaction ID</label>
                                <div class="col-sm-8">
                                    <input type="text" name="transaction_id" class="form-control1" id="transaction_id" readonly="readonly" value="<?php echo $transIDGen;?>" style="color:blue; font-weight: bold">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="focusedinput" class="col-sm-4 control-label">Services</label>
                                <div class="col-sm-8">
                                    <input type="text" name="av_services" class="form-control1" id="av_amount" value="<?php echo @$list->services_name?>" readonly="readonly" style="color:blue; font-weight: bold">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="focusedinput" class="col-sm-4 control-label">Amount (PHP)</label>
                                <div class="col-sm-8">
                                    <input type="text" name="av_amount" class="form-control1" id="av_amount" value="<?php echo @$list->services_price;?>" readonly="readonly" style="color:blue; font-weight: bold">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group">
                                <label for="selector1" class="col-sm-4 control-label">&nbsp;</label>
                                <div class="col-sm-8">
                                    <input type="hidden" name="hddServicesID" value="<?php echo @$list->services_id;?>">
                                    <input type="hidden" name="hddLogID" value="<?php echo @$getUserLogDetails->u_id?>">
                                    <button type="submit" name="btn_payment" class="btn btn-primary" <?php echo $disabled_btn?> >Avail Now</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="faq left_bar" style="width: 130%; text-align: center">
                    <img src="<?php echo base_url()?>assets_services/images/payUmoney_services.jpg" alt="PayUmoney Services" style="width:400px;">
                </div>
                <?php }?>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script type="text/javascript">
    $(".radio #location_rad").click(function(){
        if($(this).val() == 'Others'){
            $(".location_specify").css('display',"block");
        }else{
            $(".location_specify").css('display',"none");
        }
    })
</script>
<script src="<?php echo base_url('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')?>"></script>
<script src="<?php echo base_url('assets/plugins/timepicker/bootstrap-timepicker.min.js')?>"></script>
<script type="text/javascript">
    var _base_url = "<?php echo base_url()?>";
    $('#appointment_date').datepicker({
        autoclose: true,
        todayHighlight: true,
        format:'yyyy-mm-dd'
    });
    $('#appointment_time').timepicker({
        defaultTIme: false,
        icons: {
            up: 'zmdi zmdi-chevron-up',
            down: 'zmdi zmdi-chevron-down'
        }
    });
 
</script>