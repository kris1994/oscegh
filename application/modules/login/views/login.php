<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="inner_banner">
    </div>
    <!--//inner_banner-->
    <!--/short-->
    <div class="services-breadcrumb">
        <div class="inner_breadcrumb">

            <ul class="short">
                <li><a href="<?php echo base_url('')?>">Home</a><span>|</span></li>
                <li>Login</li>
            </ul>
        </div>
    </div>
<div class="grid_3 grid_4">
  <div class="ele_info_text login-top">
    <div class="col-md-12 list-grids">
        <?php
            if($this->session->flashdata('msg_error')){
                echo "<div class='alert alert-danger' role='alert'>".$this->session->flashdata('msg_error')."<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
            }
            elseif($this->session->flashdata('msg_success')){
                echo "<div class='alert alert-success' role='alert'>".$this->session->flashdata('msg_success')."<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
            }
        ?>
    </div>
    <div class="col-md-6 list-grids">
      <h3 class="ele-tittle">Login Form</h3>
      <form action="<?php echo site_url('login/do_login')?>" method="post">
        <input type="text" class="username" name="username" placeholder="Username" required="">
        <input type="password" class="password" name="password" placeholder="Password" required="">
        <?php
        if ($this->agent->is_referral())
            {
                echo '<input type="hidden" name="redirectCurrent" value="'.$this->agent->referrer().'">';
            }else{
                echo '<input type="hidden" name="redirectCurrent" value="dashboard">';
            }
        ?>
        
        <input type="submit" value="Login ">
        <!-- <p>or Connect with.... </p>
        <div class="social-icons">
          <ul>
            <li><a href="#">Facebook </a></li>
            <li><a href="#">Twitter </a></li>
          </ul>
          
        </div> -->
        <div class="clearfix"> </div>
      </form>
    </div>
    <div class="col-md-6 list-grids llist-grids-btm2">
      <h3 class="ele-tittle">Registration Form</h3>
      <form action="<?php echo base_url('login/registration')?>" method="post">
        <input type="text" class="active" name="full_name" placeholder="Your Name" required="">
        <input type="email" class="email" name="email" placeholder="Email Address" required="">
        <input type="text" class="company_name" name="company_name" placeholder="Company Name" required="">
        <input type="text" class="company_tinNo" name="company_tinNo" placeholder="Company Tin #" required="">
        <input type="text" class="mobile_no" name="mobile_no" placeholder="Mobile No." required="">
        <input type="text" class="username" name="username" placeholder="Username" required="">
        <input type="password" class="password" name="password" placeholder="Password" required="">
        <div class="g-recaptcha" data-sitekey="6LdYmkkUAAAAAJxwDN7YfL0wrjUXaAbiphey0eEF"></div>
        <input type="submit" value="Sign Up">
      </form>
    </div>
    <div class="clearfix"> </div>
  </div>
</div>
</center>