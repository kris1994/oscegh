<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Product extends MX_Controller{
    function __construct() {
        parent::__construct();
        $this->load->model('mdl_general');
        $this->load->library('session');
    }
    // public function index(){
    //     if($this->session->userdata('asess_logged_in')==true){
    //         redirect('admin');
    //     }
    // }
    public function manage_products(){
        $main_title=$this->mdl_general->GetInfoByRow('gh_configuration','config_id',array('config_id'=>1));
        $page_details['page_title']= $main_title->website_title.' | Product';
        if($this->session->userdata('asess_logged_in')==true){
            $this->load->view('admin_layout/header', $page_details);
            $this->load->view('admin_layout/sidebar');
            $this->load->view('manage_products/manage_products');
        }else{
            $this->load->view('login/login');
        }
    }
    public function manage_services(){
        $main_title=$this->mdl_general->GetInfoByRow('gh_configuration','config_id',array('config_id'=>1));
        $page_details['page_title']= $main_title->website_title.' | Product';
        if($this->session->userdata('asess_logged_in')==true){
            $this->load->view('admin_layout/header', $page_details);
            $this->load->view('admin_layout/sidebar');
            $data['list'] = $this->mdl_general->GetAllInfo('services','services_id','');
            $this->load->view('manage_services/manage_services',$data);
        }else{
        $this->load->view('login/login');
        }
    }
    public function services_process(){
        $action = $this->input->post('action');
        $status = $this->input->post('status');
        switch (strtoupper($action)) {
            case 'ADD':
                 $services_image = '';
                    if ($_FILES['services_image'] > 0) {
                        $randomNum = uniqid();
                        $files = basename($_FILES['services_image']['name']);
                        $tmp = explode('.', $files);
                        $ext = end($tmp);
                        $services_image = $randomNum . '.' . $ext;
                        
                        $uploadDir = 'uploaded_image/services/';
                        $uploadFile = $uploadDir . $services_image;
                        move_uploaded_file($_FILES['services_image']['tmp_name'], $uploadFile);
                    }else{
                        $services_image = 'no-image.png';
                    }
                    $form_data = array(
                        'services_name' => $this->input->post('services_name'),
                        'services_description' => $this->input->post('description'),
                        'services_price' => $this->input->post('services_price'),
                        'services_image' => $services_image,
                        'services_type' => $this->input->post('services_type'),
                        'venue' => $this->input->post('venue'),
                        'dateFrom' => $this->input->post('dateFrom'),
                        'dateTo' => $this->input->post('dateTo'),
                        'timeStart' => $this->input->post('timeStart'),
                        'timeEnd' => $this->input->post('timeEnd'),
                        'services_slots' => $this->input->post('set_slots'),
                        'status' => ($status==1)?1:0
                    );

                    $res = $this->mdl_general->SaveForm('services',$form_data);
                    redirect('product/manage_services');
                break;
            case 'EDIT':
                 $services_image = '';
                    if ($_FILES['services_image']['size'] > 0) {
                        $randomNum = uniqid();
                        $files = basename($_FILES['services_image']['name']);
                        $tmp = explode('.', $files);
                        $ext = end($tmp);
                        $services_image = $randomNum . '.' . $ext;
                        
                        $uploadDir = 'uploaded_image/services/';
                        $uploadFile = $uploadDir . $services_image;
                        move_uploaded_file($_FILES['services_image']['tmp_name'], $uploadFile);
                    }else{
                        $services_image = $this->input->post('services_filename');
                        if(empty($services_image)){
                            $services_image = 'no-image.png';
                        }
                    }
                    $form_data = array(
                        'services_name' => $this->input->post('services_name'),
                        'services_description' => $this->input->post('description'),
                        'services_price' => $this->input->post('services_price'),
                        'services_image' => $services_image,
                        'services_type' => $this->input->post('services_type'),
                        'venue' => $this->input->post('venue'),
                        'dateFrom' => $this->input->post('dateFrom'),
                        'dateTo' => $this->input->post('dateTo'),
                        'timeStart' => $this->input->post('timeStart'),
                        'timeEnd' => $this->input->post('timeEnd'),
                        'services_slots' => $this->input->post('set_slots'),
                        'status' => ($status==1)?1:0
                    );
                    $hddSID = $this->input->post('hddSID');
                    $where = array('services_id' => $hddSID);
                    $this->mdl_general->Manage('services',$form_data, $where);
                    redirect('product/manage_services');
                break;
            default:
                # code...
                break;
        }
        
    }
    public function getServicesList(){
        $id=$this->input->post('id');
        $data=$this->mdl_general->GetAllInfo('services','services_id',array('services_id'=>$id));
        echo json_encode($data);
    }
    public function deleteServices(){
        $id = $this->input->post('id');
        $this->mdl_general->Delete('services',array('services_id'=>$id));
        $response=array(
            'status'=>'success',
            'message'=>'Menu deleted successfully' 
            );
        $this->session->set_flashdata($response);
        redirect('setup/country');
    }
    public function set_evaluation(){
        $main_title=$this->mdl_general->GetInfoByRow('gh_configuration','config_id',array('config_id'=>1));
        $page_details['page_title']= $main_title->website_title.' | Product';
        if($this->session->userdata('asess_logged_in')==true){
            $this->load->view('admin_layout/header', $page_details);
            $this->load->view('admin_layout/sidebar');
            $data['list'] = $this->mdl_general->GetAllInfo('set_seminar','seminar_id','');
            $this->load->view('set_evaluation/set_evaluation',$data);
        }else{
        $this->load->view('login/login');
        }
    }
    public function seminar_process(){
        $action = $this->input->post('action');
        $status = $this->input->post('status');
        switch (strtoupper($action)) {
            case 'ADD':
                $form_data = array(
                    'seminar_name' => $this->input->post('seminar_name'),
                    'date_seminar' => $this->input->post('date_seminar'),
                    'name_of_speaker' => $this->input->post('name_of_speaker'),
                    'passcode' => $this->input->post('passcode'),
                    'status' => ($status==1)?1:0
                );
                // $this->mdl_general->SaveForm('set_seminar',$form_data);
                redirect('product/set_evaluation');
                break;
            case 'EDIT':
                $form_data = array(
                    'seminar_name' => $this->input->post('seminar_name'),
                    'date_seminar' => $this->input->post('date_seminar'),
                    'name_of_speaker' => $this->input->post('name_of_speaker'),
                    'passcode' => $this->input->post('passcode'),
                    'status' => ($status==1)?1:0
                );
                $hddSeminar_id = $this->input->post('hddSeminar_id');
                $where = array('seminar_id' => $hddSeminar_id);
                $this->mdl_general->Manage('set_seminar',$form_data, $where);
                redirect('product/set_evaluation');
                break;
            default:
                # code...
                break;
        }
    }
    public function getSeminarEdit(){
        $id=$this->input->post('id');
        $data=$this->mdl_general->GetAllInfo('set_seminar','seminar_id',array('seminar_id'=>$id));
        echo json_encode($data);
    }
    public function deleteSeminar(){
        $id = $this->input->post('id');
        $this->mdl_general->Delete('set_seminar',array('seminar_id'=>$id));
        $response=array(
            'status'=>'success',
            'message'=>'Seminar deleted successfully' 
            );
        $this->session->set_flashdata($response);
        redirect('product/seminar_process');
    }
    public function page_image(){
        $main_title=$this->mdl_general->GetInfoByRow('gh_configuration','config_id',array('config_id'=>1));
        $page_details['page_title']= $main_title->website_title.' | Page Image';
        if($this->session->userdata('asess_logged_in')==true){
            $this->load->view('admin_layout/header', $page_details);
            $this->load->view('admin_layout/sidebar');
            $data['list'] = $this->mdl_general->GetAllInfo('page_image','pg_id','');
            $this->load->view('page_image/page_image',$data);
        }else{
        $this->load->view('login/login');
        }
    }
    public function page_image_process(){
        $action = $this->input->post('action');
        $status = $this->input->post('status');
        switch (strtoupper($action)) {
            case 'ADD':

                 $pg_image = '';
                    if ($_FILES['pg_image'] > 0) {
                        $randomNum = uniqid();
                        $files = basename($_FILES['pg_image']['name']);
                        $tmp = explode('.', $files);
                        $ext = end($tmp);
                        $pg_image = $randomNum . '.' . $ext;
                        
                        $uploadDir = 'uploaded_image/';
                        $uploadFile = $uploadDir . $pg_image;
                        move_uploaded_file($_FILES['pg_image']['tmp_name'], $uploadFile);
                    }else{
                        $pg_image = 'no-image.png';
                    }
                    $form_data = array(
                        'pg_image_type' => $this->input->post('pg_image_type'),
                        'pg_image' => $pg_image,
                        'pg_status' => ($status==1)?1:0
                    );

                    $res = $this->mdl_general->SaveForm('page_image',$form_data);
                    redirect('product/page_image');
                break;
            case 'EDIT':
                    $pg_image = '';
                    if ($_FILES['pg_image'] > 0) {
                        $randomNum = uniqid();
                        $files = basename($_FILES['pg_image']['name']);
                        $tmp = explode('.', $files);
                        $ext = end($tmp);
                        $pg_image = $randomNum . '.' . $ext;
                        
                        $uploadDir = 'uploaded_image/';
                        $uploadFile = $uploadDir . $pg_image;
                        move_uploaded_file($_FILES['pg_image']['tmp_name'], $uploadFile);
                    }else{
                        $pg_image = $this->input->post('pg_image_filename');
                        if(empty($services_image)){
                            $pg_image = 'no-image.png';
                        }
                    }
                    $form_data = array(
                        'pg_image_type' => $this->input->post('pg_image_type'),
                        'pg_image' => $pg_image,
                        'pg_status' => ($status==1)?1:0
                    );
                    
                    $hddPGid = $this->input->post('hddPGid');
                    $where = array('pg_id' => $hddPGid);
                    $this->mdl_general->Manage('page_image',$form_data, $where);
                    redirect('product/page_image');
                break;
            default:
                # code...
                break;
        }
        
    }
    public function appointment_list(){
        $this->load->model('mdl_extra');
        $main_title=$this->mdl_general->GetInfoByRow('gh_configuration','config_id',array('config_id'=>1));
        $page_details['page_title']= $main_title->website_title.' | Appointment';
        if($this->session->userdata('asess_logged_in')==true){
            $this->load->view('admin_layout/header', $page_details);
            $this->load->view('admin_layout/sidebar');
            $data['list'] = $this->mdl_extra->getAppointmentList('');
            $data['services_list'] = $this->mdl_general->GetAllInfo('services','services_id',array('status'=>1));
            $this->load->view('appointment/appointment',$data);
        }else{
        $this->load->view('login/login');
        }
    }
    public function getPG_image(){
        $id=$this->input->post('id');
        $data=$this->mdl_general->GetAllInfo('page_image','pg_id',array('pg_id'=>$id));
        echo json_encode($data);
    }
    private function debug($msg="", $exit = false)
    {
        $today = date("Y-m-d H:i:s");

        if (is_array($msg) || is_object($msg))
        {
            echo "<hr>DEBUG ::[".$today."]<pre>\n";
            print_r($msg);
            echo "\n</pre><hr>";
        }
        else
        {
            echo "<hr>DEBUG ::[".$today."] $msg <hr>\n";
        }

        if ($exit) {
            $this->load->library('profiler');
            echo $this->profiler->run();
            exit;
        }
    }
}