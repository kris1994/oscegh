 <!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<style>
img {  width:100%; }
.btn {
    display: inline-block; font-weight: 400; line-height: 1.25; text-align: center; white-space: nowrap; vertical-align: middle; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; border: 1px solid transparent; padding: .2rem .5rem; font-size: 1rem; border-radius: .25rem; -webkit-transition: all .2s ease-in-out; -o-transition: all .2s ease-in-out; transition: all .2s ease-in-out;
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">Manage Image</h4>

                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="#">Image</a></li>
                            <li class="breadcrumb-item active">Manage Image</li>
                        </ol>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-12">
                    <div class="card-box table-responsive">
                        <h4 class="m-t-0 header-title"><b>Manage Image</b></h4>
                        <div class="card-block">
                            <a href="javascript:void(0)" onclick="add_data()" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-sm" class="btn btn-primary"><i class="fa fa-plus"></i> Add Image</a>
                        </div>
                        <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Image ID</th>
                                <th>Image Type</th>
                                <th>Image</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($list as $row) {
                                if($row['pg_status']==1){
                                    $status = '<span class="label-success label label-default">Active</span>';
                                }else{
                                    $status = '<span class="label-default label">Inactive</span>';
                                }
                                $pg_image = base_url('uploaded_image/'.$row['pg_image']);
                            ?>
                            <tr>
                               <td><?php echo $row['pg_id']?></td>
                               <td><?php echo $row['pg_image_type']?></td>
                               <td class="nav-user"><img src="<?php echo $pg_image?>" alt="user" class="rounded-circle"></td>
                               <td><?php echo $status?></td>
                               <td>
                                   <a href="javascript:void(0)" onclick="edit_data('<?php echo $row["pg_id"]?>')" class="btn-sm btn-info waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-sm"><i class="fa fa-edit"></i> </a>
                                   <a href="javascript:void(0)" onclick="delete_data('<?php echo $row["pg_id"]?>')" class="btn-sm btn-danger waves-effect waves-light"><i class="fa fa-trash"></i></a>
                               </td>
                            </tr>
                            <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <!-- Modal -->
                <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <form role="form" method="post" action="<?php echo base_url('product/page_image_process')?>" autocomplete="off" data-parsley-validate novalidate enctype="multipart/form-data">
                        <div class="modal-content" style="width:1000px;">
                            <div class="modal-header">
                                <h5 class="modal-title" id="mySmallModalLabel">Add Image</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="col-xs-12">
                                     <div class="p-20">
                                         <div class="form-group row">
                                            <label for="pg_image_type" class="col-sm-2 form-control-label">Image Type<span class="text-danger">*</span></label>
                                            <div class="col-sm-5">
                                               <select name="pg_image_type" id="pg_image_type" class="form-control">
                                                   <option value="Our Seminars">Our Seminars</option>
                                                   <option value="About">About</option>
                                                   <option value="Mission & Vision">Mission & Vision</option>
                                               </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="pg_image" class="col-sm-2 form-control-label">Picture<span class="text-danger">*</span></label>
                                            <div class="col-sm-5">
                                                <input type="file" name="pg_image" class="dropify" id="pg_image" data-height="172" />
                                                <input type="hidden" name="pg_image_filename" id="pg_image_filename">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2">Active</label>
                                            <div class="col-sm-4">
                                                <div class="checkbox checkbox-primary">
                                                    <input id="status" name="status" type="checkbox" value="1">
                                                    <label for="status">Check me out</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="hddPGid" id="hddPGid">
                                <input type="hidden" name="action" id="action" value="add">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" name="bnt_save" id="bnt_save" class="btn btn-primary">Save Services</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>

        </div> <!-- container -->

    </div> <!-- content -->

</div>
<!-- End content-page -->
<?php echo $this->load->view('admin_layout/footer');?>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
<script type="text/javascript">
$(document).ready(function() {
     $('.dropify').dropify({
        messages: {
            'default': '',
            'replace': 'Drag and drop or click to replace',
            'remove': 'Remove',
            'error': 'Ooops, something wrong appended.'
        },
        error: {
            'fileSize': 'The file size is too big (1M max).'
        }
    });
    $("#description").Editor();
    $('#datatable').DataTable();
    //Buttons examples
    var table = $('#datatable-buttons').DataTable({
        lengthChange: false,
        buttons: ['copy', 'excel', 'pdf', 'colvis']
    });

    table.buttons().container().appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
});
function add_data(){
    $("#services_name").val("");
    $(".dropify-preview").css("display","none");
    $("#status").prop('checked', false);
    $("#bnt_save").text("Add Image");
    $("#hddPGid").val("");
    $("#service_filename_img").val("");
    $("#pg_image_filename").val('');
    $(this).prop('selected', false);
}
function edit_data(id){
    $('#action').val('edit');
    $("#mySmallModalLabel").text('Edit Image');
    $("#bnt_save").val('Update Admin Menu');
        $.ajax({
            data:{
            id:id
            },
            type: "post",
            url: "<?php echo base_url()?>"+"admin/product/getPG_image",
            dataType: "json",
            success: function(data) {
                jQuery.each(data, function(key, list) {
                    // alert(list['services_description']);
                    var _url = "<?php echo base_url()?>";
                    var pg_image = _url+"uploaded_image/"+list['pg_image'];
                    $(".dropify-preview").css("display","block");
                    $(".dropify-render").html('<img src="'+pg_image+'" style="max-height: 172px;">')
                    if(list['pg_status']==1){
                        $("#status").prop('checked', true);
                    }else{
                        $("#status").prop('checked', false);
                    }
                    $("#bnt_save").text("Update Image");
                    $("#hddPGid").val(list['pg_id']);
                    $("#pg_image_filename").val(list['pg_image']);
                    $("#pg_image_type option").each(function(){
                        if($(this).text() == list['pg_image_type']){
                            $(this).prop('selected', true);
                        }else{
                            $(this).prop('selected', false);
                        }
                    })
                });
            }
        });
}
$('#bnt_save').click(function(){
    $('.Editor-editor').each(function() {
        $('#description').val(escape($(this).html()));
    });
});
function delete_data(id){
    var r = confirm("Do you want to delete this record?");
    if(r== true){
        $.ajax({
            data:{
                id:id
            },
            type: "post",
            url: "<?php echo base_url()?>"+"admin/product/deleteServices",
            success:function(data){
                location.reload();
                
                
            }
        });

    }else{
        return false;
    }
}
</script>

</body>
</html>