 <!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">Admin Menu</h4>

                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="#">Products</a></li>
                            <li class="breadcrumb-item active">Admin Menu</li>
                        </ol>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <!-- Modal -->
                <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <form role="form" method="post" action="<?php echo base_url('admin/settings/admin_menu_process/')?>" autocomplete="off" data-parsley-validate novalidate>
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="mySmallModalLabel">Add Menu</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="col-xs-12">
                                     <div class="p-20">
                                        <div class="form-group row">
                                            <label for="menu_name" class="col-sm-4 form-control-label">Menu Name<span class="text-danger">*</span></label>
                                            <div class="col-sm-7">
                                                <input type="text" name="menu_name" required class="form-control" id="menu_name" placeholder="Menu Name">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="menu_url" class="col-sm-4 form-control-label">Menu Url<span class="text-danger">*</span></label>
                                            <div class="col-sm-7">
                                                <input type="text" name="menu_url" id="menu_url" placeholder="Menu Url" required class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="menu_type" class="col-sm-4 form-control-label">Menu Type<span class="text-danger">*</span></label>
                                            <div class="col-sm-7">
                                                <select name="menu_type" required parsley-type="url" class="form-control" id="menu_type">
                                                    <option value="">-Select-</option>
                                                    <?php foreach($menu_type_list as $mt){?>
                                                    <option value="<?php echo $mt['mt_id']?>"><?php echo $mt['mt_name']?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="menu_order" class="col-sm-4 form-control-label">Menu Order<span class="text-danger"></span></label>
                                            <div class="col-sm-7">
                                                <input type="number" name="menu_order" id="menu_order" required placeholder="Menu Order" class="form-control" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="action" id="action" value="add">
                                <input type="hidden" name="hddmenu_id" id="hddmenu_id">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" name="bnt_save" id="bnt_save" class="btn btn-primary">Save Menu</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            <div class="row">
                <div class="col-12">
                    <div class="card-box table-responsive">
                        <h4 class="m-t-0 header-title"><b>Admin Menu</b></h4>
                        <div class="card-block">
                            <a href="javascript:void(0)" onclick="add_data()" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-sm"><i class="fa fa-plus"></i>  Add</a>
                        </div>
                        <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Menu ID</th>
                                <th>Menu Name</th>
                                <th>Menu Url</th>
                                <th>Menu Order</th>
                                <th>Menu Type</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach($list as $row){ ?>
                            <tr>
                                <td><?php echo $row['menu_id']?></td>
                                <td><?php echo $row['menu_name']?></td>
                                <td><?php echo $row['menu_url']?></td>
                                <td><?php echo $row['menu_order']?></td>
                                <td><?php echo $row['mt_name']?></td>
                                <td>
                                    <a href="javascript:void(0)" onclick="edit_data('<?php echo $row["menu_id"]?>')" class="btn-sm btn-info waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-sm"><i class="fa fa-edit"></i>  Edit</a>
                                    <a href="javascript:void(0)" onclick="delete_data('<?php echo $row["menu_id"]?>')" class="btn-sm btn-danger waves-effect waves-light"><i class="fa fa-trash"></i>  Delete</a>
                                </td>
                            </tr>
                            <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end row -->


        </div> <!-- container -->

    </div> <!-- content -->

</div>
<!-- End content-page -->
<?php echo $this->load->view('admin_layout/footer');?>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
<script type="text/javascript">
function add_data(){
    $("#mySmallModalLabel").text('Add Admin Menu');
    $("#bnt_save").val('Save Menu');
    $('#menu_name').val("");
    $('#menu_url').val("");
    $('#menu_order').val("");
    $('#menu_type option').prop('selected', false);
    $('#action').val('add');
    $("#hddmenu_id").val("");
}
function edit_data(id){
    $('#action').val('edit');
    // alert(id);
    $("#mySmallModalLabel").text('Edit Admin Menu');
    $("#bnt_save").val('Update Admin Menu');
        $.ajax({
            data:{
            id:id
            },
            type: "post",
            url: "<?php echo base_url()?>"+"admin/settings/getEditMenuList",
            dataType: "json",
            success: function(data) {
                jQuery.each(data, function(key, list) {
                    // alert(list['no_of_picture']);
                $('#menu_name').val(list['menu_name']);
                $('#menu_url').val(list['menu_url']);
                $('#menu_order').val(list['menu_order']);
                    $('#menu_type option').each(function(){
                       if($(this).val() == list['mt_id']){
                            $(this).prop('selected', true);
                       }else{
                            $(this).prop('selected', false);
                       }
                    });
                $("#hddmenu_id").val(list['menu_id']);
                });
            }
        });
}
function delete_data(id){
    var r = confirm("Do you want to delete this record?");
    if(r== true){
        $.ajax({
            data:{
                id:id
            },
            type: "post",
            url: "<?php echo base_url()?>"+"admin/settings/deleteAdminMenu",
            success:function(data){
                location.reload();
                
                
            }
        });

    }else{
        return false;
    }
}
$(document).ready(function() {
    $('#datatable').DataTable();
    //Buttons examples
    var table = $('#datatable-buttons').DataTable({
        lengthChange: false,
        buttons: ['copy', 'excel', 'pdf', 'colvis']
    });

    table.buttons().container()
            .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
} );

</script>

</body>
</html>