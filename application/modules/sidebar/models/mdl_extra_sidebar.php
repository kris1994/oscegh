<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_extra_sidebar extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    public function GetPermittedMenuItem($type,$user_id) {
        $this->db->order_by('gh_menu.menu_order', 'ASC');
        $this->db->where('gh_menu.mt_id',$type);
        $this->db->where('gh_user.u_id',$user_id);
        $this->db->where('gh_userd.u_view','1');
        $this->db->from('gh_menu');
        $this->db->join('gh_userd','gh_menu.menu_id=gh_userd.menu_id');
        $this->db->join('gh_user','gh_user.u_id=gh_userd.u_id');
        $result = $this->db->get()->result_array();
        return $result;
    }

    private function debug($msg="", $exit = false)
    {
        $today = date("Y-m-d H:i:s");

        if (is_array($msg) || is_object($msg))
        {
            echo "<hr>DEBUG ::[".$today."]<pre>\n";
            print_r($msg);
            echo "\n</pre><hr>";
        }
        else
        {
            echo "<hr>DEBUG ::[".$today."] $msg <hr>\n";
        }

        if ($exit) {
            $this->load->library('profiler');
            echo $this->profiler->run();
            exit;
        }
    }
    
}