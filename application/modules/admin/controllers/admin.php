<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin extends MX_Controller{

	function __construct() {
        parent::__construct();
        $this->load->model('mdl_general');
        $this->load->library('session');
    }
    function index(){
        $main_title=$this->mdl_general->GetInfoByRow('gh_configuration','config_id',array('config_id'=>1));
        $page_details['page_title']= $main_title->website_title.' | Login';
        if($this->session->userdata('asess_logged_in')==true){
            redirect('admin/dashboard');
        }
        $this->load->view('login/login');
    }

    function checklogin(){

    	$username=trim($this->input->post('username'));
    	$password=trim($this->input->post('password'));
        $encrpt_pass = sha1($password);
    	$logindate = date("Y-m-d");
        $user=$this->mdl_general->GetInfoByRow('gh_user','u_id',array('u_username'=>$username,'u_password'=>$encrpt_pass));

        if($user){//user available in database
    		if($user->u_enabled !='1'){//is user is disabled?
    			redirect('admin/index?error=2');
    		}else{//do the login ,set the session and redirect dashboard
    			$session_data=array(
    				'asess_user_id'=>$user->u_id,
                    'asess_user_name'=>$user->u_name,
                    'asess_user_type'=>$user->user_type,
                    'asess_profile_pic'=>$user->profile_pic,
    				'asess_logged_in'=>true
    				);
                $form_data = array('u_loginfdate' => date('Y-m-d h:i:s'));
                $this->mdl_general->Manage('gh_user',$form_data, array('u_id' => $user->u_id),'');
    			$this->session->set_userdata($session_data);
    			redirect('admin/dashboard');
    		}
    	}else{//username not available in database
    		redirect('admin/index?error=1');
    	}
    }
    public function dashboard(){
        $main_title=$this->mdl_general->GetInfoByRow('gh_configuration','config_id',array('config_id'=>1));
        $page_details['page_title']= $main_title->website_title.' | Dashboard';
        if($this->session->userdata('asess_logged_in')==true){
            $this->load->view('admin_layout/header', $page_details);
            $this->load->view('admin_layout/sidebar');
            $this->load->view('dashboard/dashboard');
            $this->load->view('admin_layout/footer');
        }else{
            redirect('admin/index');
        }
    }
    function logout(){
    	//unsetting data from session and redirect to login page
        $form_data = array('u_logoutdate' => date('Y-m-d h:i:s'));
        $this->mdl_general->Manage('gh_user',$form_data, array('u_id' => $this->session->userdata('sess_user_id')),'');
    	$session_data=array(
                    'asess_user_id'=>$user->u_id,
                    'asess_user_name'=>$user->u_name,
                    'asess_user_type'=>$user->user_type,
                    'asess_logged_in'=>false
                    );
    	$this->session->set_userdata($session_data);

    	redirect('admin/login');
    }
}