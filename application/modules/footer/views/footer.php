    <!--footer -->
    <div class="footer">
        <div class="container">
            <div class="footer-top_wthree_agileits">
                <div class="col-md-3  footer-grid_w3ls_agile">
                    <div class="footer-title">
                        <h2>About Us</h2>
                    </div>
                    <div class="footer-text">
                        <p>We’re from GH Global Training Center Co. a seminar provider regarding of Pro Company’s Labor Law Management Seminar & Taxation Seminar/Accounting Services/Bookkeeping Services. We also provide an external service for company like HR Management Service, handling labor cases (NLRC/Dole), bookkeeping services and other mandatory compliance services. </p>
                    </div>
                </div>
                <div class="col-md-3  footer-grid_w3ls_agile">
                    <div class="footer-title">
                        <h3>Office Hour</h3>
                    </div>
                    <div class="footer-office-hour">
                        <ul>
                            <li class="hd">Opening Days :</li>
                            <li>Monday – Friday : 9am to 20 pm</li>
                            <li>Saturday : 9am to 17 pm</li>
                        </ul>
                        <ul>
                            <li class="hd">Vacations :</li>
                            <li>All Sundays</li>
                            <li>All Official Holidays</li>
                        </ul>
                    </div>
                </div>
               <div class="col-md-3  footer-grid_w3ls_agile">
                    <div class="footer-title">
                        <h3></span> Visit Us</h3>
                    </div>
                    <div class="footer-office-hour">
                        <ul>
                            <li class="hd"><span class="fa fa-home" aria-hidden="true" style="color:orange"> </span>  Address :</li>
                            <li><?php echo $main_title->address?></li>
                        </ul>
                        <ul>
                            <li class="hd"><span class="fa fa-envelope" aria-hidden="true" style="color:orange"></span> Mail :</li>
                            <li><a href="mailto:<?php echo $main_title->email?>"><?php echo $main_title->email?></a></li>
                        </ul>
                    </div>
                </div>
               
                <div class="col-md-3  footer-grid_w3ls_agile">
                    <div class="footer-title">
                        <h3>Subscribe</h3>
                    </div>
                    <p></p>
                    <form action="#" method="post" class="newsletter">
                        <input class="email" type="email" placeholder="Your email..." required="">
                        <button class="btn1"><i class="fa fa-envelope-o" aria-hidden="true"></i></button>
                    </form>

                </div>
                <div class="clearfix"></div>
            </div>

        </div>
    </div>
    <!-- //footer -->
    <!-- copyright -->
    <div class="copyright">
        <div class="container">
            <div class="copyrighttop">
                <ul>
                    <li>
                        <h4>Follow us on:</h4>
                    </li>
                    <li><a class="facebook" href="https://www.facebook.com/pg/GHGLOBALTRAININGCENTERCO/photos/?ref=page_internal" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a class="facebook" href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a class="facebook" href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                    <li><a class="facebook" href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                </ul>
            </div>
            <div class="copyrightbottom">
                <p>© 2018 GH-Global Training Center</p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- //copyright -->


    <!-- js -->
    <script type="text/javascript" src="<?php echo base_url('assets_services/js/jquery-2.2.3.min.js')?>"></script>
    <!-- //js -->
    <!--search-bar-->
    <script src="<?php echo base_url('assets_services/js/main.js')?>"></script>
    <!--//search-bar-->
    <script>
        $('ul.dropdown-menu li').hover(function () {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
        }, function () {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
        });
    </script>

    <!-- stats -->
    <script src="<?php echo base_url('assets_services/js/jquery.waypoints.min.js')?>"></script>
    <script src="<?php echo base_url('assets_services/js/jquery.countup.js')?>"></script>
    <script>
        $('.counter').countUp();
    </script>
    <!-- //stats -->


    <script src="<?php echo base_url('assets_services/js/responsiveslides.min.js')?>"></script>
    <script>
        $(function () {
            $("#slider4").responsiveSlides({
                auto: true,
                pager: true,
                nav: true,
                speed: 1000,
                namespace: "callbacks",
                before: function () {
                    $('.events').append("<li>before event fired.</li>");
                },
                after: function () {
                    $('.events').append("<li>after event fired.</li>");
                }
            });
        });
    </script>
    <!-- script for responsive tabs -->
    <script src="<?php echo base_url('assets_services/js/easy-responsive-tabs.js')?>"></script>
    <script>
        $(document).ready(function () {
            $('#horizontalTab').easyResponsiveTabs({
                type: 'default', //Types: default, vertical, accordion           
                width: 'auto', //auto or any width like 600px
                fit: true, // 100% fit in a container
                closed: 'accordion', // Start closed if in accordion view
                activate: function (event) { // Callback function if tab is switched
                    var $tab = $(this);
                    var $info = $('#tabInfo');
                    var $name = $('span', $info);
                    $name.text($tab.text());
                    $info.show();
                }
            });
            $('#verticalTab').easyResponsiveTabs({
                type: 'vertical',
                width: 'auto',
                fit: true
            });
        });
    </script>
    <!--// script for responsive tabs -->
    <!-- start-smoth-scrolling -->
    <script type="text/javascript" src="<?php echo base_url('assets_services/js/move-top.js')?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets_services/js/easing.js')?>"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();
                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 900);
            });
        });
    </script>
    <!-- start-smoth-scrolling -->

    <script type="text/javascript">
        $(document).ready(function () {
            /*
                                    var defaults = {
                                        containerID: 'toTop', // fading element id
                                        containerHoverID: 'toTopHover', // fading element hover id
                                        scrollSpeed: 1200,
                                        easingType: 'linear' 
                                    };
                                    */

            $().UItoTop({
                easingType: 'easeOutQuart'
            });

        });
    </script>
    <a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
    <script type="text/javascript" src="<?php echo base_url('assets_services/js/bootstrap-3.1.1.min.js')?>"></script>


</body>

</html>