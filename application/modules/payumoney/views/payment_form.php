<?php
// Merchant key here as provided by Payu
$MERCHANT_KEY = "oYryQDGF";
// Merchant Salt as provided by Payu
$SALT = "LoLtoBB1qa";
// End point - change to https://secure.payu.in for LIVE mode
$PAYU_BASE_URL = "https://sandboxsecure.payu.in/_payment";

$action = '';

$posted = array();
//Generate random transaction id
$txnid = random_string('numeric', 5);
if(!empty($_POST)) {
        
        $posted['amount'] =$this->input->post('amount');
        $posted['phone'] = $this->input->post('phone');
        $posted['firstname'] = $this->input->post('firstname');
        $posted['email'] = $this->input->post('email');
        $posted['key'] = $MERCHANT_KEY;
        $posted['txnid'] = $this->input->post('hddTransactioID');
        $posted['productinfo'] = 'This is a Test Product';
        $posted['surl'] = base_url("payumoney/success");
        $posted['furl'] = base_url("payumoney/error");
        $posted['curl'] = base_url("payumoney/cancel");
        $posted['service_provider'] = 'payu_paisa';
}

$hash = '';
// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";

if(empty($posted['hash']) && sizeof($posted) > 0) {
    if(
          empty($posted['key'])
          || empty($posted['txnid'])
          || empty($posted['amount'])
          || empty($posted['firstname'])
          || empty($posted['email'])
          || empty($posted['phone'])
          || empty($posted['productinfo'])
          || empty($posted['surl'])
          || empty($posted['furl'])
          || empty($posted['service_provider'])
      ) {
        //echo "Fail";
        redirect('payumoney/');
      }
    else{
        $hashVarsSeq = explode('|', $hashSequence);
        $hash_string = '';
        foreach($hashVarsSeq as $hash_var){
              $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
              $hash_string .= '|';
        }

        $hash_string .= $SALT;
        $hash = strtolower(hash('sha512', $hash_string));
        $posted['hash'] = $hash;
        $action = $PAYU_BASE_URL . '/_payment';
    }
}
elseif(!empty($posted['hash'])){
  $hash = $posted['hash'];
  $action = $PAYU_BASE_URL . '/_payment';
}

?>
<!DOCTYPE html>
<html>
<head>
<title>Payment </title>
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
 <style type="text/css">  
#payUmoney { width: 150px; height: 150px; background-image: url(<?php echo base_url()?>assets_services/images/payUmoneyIcon.png); background-repeat: no-repeat; background-size: 100%; top: -50px
}
 </style>
<script>
    var hash = '<?php echo $hash ?>';
    function submitPayuForm() {
     if(hash == '') {
        return;
      }
      var payuForm = document.forms.payuForm;
      payuForm.submit();
    }
 </script>
</head>
<body onload="submitPayuForm()">
    <div class="header" id="home">
        <?php  $this->load->Module('sidebar')->index();?>
        <!--top-bar-w3-agile-->
        <!--//top-bar-w3-agile-->
    </div><!--/inner_banner-->
    <div class="inner_banner">
    </div>
    <!--//inner_banner-->
    <!--/short-->
    <div class="services-breadcrumb">
        <div class="inner_breadcrumb">

            <ul class="short">
                <li><a href="index.html">Home</a><span>|</span></li>
                <li>Payment</li>
            </ul>
        </div>
    </div>
    <div class="banner_bottom">
    <div class="container">
        <div class="tittle_head">
            <h3 class="tittle">Payment Form</h3>
        </div>
        <div class="inner_sec_info_wthree_agile">
            <div class="">
                <div class="blog_img single">
                    <div class="contact-form left_bar">
                        <?php
                            if($this->session->flashdata('msg_error')){
                                echo "<div class='alert alert-danger' role='alert'>".$this->session->flashdata('msg_error')."<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
                            }
                            elseif($this->session->flashdata('msg_success')){
                                echo "<div class='alert alert-success' role='alert'>".$this->session->flashdata('msg_success')."<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
                            }
                        ?>
                        <h3>Payment <span>Details</span></h3>
                        <form method="post" action="<?php echo $action?>" name="payuForm" id="payuForm">
                            <input type="hidden" name="key" value="<?php echo (!isset($posted['key'])) ? '' : $posted['key'] ?>" />
                            <input type="hidden" id="hash" name="hash" value="<?php echo (!isset($posted['hash'])) ? '' : $posted['hash'] ?>"/>
                            <input type="hidden" name="txnid" value="<?php echo (!isset($posted['txnid'])) ? '' : $posted['txnid'] ?>" />

                            <input type="hidden" name="productinfo" id="productinfo" value="<?php echo (!isset($posted['productinfo'])) ? '' : $posted['productinfo'] ?>">
                            <input type="hidden" name="surl" value="<?php echo (!isset($posted['surl'])) ? '' : $posted['surl'] ?>" size="64" />
                            <input type="hidden" name="curl" value="<?php echo (!isset($posted['curl'])) ? '' : $posted['curl'] ?>" size="64" />
                            <input type="hidden" id="furl" name="furl" value="<?php echo (!isset($posted['furl'])) ? '' : $posted['furl'] ?>" size="64" />
                            <input type="hidden" name="service_provider" value="<?php echo (!isset($posted['service_provider'])) ? '' : $posted['service_provider'] ?>" size="64" />
                            <div class="left_form">
                                <div>
                                    <span><label>Company Name</label></span>
                                    <span><input type="text" name="firstname" id="av_company_name" readonly="readonly" value="<?php echo @$getUserLogDetails->u_name?>"></span>
                                </div>
                                <div>
                                    <span><label>Address</label></span>
                                    <span><input type="text" name="av_address" id="av_address" readonly="readonly" value="<?php echo @$getUserLogDetails->u_name?>"></span>
                                </div>
                                <div>
                                    <span><label>Contact No.</label></span>
                                    <span><input type="text" name="phone" id="av_contact_no" readonly="readonly" value="<?php echo @$getUserLogDetails->contact_no?>"></span>
                                </div>
                                <div>
                                    <span><label>Email</label></span>
                                    <span><input type="text" name="email" id="av_email" readonly="readonly" value="<?php echo @$getUserLogDetails->u_email?>"></span>
                                </div>
                                <div>
                                    <span><label>Mobile No.</label></span>
                                    <span><input type="text" name="av_mobile_no" id="av_mobile_no" readonly="readonly" value="<?php echo @$getUserLogDetails->u_cellno?>"></span>
                                </div>
                                <div>
                                    <span><label>Company TIN #</label></span>
                                    <span><input type="text" name="av_company_tin_no" id="av_company_tin_no" readonly="readonly" value="<?php echo @$getUserLogDetails->contact_no?>"></span>
                                </div>
                                <div>
                                    <img src="<?php echo base_url()?>assets_services/images/payUmoney_services.jpg" alt="PayUmoney Services" style="width:550px;">
                                </div>
                            </div>
                            <div class="right_form">
                                <div>
                                    <span><label>Services</label></span>
                                    <span><input type="text" name="av_services" id="av_amount" value="<?php echo @$list->services_name?>" readonly="readonly" style="color:blue; font-weight: bold"></span>
                                </div>
                                <div>
                                    <span><label>Venue</label></span>
                                    <span><textarea name="Venue" readonly="readonly" style="height: 127px"><?php echo $list->venue?></textarea></span>
                                </div>
                                <div>
                                    <span><label>Sched Date</label></span>
                                    <span><input type="text" name="scheddDate" id="scheddDate" readonly="readonly" value="<?php echo @$list->dateFrom.' - '.@$list->dateTo?>"></span>
                                </div>
                                <div>
                                    <span><label>Sched Time</label></span>
                                    <span><input type="text" name="schedTime" id="schedTime" readonly="readonly" value="<?php echo @$list->timeStart.' - '.@$list->timeEnd?>"></span>
                                </div>
                                <div>
                                    <span><label>Amount (PHP)</label></span>
                                    <span><input type="text" name="amount"  id="amount" value="<?php echo @$payment_list->amount;?>" readonly="readonly" style="color:blue; font-weight: bold"></span>
                                </div>
                                <div style="float:right;">
                                    <input type="hidden" name="hddTransactioID" value="<?php echo $payment_list->transaction_id?>">
                                    <input type="hidden" name="hddServicesID" value="<?php echo $list->services_id?>">
                                    <span style="color:red">Amount (PHP): <?php echo number_format(@$payment_list->amount,2);?> <button type="submit" name="btn_payment" id="payUmoney" class="btn btn-primary" ></button></span>
                                </div>
                                <!-- <hr> -->
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>

            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>
</body>
</html>