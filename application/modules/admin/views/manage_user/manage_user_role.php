<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<style type="text/css">
    .btn-file {
        position: relative;
        overflow: hidden;
        background-color: #4f5151;
        color: white;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }
    #img-upload{
        /*position: absolute;*/
        width: 100%;
    }
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">Manage User Role Page</h4>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="#">Settings</a></li>
                            <li class="breadcrumb-item active">Manage User Role</li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-0">Manage User Role</h4>
                        <form action="<?php echo base_url('admin/user_role_process')?>" method="post">
                            <div class="col-lg-5 m-t-10">
                                <div class="p-0">
                                    <table class="table table-hover">
                                        <tr>
                                            <th>
                                                <select name="menu_name">
                                                    <?php 
                                                    $menu=$this->mdl_general->GetAllInfo('gh_menu','menu_id');
                                                    foreach ($menu as $row) {?>
                                                    <option value="<?php echo $row['menu_id']?>"><?php echo $row['menu_name']?></option>
                                                    <?php } ?>
                                                </select>
                                            </th>
                                            <th>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="u_add" name="u_add" type="checkbox" value="1">
                                                    <label for="u_add">Add</label>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="u_edit" name="u_edit" type="checkbox" value="1">
                                                    <label for="u_edit">Edit</label>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="u_view" name="u_view" type="checkbox" value="1">
                                                    <label for="u_view">View</label>
                                                </div>
                                            </th>
                                            <th>
                                                <div class="checkbox checkbox-primary">
                                                    <input id="u_del" name="u_del" type="checkbox" value="1">
                                                    <label for="u_del">Del</label>
                                                </div>
                                            </th>
                                            <th>
                                                <input type="hidden" name="hddId" value="<?php echo $hddId?>">
                                                <button type="submit" style="">Add</button>
                                            </th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- <a href="<?php echo base_url('admin/add_user')?>" class="btn-sm btn-primary waves-effect waves-light"><i class="fa fa-plus"></i>  Add User</a> -->
                        <!-- </div> -->
                            <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Menu Name</th>
                                <th>Add</th>
                                <th>Edit</th>
                                <th>View</th>
                                <th>Delete</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                $counter=1;
                                foreach($list as $row){
                                ?>
                            <tr>
                                <td><?php echo $row['menu_name']?></td>
                                <td><?php echo $row['u_add']?></td>
                                <td><?php echo $row['u_edit']?></td>
                                <td><?php echo $row['u_view']?></td>
                                <td><?php echo $row['u_del']?></td>
                                <td>
                                    <a href="javascript:void(0)" onclick="delete_data('<?php echo $row["rec"]?>')" class="btn-sm btn-danger waves-effect waves-light"><i class="fa fa-trash"></i>  Delete</a>
                                </td>
                            </tr>
                            <?php $counter++; }?>
                            </tbody>
                        </table>
                    </div>
                </div><!-- end col -->
            </div>
            <!-- end row -->
        </div> <!-- container -->
    </div> <!-- content -->

</div>
<!-- End content-page -->
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
<?php echo $this->load->view('admin_layout/footer');?>
<script type="text/javascript">
$(document).ready(function() {
    $('#datatable').DataTable();
    //Buttons examples
    var table = $('#datatable-buttons').DataTable({
        lengthChange: false
    });

    table.buttons().container()
            .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
} );
function delete_data(id){
    var r = confirm("Do you want to delete this record?");
    if(r== true){
        $.ajax({
            data:{
                id:id
            },
            type: "post",
            url: "<?php echo base_url()?>"+"admin/settings/deleteUserRole",
            success:function(data){
                location.reload();
                
                
            }
        });

    }else{
        return false;
    }
}
</script>