<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class About extends MX_Controller{
	function __construct() {
        parent::__construct();
        $this->load->model('mdl_general');
        $this->load->model('mdl_extra_about');
        
    }

    function index(){
    	$main_title=$this->mdl_general->GetInfoByRow('gh_configuration','config_id',array('config_id'=>1));
    	$page_details['page_title']= $main_title->website_title.' | About';
        $this->load->Module('header')->index($page_details);
        // $this->load->Module('sidebar')->index();
        $wheremv = array('pg_status'=>1,'pg_image_type'=>'Mission & Vision');
        $data['mission_vision'] = $this->mdl_extra_about->getDetailsperTable('page_image', $wheremv, '', 2);
    	$this->load->view('about', $data);
    	$this->load->Module('footer')->index();
    }
}