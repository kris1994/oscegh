 <!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<style>
img {  width:100%; }
.btn {
    display: inline-block; font-weight: 400; line-height: 1.25; text-align: center; white-space: nowrap; vertical-align: middle; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; border: 1px solid transparent; padding: .2rem .5rem; font-size: 1rem; border-radius: .25rem; -webkit-transition: all .2s ease-in-out; -o-transition: all .2s ease-in-out; transition: all .2s ease-in-out;
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">Manage Services</h4>

                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="#">Services</a></li>
                            <li class="breadcrumb-item active">Manage Services</li>
                        </ol>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-12">
                    <div class="card-box table-responsive">
                        <h4 class="m-t-0 header-title"><b>Manage Services</b></h4>
                        <div class="card-block">
                            <a href="javascript:void(0)" onclick="add_data()" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-sm" class="btn btn-primary"><i class="fa fa-plus"></i> Add Services</a>
                        </div>
                        <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Services ID</th>
                                <th>Services Name</th>
                                <th>Description</th>
                                <th>Image</th>
                                <th>Price</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($list as $row) {
                                if($row['status']==1){
                                    $status = '<span class="label-success label label-default">Active</span>';
                                }else{
                                    $status = '<span class="label-default label">Inactive</span>';
                                }
                                $services_image = base_url('uploaded_image/services/'.$row['services_image']);
                            ?>
                            <tr>
                               <td><?php echo $row['services_id']?></td>
                               <td><?php echo $row['services_name']?></td>
                               <td><?php echo substr(strip_tags(rawurldecode($row['services_description'])),0,100)?></td>
                               <td class="nav-user"><img src="<?php echo $services_image?>" alt="user" class="rounded-circle"></td>
                               <td><?php echo $row['services_price']?></td>
                               <td><?php echo $status?></td>
                               <td>
                                   <a href="javascript:void(0)" onclick="edit_data('<?php echo $row["services_id"]?>')" class="btn-sm btn-info waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-sm"><i class="fa fa-edit"></i> </a>
                                   <a href="javascript:void(0)" onclick="delete_data('<?php echo $row["services_id"]?>')" class="btn-sm btn-danger waves-effect waves-light"><i class="fa fa-trash"></i></a>
                               </td>
                            </tr>
                            <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <!-- Modal -->
                <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <form role="form" method="post" action="<?php echo base_url('products/services_process')?>" autocomplete="off" data-parsley-validate novalidate enctype="multipart/form-data">
                        <div class="modal-content" style="width:1000px;">
                            <div class="modal-header">
                                <h5 class="modal-title" id="mySmallModalLabel">Add Services</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="col-xs-12">
                                     <div class="p-20">
                                        <div class="form-group row">
                                            <label for="services_name" class="col-sm-2 form-control-label">Services Name<span class="text-danger">*</span></label>
                                            <div class="col-sm-10">
                                                <input type="text" name="services_name" required class="form-control" id="services_name" placeholder="Services Name">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="Description" class="col-sm-2 form-control-label">Description<span class="text-danger">*</span></label>
                                            <div class="col-sm-10">
                                                <textarea id="description" name="description" id="description"></textarea> 
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="services_price" class="col-sm-2 form-control-label">Price<span class="text-danger">*</span></label>
                                            <div class="col-sm-4">
                                                <input type="number" name="services_price" required class="form-control" id="services_price" placeholder="Services Name" value="0">
                                            </div>
                                            <label for="services_type" class="col-sm-1 form-control-label">Services Type<span class="text-danger">*</span></label>
                                            <div class="col-sm-5">
                                               <select name="services_type" id="services_type" class="form-control">
                                                   <option value="Other Services">Other Services</option>
                                                   <option value="Services">Services</option>
                                               </select>
                                            </div>
                                        </div>
                                        <div id="ifServicesDiv" style="display: none">
                                            <div class="form-group row">
                                                <label for="venue" class="col-sm-2 form-control-label">Venue<span class="text-danger">*</span></label>
                                                <div class="col-sm-10">
                                                    <textarea name="venue" id="venue" class="form-control"></textarea> 
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="from_date" class="col-sm-2 form-control-label">From Date<span class="text-danger"></span></label>
                                                <div class="col-sm-10">
                                                    <div class="input-daterange input-group" id="date-range">
                                                        <input type="text" class="form-control" name="dateFrom" id="dateFrom">
                                                        <span class="input-group-addon bg-custom b-0">to</span>
                                                        <input type="text" class="form-control" name="dateTo" id="dateTo">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="timeStart" class="col-sm-2 form-control-label">Start Time</label>
                                                <div class="col-sm-4">
                                                    <div class="input-group">
                                                        <input id="timeStart" type="text" name="timeStart" class="form-control">
                                                        <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                                    </div>
                                                </div>
                                                <label for="timeEnd" class="col-sm-2 form-control-label">End Time</label>
                                                <div class="col-sm-4">
                                                    <div class="input-group">
                                                        <input id="timeEnd" type="text" name="timeEnd" class="form-control">
                                                        <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="slots" class="col-sm-2 form-control-label">Slots<span class="text-danger">*</span></label>
                                                <div class="col-sm-4">
                                                    <input type="number" name="set_slots" id="set_slots" class="form-control" value="100">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="services_image" class="col-sm-2 form-control-label">Picture<span class="text-danger">*</span></label>
                                            <div class="col-sm-5">
                                                <input type="file" name="services_image" class="dropify" id="services_image" data-height="172" />
                                                <input type="hidden" name="services_filename" id="services_filename">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-2">Active</label>
                                            <div class="col-sm-4">
                                                <div class="checkbox checkbox-primary">
                                                    <input id="status" name="status" type="checkbox" value="1">
                                                    <label for="status">Check me out</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="service_filename_img" id="service_filename_img">
                                <input type="hidden" name="hddSID" id="hddSID">
                                <input type="hidden" name="action" id="action" value="add">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" name="bnt_save" id="bnt_save" class="btn btn-primary">Save Services</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>

        </div> <!-- container -->

    </div> <!-- content -->

</div>
<!-- End content-page -->
<?php echo $this->load->view('admin_layout/footer');?>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
<script type="text/javascript">
$('#timeStart').timepicker({
    defaultTIme: false,
    icons: {
        up: 'zmdi zmdi-chevron-up',
        down: 'zmdi zmdi-chevron-down'
    }
});
$('#timeEnd').timepicker({
    defaultTIme: false,
    icons: {
        up: 'zmdi zmdi-chevron-up',
        down: 'zmdi zmdi-chevron-down'
    }
});
$('#date-range').datepicker({
        toggleActive: true
    });
$("#services_type").change(function(){
    if($(this).val()=='Services'){
        $("#ifServicesDiv").show();
    }else{
        $("#ifServicesDiv").hide();
    }
    
})
$(document).ready(function() {
     $('.dropify').dropify({
        messages: {
            'default': '',
            'replace': 'Drag and drop or click to replace',
            'remove': 'Remove',
            'error': 'Ooops, something wrong appended.'
        },
        error: {
            'fileSize': 'The file size is too big (1M max).'
        }
    });
    $("#description").Editor();
    $('#datatable').DataTable();
    //Buttons examples
    var table = $('#datatable-buttons').DataTable({
        lengthChange: false,
        buttons: ['copy', 'excel', 'pdf', 'colvis']
    });

    table.buttons().container().appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
});
function add_data(){
    $("#services_name").val("");
    $(".Editor-editor").html("");
    $(".dropify-preview").css("display","none");
    $("#status").prop('checked', false);
    $("#bnt_save").text("Add Services");
    $("#hddSID").val("");
    $("#service_filename_img").val("");
    $("#services_price").val("0");
    $("#services_filename").val('');
    $("#set_slots").val('');
    $(this).prop('selected', false);
}
function edit_data(id){
    $('#action').val('edit');
    $("#mySmallModalLabel").text('Edit Services');
    $("#bnt_save").val('Update Admin Menu');
        $.ajax({
            data:{
            id:id
            },
            type: "post",
            url: "<?php echo base_url()?>"+"admin/product/getServicesList",
            dataType: "json",
            success: function(data) {
                jQuery.each(data, function(key, list) {
                    // alert(list['services_description']);
                    var _url = "<?php echo base_url()?>";
                    var services_image = _url+"uploaded_image/services/"+list['services_image'];
                    $("#services_name").val(list['services_name']);
                    $(".Editor-editor").html(unescape(list['services_description']));
                    // $("#services_image").attr('data-default-file');
                    $(".dropify-preview").css("display","block");
                    $(".dropify-render").html('<img src="'+services_image+'" style="max-height: 172px;">')
                    if(list['status']==1){
                        $("#status").prop('checked', true);
                    }else{
                        $("#status").prop('checked', false);
                    }
                    $("#services_price").val(list['services_price']);
                    $("#bnt_save").text("Update Services");
                    $("#hddSID").val(list['services_id']);
                    $("#service_filename_img").val(list['services_image']);
                    $("#services_filename").val(list['services_image']);

                    $("#venue").val(list['venue']);
                    $("#dateFrom").val(list['dateFrom']);
                    $("#dateTo").val(list['dateTo']);
                    $("#timeStart").val(list['timeStart']);
                    $("#timeEnd").val(list['timeEnd']);
                    $("#set_slots").val(list['services_slots']);
                    $("#services_type option").each(function(){
                        if($(this).text() == list['services_type']){
                            $(this).prop('selected', true);
                        }else{
                            $(this).prop('selected', false);
                        }
                    })
                    if(list['services_type'] =='Services'){
                        $("#ifServicesDiv").show();
                    }else{
                        $("#ifServicesDiv").hide();
                    }
                });
            }
        });
}
$('#bnt_save').click(function(){
    $('.Editor-editor').each(function() {
        $('#description').val(escape($(this).html()));
    });
});
function delete_data(id){
    var r = confirm("Do you want to delete this record?");
    if(r== true){
        $.ajax({
            data:{
                id:id
            },
            type: "post",
            url: "<?php echo base_url()?>"+"admin/product/deleteServices",
            success:function(data){
                location.reload();
                
                
            }
        });

    }else{
        return false;
    }
}
</script>

</body>
</html>