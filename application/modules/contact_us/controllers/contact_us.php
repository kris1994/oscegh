<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Contact_us extends MX_Controller{
	function __construct() {
        parent::__construct();
        $this->load->model('mdl_general');
        
        
    }

    function index(){
    	$main_title=$this->mdl_general->GetInfoByRow('gh_configuration','config_id',array('config_id'=>1));
    	$page_details['page_title']= $main_title->website_title.' | Contact Us';
        $this->load->Module('header')->index($page_details);
        // $this->load->Module('sidebar')->index();
    	$this->load->view('contact_us');
    	$this->load->Module('footer')->index();
    }
    function saveContact(){
        $config_details=$this->mdl_general->GetInfoByRow('gh_configuration','config_id',array('config_id'=>1));
        $c_name = $this->input->post('c_name');
        $c_email = $this->input->post('c_email');
        $c_subject = $this->input->post('c_subject');
        $c_message = $this->input->post('c_message');
        if(!empty($c_email)){
            $config['useragent']  = "CodeIgniter";
            $config['mailpath']  = "/usr/sbin/sendmail"; // or "/usr/sbin/sendmail"
            $config['protocol']   = "sendmail";
            $config['mailtype'] = 'html';
            $config['charset']  = 'utf-8';
            $config['newline']  = "\r\n";
            $config['wordwrap'] = FALSE; 
            $config['validate']=TRUE;

            $this->load->library('email');

            $this->email->from("$c_email", "$c_name");
            $this->email->to("$config_details->email");

            $this->email->subject("$c_subject");
            $this->email->message($c_message);

            $this->email->send();
        }
        $form_data = array(
            'c_name' => $this->input->post('c_name'),
            'c_email' => $this->input->post('c_email'),
            'c_subject' => $this->input->post('c_subject'),
            'c_message' => $this->input->post('c_message'),
        );
        $res = $this->mdl_general->SaveForm('gh_contact_us',$form_data);
        if($res){
            echo "Success";
        }else{
            echo "Failed";
        }
    }
}