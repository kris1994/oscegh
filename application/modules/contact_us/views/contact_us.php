    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      /*html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }*/
    </style>
    <!--Header-->
    <div class="header" id="home">
        <?php  $this->load->Module('sidebar')->index();?>
        <!--top-bar-w3-agile-->
        <!--//top-bar-w3-agile-->
    </div>
    <!--/inner_banner-->
    <div class="inner_banner">
    </div>
    <!--//inner_banner-->
    <!--/short-->
    <div class="services-breadcrumb">
        <div class="inner_breadcrumb">

            <ul class="short">
                <li><a href="index.html">Home</a><span>|</span></li>
                <li>Contact Us</li>
            </ul>
        </div>
    </div>
    <!--//short-->
    <!-- /inner_content -->
    <div class="banner_bottom">
        <div class="container">
            <div class="tittle_head">
                <h3 class="tittle">Contact <span>Us </span></h3>
            </div>
            <div class="inner_sec_info_wthree_agile">
                <div class="col-md-8 map">
                    <div id="map"></div>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3861.345369560966!2d121.05403601439623!3d14.579385689815947!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c83ffc736833%3A0xaeb31d0c39fe147b!2sCityland+Shaw+Tower!5e0!3m2!1sen!2sph!4v1518056024429" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="col-md-4 contact_grids_info">
                    <div class="contact_grid">
                        <div class="contact_grid_right">
                            <h4> COMPANY NAME</h4>
                            <p>GH GLOBAL TRAINING CENTER CO.</p>
                            <p>Labor consulting HR Outsourcing (Tax and Labor Seminar)</p>
                        </div>
                    </div>
                    <div class="contact_grid">
                        <div class="contact_grid_right">
                            <h4> Address</h4>
                            <p>Room 1015 10th Flr. Cityland Shaw Tower Shaw Blvd. Mandaluyong City</p>
                            <!-- <p></p> -->
                        </div>
                    </div>
                    <div class="contact_grid" data-aos="flip-up">

                        <div class="contact_grid_right">
                            <h4>Contact & Email</h4>
                            <p>Tel No.: 02-5084655</p>
                            <p>Email : highway54accounting@yahoo.com</p>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="clearfix"> </div>
            </div>

            <div class="mail_form">
                <h3 class="tittle mail">Send Us <span>a Message </span></h3>
                <div class="inner_sec_info_wthree_agile">
                    <!-- <form action="#" method="post"> -->
                        <span class="input input--chisato">
                        <input class="input__field input__field--chisato" name="Name" type="text" id="input-13" placeholder=" " required="" value="<?php echo $this->session->userdata('sess_user_name')?>" />
                        <label class="input__label input__label--chisato" for="input-13">
                            <span class="input__label-content input__label-content--chisato" data-content="Name">Name</span>
                        </label>
                        </span>
                        <span class="input input--chisato">
                        <input class="input__field input__field--chisato" name="Email" type="email" id="input-14" placeholder=" " required="" value="<?php echo $this->session->userdata('sess_user_email')?>"/>
                        <label class="input__label input__label--chisato" for="input-14">
                            <span class="input__label-content input__label-content--chisato" data-content="Email">Email</span>
                        </label>
                        </span>
                        <span class="input input--chisato">
                        <input class="input__field input__field--chisato" name="Subject" type="text" id="input-15" placeholder=" " required="" />
                        <label class="input__label input__label--chisato" for="input-15">
                            <span class="input__label-content input__label-content--chisato" data-content="Subject">Subject</span>
                        </label>
                        </span>
                        <textarea name="Message" id="Message" placeholder="Your comment here..." required=""></textarea>
                        <input type="submit" value="Submit" id="btn_submit">
                    <!-- </form> -->

                </div>
            </div>
            <div class="clearfix"> </div>

        </div>
    </div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script type="text/javascript">
    $("#btn_submit").click(function(e){
        var _base_url = "<?php echo base_url()?>";
        e.preventDefault();
        var _name = $("#input-13").val();
        var _email = $("#input-14").val();
        var _subject = $("#input-15").val();
        var _message = $("#Message").val();
        $.ajax({
            type:'POST',
            data:{
                c_name : _name,
                c_email : _email,
                c_subject : _subject,
                c_message : _message,
            },
            url: _base_url+'contact_us/saveContact',
            success : function(_result){
                // console.log(_result);
                if(_result=='Success'){
                    alert('Send Message');
                    location.reload();
                }else{
                    alert('Failed to Send');
                }
            }
        })
    })
</script>