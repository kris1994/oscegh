<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Footer extends MX_Controller{
	function __construct() {
        parent::__construct();
        
    }

    function index(){
    	$data['list'] = $this->mdl_general->GetAllInfo('services','services_id',array('status'=>1,'services_type'=>'Other Services'));
    	$data['main_title']=$this->mdl_general->GetInfoByRow('gh_configuration','config_id',array('config_id'=>1));
    	$this->load->view('footer',$data);
    }
}