<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_extra_services extends CI_Model {

	public function getDetailsperTable($table, $where, $group_by, $sort_by){
		$this->db->select('*');
		$this->db->from($table);
		if(!empty($where)){
			$this->db->where($where);
		}
		if(!empty($group_by)){
			$this->db->group_by($group_by);
		}
		if(!empty($order_by)){
			$this->db->order_by($order_by);	
		}
		
		return $this->db->get()->result_array();
	}
	public function getAvailableSlots($where=null){
		$query = $this->db->query("SELECT ifnull(count(pl.services_id),0) as avail_slots
				FROM `payment_list` pl
				LEFT JOIN services s ON pl.services_id = s.services_id
				WHERE date_format(pl.createdatetime,'%Y-%m-%d') <= STR_TO_DATE(s.dateTo, '%m/%d/%Y')
				$where
				group by pl.services_id")->row();
		return $query;
	}
}