-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2018 at 06:08 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oscegh`
--
CREATE DATABASE IF NOT EXISTS `oscegh` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `oscegh`;

-- --------------------------------------------------------

--
-- Table structure for table `evaluation_list`
--

CREATE TABLE `evaluation_list` (
  `el_id` int(11) NOT NULL,
  `el_name` text NOT NULL,
  `el_order` int(11) NOT NULL,
  `et_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `evaluation_list`
--

INSERT INTO `evaluation_list` (`el_id`, `el_name`, `el_order`, `et_id`) VALUES
(1, 'The seminar was easy to register for.', 1, 1),
(2, 'The seminar location was convenient.', 2, 1),
(3, 'The lecturer’s presentation style was effective.', 1, 2),
(4, 'The lecturer demonstrated enthusiasm for the subject matter.', 2, 2),
(5, 'The lecturer was well prepared and organized.', 3, 1),
(6, 'Arrangement of room was adequate.', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `evaluation_type`
--

CREATE TABLE `evaluation_type` (
  `et_id` int(11) NOT NULL,
  `et_name` varchar(50) NOT NULL,
  `et_order` tinyint(3) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `evaluation_type`
--

INSERT INTO `evaluation_type` (`et_id`, `et_name`, `et_order`, `status`) VALUES
(1, 'SEMINAR CONTENT:', 1, 1),
(2, 'LECTURER:', 2, 1),
(3, 'FACILITIES AND EQUIPMENT:', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `gh_configuration`
--

CREATE TABLE `gh_configuration` (
  `config_id` int(11) NOT NULL,
  `website_title` varchar(30) NOT NULL,
  `address` varchar(150) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `fax` varchar(15) NOT NULL,
  `email` varchar(254) NOT NULL,
  `website` varchar(30) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `approval_disabled` tinyint(2) NOT NULL,
  `signatureimage` varchar(255) NOT NULL,
  `config_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gh_configuration`
--

INSERT INTO `gh_configuration` (`config_id`, `website_title`, `address`, `phone`, `fax`, `email`, `website`, `logo`, `approval_disabled`, `signatureimage`, `config_date`) VALUES
(1, 'GH-Ordering System and Custome', '18 Holybrook Street, Glasgow , G42 7EH', '07854327040', '', 'info@onyxtech.co.uk', 'www.onyxtech.co.uk', 'favicon.ico', 1, 'RajakSign.jpg', '2014-08-23');

-- --------------------------------------------------------

--
-- Table structure for table `gh_menu`
--

CREATE TABLE `gh_menu` (
  `menu_id` int(11) NOT NULL,
  `menu_name` varchar(50) NOT NULL,
  `menu_url` varchar(50) NOT NULL,
  `menu_order` int(11) NOT NULL,
  `mt_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gh_menu`
--

INSERT INTO `gh_menu` (`menu_id`, `menu_name`, `menu_url`, `menu_order`, `mt_id`) VALUES
(1, 'Admin menu', 'admin_menu', 1, 3),
(2, 'Manage Products', 'manage_products', 1, 2),
(3, 'Manage Services', 'manage_services', 2, 2),
(4, 'Manage User', 'manage_user', 2, 3),
(5, 'Manage Evaluation Form', 'evaluation_list', 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `gh_menutype`
--

CREATE TABLE `gh_menutype` (
  `mt_id` int(11) NOT NULL,
  `mt_name` varchar(50) NOT NULL,
  `mt_url` varchar(45) NOT NULL,
  `menu_icon` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gh_menutype`
--

INSERT INTO `gh_menutype` (`mt_id`, `mt_name`, `mt_url`, `menu_icon`) VALUES
(1, 'Dashboard', 'dashboard', 'zmdi zmdi-view-dashboard'),
(2, 'Products Category', 'product', 'zmdi zmdi-collection-item'),
(3, 'Settings', 'settings', 'zmdi zmdi-settings-square');

-- --------------------------------------------------------

--
-- Table structure for table `gh_user`
--

CREATE TABLE `gh_user` (
  `u_id` int(11) NOT NULL,
  `u_username` varchar(50) NOT NULL,
  `u_password` varchar(50) NOT NULL,
  `u_name` varchar(50) NOT NULL,
  `u_email` varchar(40) NOT NULL,
  `u_remarks` varchar(200) NOT NULL,
  `u_cellno` varchar(15) NOT NULL,
  `u_address` text NOT NULL,
  `u_loginfdate` datetime NOT NULL,
  `u_logoutdate` datetime NOT NULL,
  `u_enabled` tinyint(2) NOT NULL,
  `user_type` varchar(20) NOT NULL,
  `profile_pic` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gh_user`
--

INSERT INTO `gh_user` (`u_id`, `u_username`, `u_password`, `u_name`, `u_email`, `u_remarks`, `u_cellno`, `u_address`, `u_loginfdate`, `u_logoutdate`, `u_enabled`, `user_type`, `profile_pic`) VALUES
(3, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'admin', 'admin@gmail.com', 'admin', '', '', '2018-01-22 04:54:03', '2018-01-04 04:30:35', 1, 'admin', ''),
(21, 'guest', '084e0343a0486ff05530df6c705c8bb4', 'guest', 'test@gmail.com', 'guest', '902390234', '', '2017-10-22 00:00:00', '2017-10-22 00:00:00', 1, 'customer', ''),
(22, 'kristian', 'd41d8cd98f00b204e9800998ecf8427e', 'Kristian Rafael Claridad', 'kristianrafael.claridad@gmail.com', '', '099323231', 'Philippines', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', '5a642c3a3bd7d.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `gh_userd`
--

CREATE TABLE `gh_userd` (
  `rec` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `u_add` tinyint(2) NOT NULL,
  `u_edit` tinyint(2) NOT NULL,
  `u_del` tinyint(2) NOT NULL,
  `u_view` tinyint(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gh_userd`
--

INSERT INTO `gh_userd` (`rec`, `u_id`, `menu_id`, `u_add`, `u_edit`, `u_del`, `u_view`) VALUES
(1, 3, 1, 1, 1, 1, 1),
(2, 21, 1, 1, 1, 1, 1),
(3, 3, 2, 1, 1, 1, 1),
(4, 3, 3, 1, 1, 1, 1),
(5, 3, 4, 1, 1, 1, 1),
(9, 3, 5, 1, 1, 1, 1),
(8, 22, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `services_id` int(11) NOT NULL,
  `services_name` varchar(70) NOT NULL,
  `services_description` text NOT NULL,
  `services_image` varchar(50) NOT NULL,
  `services_price` int(30) NOT NULL,
  `services_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`services_id`, `services_name`, `services_description`, `services_image`, `services_price`, `services_created`, `status`) VALUES
(1, 'Accounting Services', '%3Cp%20class%3D%22MsoNormal%22%3E1.%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%20%3Cspan%20style%3D%22font-weight%3A%20bold%3B%20font-style%3A%20italic%3B%20text-decoration-line%3A%20underline%3B%22%3EReview%0Aand%20analysis%20of%20accounting%20entries%20prepared%20by%20Client%20and%20perform%20necessary%0Aadjustments%2C%20if%20any%2C%20on%20the%20following%20books%20of%20accounts%3A%26nbsp%3B%20%3C/span%3E%3Co%3Ap%3E%3C/o%3Ap%3E%3C/p%3E%0A%0A%3Cp%20class%3D%22MsoNormal%22%3E%u2022%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%20Cash%20receipts%0Ajournal%3Co%3Ap%3E%3C/o%3Ap%3E%3C/p%3E%0A%0A%3Cp%20class%3D%22MsoNormal%22%3E%u2022%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%20Cash%0Adisbursements%20journal%20%3Co%3Ap%3E%3C/o%3Ap%3E%3C/p%3E%0A%0A%3Cp%20class%3D%22MsoNormal%22%3E%u2022%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%20Sales%0AJournal%20%3Co%3Ap%3E%3C/o%3Ap%3E%3C/p%3E%0A%0A%3Cp%20class%3D%22MsoNormal%22%3E%u2022%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%20Purchases%0AJournal%3Co%3Ap%3E%3C/o%3Ap%3E%3C/p%3E%0A%0A%3Cp%20class%3D%22MsoNormal%22%3E%u2022%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%20Subsidiary%0AJournals%3Co%3Ap%3E%3C/o%3Ap%3E%3C/p%3E%0A%0A%3Cp%20class%3D%22MsoNormal%22%3E%u2022%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%20General%0AJournal%3Co%3Ap%3E%3C/o%3Ap%3E%3C/p%3E%0A%0A%3Cp%20class%3D%22MsoNormal%22%3E%u2022%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%20Ledger%3Co%3Ap%3E%3C/o%3Ap%3E%3C/p%3E%0A%0A%3Cp%20class%3D%22MsoNormal%22%3E2.%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%20Review%0Amonthly%20financial%20statements%20%28FS%29%20and%20analytics%20prepared%20by%20client%3A%3Co%3Ap%3E%3C/o%3Ap%3E%3C/p%3E%0A%0A%3Cp%20class%3D%22MsoNormal%22%3E%u2022%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%20Monthly%0ABalance%20Sheet%3Co%3Ap%3E%3C/o%3Ap%3E%3C/p%3E%0A%0A%3Cp%20class%3D%22MsoNormal%22%3E%u2022%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%20Monthly%0AIncome%20Statement%3Co%3Ap%3E%3C/o%3Ap%3E%3C/p%3E%0A%0A%3Cp%20class%3D%22MsoNormal%22%3E%u2022%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%20Monthly%0AStatement%20of%20Changes%20in%20Equity%3Co%3Ap%3E%3C/o%3Ap%3E%3C/p%3E%0A%0A%3Cp%20class%3D%22MsoNormal%22%3E%u2022%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%26nbsp%3B%20Other%0Amonthly%20reports%20and%20schedules%3Co%3Ap%3E%3C/o%3Ap%3E%3C/p%3E', '5a6485a5527e2.png', 0, '2018-01-21 12:20:53', 1),
(4, 'Bookkeeping Services', '%3Cspan%20style%3D%22font-size%3A%2014.4px%3B%20font-weight%3A%20bold%3B%20font-style%3A%20italic%3B%20text-decoration-line%3A%20underline%3B%22%3EBookkeeping%20Services%3C/span%3E', '5a648b6eb8c63.png', 1200, '2018-01-21 12:45:34', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `evaluation_list`
--
ALTER TABLE `evaluation_list`
  ADD PRIMARY KEY (`el_id`);

--
-- Indexes for table `evaluation_type`
--
ALTER TABLE `evaluation_type`
  ADD PRIMARY KEY (`et_id`);

--
-- Indexes for table `gh_configuration`
--
ALTER TABLE `gh_configuration`
  ADD PRIMARY KEY (`config_id`);

--
-- Indexes for table `gh_menu`
--
ALTER TABLE `gh_menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `gh_menutype`
--
ALTER TABLE `gh_menutype`
  ADD PRIMARY KEY (`mt_id`);

--
-- Indexes for table `gh_user`
--
ALTER TABLE `gh_user`
  ADD PRIMARY KEY (`u_id`);

--
-- Indexes for table `gh_userd`
--
ALTER TABLE `gh_userd`
  ADD PRIMARY KEY (`rec`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`services_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `evaluation_list`
--
ALTER TABLE `evaluation_list`
  MODIFY `el_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `evaluation_type`
--
ALTER TABLE `evaluation_type`
  MODIFY `et_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `gh_configuration`
--
ALTER TABLE `gh_configuration`
  MODIFY `config_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `gh_menu`
--
ALTER TABLE `gh_menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `gh_menutype`
--
ALTER TABLE `gh_menutype`
  MODIFY `mt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `gh_user`
--
ALTER TABLE `gh_user`
  MODIFY `u_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `gh_userd`
--
ALTER TABLE `gh_userd`
  MODIFY `rec` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `services_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
