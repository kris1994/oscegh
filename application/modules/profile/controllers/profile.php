<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Profile extends MX_Controller{

	function __construct() {
        parent::__construct();
        $this->load->model('mdl_general');
        $this->load->library('session');
        //$this->load->library('facebook'); 
        $this->load->library('user_agent');
        if(!$this->session->userdata('sess_logged_in')){
            redirect('login');
        }
    }
    function index(){
        $main_title=$this->mdl_general->GetInfoByRow('gh_configuration','config_id',array('config_id'=>1));
        $page_details['page_title']= $main_title->website_title.' | Login';
        $this->load->Module('header')->index($page_details);
        $this->load->Module('sidebar')->index();
        $u_id = $this->session->userdata('sess_user_id');
        $data['profile'] = $this->mdl_general->GetInfoByRow('gh_user','u_id',array('u_id'=>$u_id));
        $this->load->view('profile',$data);
    	$this->load->Module('footer')->index();

    }

    public function update_profile(){
        $form_data = array(); // initialize
        $form_data = array(
                'u_name' => $this->input->post('Name'),
                'company_name' => $this->input->post('company_name'),
                'company_tin_no' => $this->input->post('company_tin_no'),
                'contact_no' => $this->input->post('contact_no'),
                'u_cellno' => $this->input->post('mobile_no'),
                'u_email' => $this->input->post('email'),
                'u_username' => $this->input->post('username'),
                'u_password' => sha1($this->input->post('password')),
                'u_address' => $this->input->post('address'),
                'u_enabled' => 1
        );
       
        $hdd_Uid = $this->input->post('hdd_Uid');
        $where = array('u_id'=> $hdd_Uid);
        $res = $this->mdl_general->Manage('gh_user',$form_data, $where);
        if($res){
            $this->session->set_flashdata('msg_success', "Update successful..");
        }else{
            $this->session->set_flashdata('msg_error', "Failed To Update..");
        }

        redirect('profile');
    }
   
}