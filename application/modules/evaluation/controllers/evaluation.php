<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Evaluation extends MX_Controller{

	function __construct() {
        parent::__construct();
        $this->load->model('mdl_general');
        $this->load->model('mdl_extra_evaluation');
        $this->load->library('session');
       //  if($this->session->userdata('sess_logged_in')!=true){
    			// redirect('login/index?error=4');
       //  }
        
    }

    public function index(){
        $user_type = $this->session->userdata('sess_user_type');
        $main_title=$this->mdl_general->GetInfoByRow('gh_configuration','config_id',array('config_id'=>1));
    	$page_details['page_title']= $main_title->website_title.' | Services';
        $this->load->Module('header')->index($page_details);
        // $this->load->Module('carousel')->index();
        $passcode = $this->input->post('passcode');
        
        if(empty($passcode)){
            $passcode ='';
        }
            $seminar_list = $this->mdl_general->GetInfoByRow('set_seminar','seminar_id',array('passcode' => $passcode));
            $data['set_seminar'] = $seminar_list;
        
        $data['evaluation_type'] = $this->mdl_general->GetAllInfo('evaluation_type','et_id',array('status'=>1));
        // $data['evaluation_list'] = $this->mdl_general->GetAllInfo('evaluation_list','el_id');
        $this->load->view('evaluation',$data);
        $this->load->Module('footer')->index();
    
    }
    public function evaluation_process(){
        $ratings = $this->input->post('ratings');
        foreach ($ratings as $et_id => $el_list) {
            foreach ($el_list as $el_id => $rating) {
                $form_data = array(
                    'customer_id' => $this->input->post('customer_id'),
                    'et_id' => $et_id,
                    'el_id' => $el_id,
                    'ratings' => $rating,
                    'passcode' => $this->input->post('passcode'),
                );
                $res = $this->mdl_general->SaveForm('customer_evaluation',$form_data);
             }
        }
        $f_seminar = $this->input->post('f_seminar');
        $form_data1 = array(
            'customer_id' => $this->input->post('customer_id'),
            'reason' => $this->input->post('reason'),
            'comments_suggestion' => $this->input->post('comments_suggestion'),
            'f_seminar' => ($f_seminar == 'Yes')? 'Yes':'',
            'ifYesEmail' => $this->input->post('ifYesEmail'),
            'passcode' => $this->input->post('passcode')
        );
        $this->mdl_general->SaveForm('cust_evaluation_dtls',$form_data1);
        redirect('evaluation');
    }
    private function debug($msg="", $exit = false)
    {
        $today = date("Y-m-d H:i:s");

        if (is_array($msg) || is_object($msg))
        {
            echo "<hr>DEBUG ::[".$today."]<pre>\n";
            print_r($msg);
            echo "\n</pre><hr>";
        }
        else
        {
            echo "<hr>DEBUG ::[".$today."] $msg <hr>\n";
        }

        if ($exit) {
            $this->load->library('profiler');
            echo $this->profiler->run();
            exit;
        }
    }
    public function view_evaluation(){
        $user_type = $this->session->userdata('sess_user_type');
        $main_title=$this->mdl_general->GetInfoByRow('gh_configuration','config_id',array('config_id'=>1));
        $page_details['page_title']= $main_title->website_title.' | Services';
        $this->load->Module('header')->index($page_details);
        // $this->load->Module('carousel')->index();
        $passcode = $this->input->post('passcode');
        
        if(empty($passcode)){
            $passcode ='';
        }
            $seminar_list = $this->mdl_general->GetInfoByRow('set_seminar','seminar_id',array('passcode' => $passcode));
            $data['set_seminar'] = $seminar_list;
        
        $data['evaluation_type'] = $this->mdl_general->GetAllInfo('evaluation_type','et_id',array('status'=>1));
        $customer_id = $this->session->userdata('sess_user_id');
        $data['evaluation_dtls'] = $this->mdl_general->GetInfoByRow('cust_evaluation_dtls','ced_id', array('customer_id' => $customer_id,'passcode' => $passcode));
        $this->load->view('view_evaluation',$data);
        $this->load->Module('footer')->index();
    
    }
}