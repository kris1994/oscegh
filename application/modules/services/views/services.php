
    <!--Header-->
    <div class="header" id="home">
        <?php  $this->load->Module('sidebar')->index();?>
        <!--top-bar-w3-agile-->
        <!--//top-bar-w3-agile-->
    </div>
    <!--/inner_banner-->
    <div class="inner_banner" >
    </div>
    <!--//inner_banner-->
    <!--/short-->
    <div class="services-breadcrumb">
        <div class="inner_breadcrumb">

            <ul class="short">
                <li><a href="index.html">Home</a><span>|</span></li>
                <li>Services</li>
            </ul>
        </div>
    </div>
    <!--//short-->
    <!-- /inner_content -->
    <div class="banner_bottom">
        <div class="container">
            <div class="tittle_head">
                <h3 class="tittle">Our <span>Services </span></h3>
            </div>
            <div class="inner_sec_info_wthree_agile">
                <div class="col-md-8 blog_section">
                    <?php
                    foreach ($list as $r) {
                    ?>
                    <div class="col-md-6 blog_img">
                        <div class="blog_con">
                            <a href="<?php echo base_url('services/services_view').'/'.$r['services_id']?>"><img src="<?php echo base_url('uploaded_image/services').'/'.$r['services_image']?>" alt=" " class="img-responsive" style="height: 200px"></a>
                            <div class="blog_info">
                                <h4><?php echo number_format($r['services_price'],2)?> / Month</h4>
                                <h5><a href="<?php echo base_url('services/services_view').'/'.$r['services_id']?>"><?php echo $r['services_name']?></a></h5>
                                <ul class="blog_list">
                                    <li><span class="fa fa-user" aria-hidden="true"></span><a href="#">Test Admin</a><i>|</i></li>
                                    <li><span class="fa fa-share" aria-hidden="true"></span><a href="#">0</a><i>|</i></li>
                                    <li><span class="fa fa-tag" aria-hidden="true"></span><a href="#">0</a></li>

                                </ul>
                                <p><?php echo substr(strip_tags(rawurldecode($r['services_description'])),0,130).'...'?><a href="<?php echo base_url('services/services_view').'/'.$r['services_id']?>" style="color: blue;font-weight: bold; font-size: 13px;">Readmore</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                    <!-- <div class="clearfix"></div> -->
                    <?php foreach ($list_Oservices as $Or) {?>
                    <div class="col-md-12 blog_img lost">
                        <div class="blog_con">
                            <a href="<?php echo base_url('services/services_view').'/'.$Or['services_id']?>"><img src="<?php echo base_url('uploaded_image/services').'/'.$Or['services_image']?>" alt=" " class="img-responsive" ></a>
                            <div class="blog_info">
                                <h4><?php echo number_format($Or['services_price'],2)?> / Month</h4>
                                <h5><a href="<?php echo base_url('services/services_view').'/'.$Or['services_id']?>"><?php echo $Or['services_name']?></a></h5>
                                <ul class="blog_list">
                                    <li><span class="fa fa-user" aria-hidden="true"></span><a href="#">Test Admin</a><i>|</i></li>
                                    <li><span class="fa fa-share" aria-hidden="true"></span><a href="#">0</a><i>|</i></li>
                                    <li><span class="fa fa-tag" aria-hidden="true"></span><a href="#">0</a></li>

                                </ul>
                                <p><?php echo substr(strip_tags(rawurldecode($Or['services_description'])),0,130).'...'?><a href="<?php echo base_url('services/services_view').'/'.$Or['services_id']?>" style="color: blue;font-weight: bold; font-size: 13px;">Readmore</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                </div>
                <div class="col-md-4 blog_left">
                    <div class="search left_bar">
                        <h3>Subscribe <span>here</span></h3>
                        <form action="#" method="post">
                            <input class="email" type="email" name="Email" placeholder="Email here" required="">
                            <input type="submit" value="Send">
                        </form>
                    </div>
                    <div class="faq left_bar">
                        <h3>Appointment Other Services</h3>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <?php foreach ($list as $c) { ?>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading<?php echo $c['services_id']?>">
                                    <h4 class="panel-title asd">
                                        <a class="pa_italic collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $c['services_id']?>" aria-expanded="false"
                                            aria-controls="collapse<?php echo $c['services_id']?>">
                              <span class="glyphicon glyphicon-plus" aria-hidden="true"></span><i class="glyphicon glyphicon-minus" aria-hidden="true"></i><?php echo $c['services_name']?> ( <font style="color:orange; font-weight: bold"> 0</font> )
                            </a>
                                    </h4>
                                </div>
                                <div id="collapse<?php echo $c['services_id']?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $c['services_id']?>">
                                    <div class="panel-body panel_text">
                                        <?php echo $c['services_name']?>
                                    </div>
                                </div>
                            </div>
                            <?php }?>
                            
                            
                        </div>
                    </div>
                    <div class="faq left_bar">
                        <h3>Available Slots Services</h3>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <?php foreach ($list_Oservices as $c) {
                                $where = "AND pl.services_id='".@$c['services_id']."'"; 
                                $paymentList = $this->mdl_extra_services->getAvailableSlots($where);
                                $avl_slots = (@$c['services_slots']-@$paymentList->avail_slots);

                            ?>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading<?php echo $c['services_id']?>">
                                    <h4 class="panel-title asd">
                                        <a class="pa_italic collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $c['services_id']?>" aria-expanded="false"
                                            aria-controls="collapse<?php echo $c['services_id']?>">
                              <span class="glyphicon glyphicon-plus" aria-hidden="true"></span><i class="glyphicon glyphicon-minus" aria-hidden="true"></i><?php echo $c['services_name']?> ( <font style="color:orange; font-weight: bold"> <?php echo $avl_slots?></font> )
                            </a>
                                    </h4>
                                </div>
                                <div id="collapse<?php echo $c['services_id']?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $c['services_id']?>">
                                    <div class="panel-body panel_text">
                                        <?php echo $c['services_name']?>
                                    </div>
                                </div>
                            </div>
                            <?php }?>
                            
                            
                        </div>
                    </div>
                    <div class="categories left_bar">
                        <h3>FOR GROUP DISCOUNT</h3>
                        <ul>
                            <li></span> 3+1 REGISTER 3 AND GET 1 SLOT FOR FREE</a></li>
                            <li></span> 5+2   REGISTER 5 AND GET 2 SLOT FOR FREE</a></li>
                        </ul>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>
