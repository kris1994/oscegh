<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<style type="text/css">
    .btn-file {
        position: relative;
        overflow: hidden;
        background-color: #4f5151;
        color: white;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }
    #img-upload{
        position: absolute;
        width: 100%;
    }
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">Add User</h4>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="#">Settings</a></li>
                            <li class="breadcrumb-item active">add user</li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-12">
                    <div class="card-box">

                        <h4 class="header-title m-t-0 m-b-30">Add User</h4>
                        <form method="post" action="<?php echo base_url('admin/add_user_process')?>" autocomplete="off" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label for="full_name" class="col-1 col-form-label">Full Name</label>
                                <div class="col-3">
                                    <input class="form-control" type="text" name="full_name" id="full_name">
                                </div>
                                <label for="username" class="col-1 col-form-label">Username</label>
                                <div class="col-3">
                                    <input class="form-control" type="text" name="username" id="username">
                                </div>
                                <label for="username" class="col-1 col-form-label">Profile Image</label>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <!-- <label>Upload Image</label> -->
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                                <span class="btn btn-default btn-file">
                                                    Browse… <input type="file" id="imgInp" name="profile_image">
                                                </span>
                                            </span>
                                            <input type="text" class="form-control" readonly>
                                        </div>
                                        <img id='img-upload'/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="user_email" class="col-1 col-form-label">Email</label>
                                <div class="col-3">
                                    <input class="form-control" type="email" name="user_email" id="user_email">
                                </div>
                                <label for="contact_no" class="col-1 col-form-label">Contact No</label>
                                <div class="col-3">
                                    <input class="form-control" type="number" name="contact_no" id="contact_no">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="address" class="col-1 col-form-label">Address</label>
                                <div class="col-7">
                                    <textarea class="form-control" type="email" rows="4" name="address" id="address"></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password" class="col-1 col-form-label">Password</label>
                                <div class="col-3">
                                    <input class="form-control" type="password" name="password" id="password">
                                </div>
                                <button id="generate_pass" class="btn-sm  btn-primary btn btn-save waves-effect m-l-5 " type="button">Generate</button>
                                <button id="show_pass" class="btn-sm  btn-warning btn btn-save waves-effect m-l-5 " type="button">Show</button>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-1">Enable</label>
                                <div class="col-sm-4">
                                    <div class="checkbox checkbox-primary">
                                        <input id="enable" name="enable" type="checkbox" value="1">
                                        <label for="enable">Check me out</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-1">&nbsp;</label>
                                <button type="submit" class="btn btn-primary">Add User</button>
                            </div>
                        </form>
                    </div>
                </div><!-- end col -->
            </div>
            <!-- end row -->
        </div> <!-- container -->
    </div> <!-- content -->

</div>
<!-- End content-page -->
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
<?php echo $this->load->view('admin_layout/footer');?>
<script type="text/javascript">
$(document).ready( function() {
        $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function(event, label) {
            
            var input = $(this).parents('.input-group').find(':text'),
                log = label;
            
            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }
        
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();                
                reader.onload = function (e) {
                    $('#img-upload').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function(){
            readURL(this);
        });     
    });
$("#generate_pass").click(function(){
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>" + "admin/settings/random_password",
            dataType :'json',
            success: function (res) {
                $("#password").val(res['gen_pass']);
            }
        });
    });
$("#show_pass").click(function(){
    if($("#password").attr('type')=='password'){
        $("#password").attr('type','text');
        $('#show_pass').text('Hide');
    }else{
        $("#password").attr('type','password');
        $('#show_pass').text('Show');
    }
})
</script>