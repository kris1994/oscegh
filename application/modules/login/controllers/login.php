<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Login extends MX_Controller{

	function __construct() {
        parent::__construct();
        $this->load->model('mdl_general');
        $this->load->library('session');
        //$this->load->library('facebook'); 
        $this->load->library('user_agent');
    }
    function index(){
        $main_title=$this->mdl_general->GetInfoByRow('gh_configuration','config_id',array('config_id'=>1));
        $page_details['page_title']= $main_title->website_title.' | Login';
        if($this->session->userdata('sess_logged_in')==true){
            redirect('dashboard');
        }
        $this->load->Module('header')->index($page_details);
        $this->load->Module('sidebar')->index();
        $this->load->view('login');
    	$this->load->Module('footer')->index();

    }

    function do_login(){
        // $username=trim($this->input->post('username'));
    	$username=trim($this->input->post('username'));
    	$password=trim($this->input->post('password'));
        $encrpt_pass = sha1($password);
    	$logindate = date("Y-m-d");
        $user=$this->mdl_general->GetInfoByRow('gh_user','u_id',array('u_username'=>$username,'u_password'=>$encrpt_pass));
        if($user){//user available in database

    		if($user->u_enabled !='1'){//is user is disabled?
                $this->session->set_flashdata('msg_error', "Disabled this Account..");
    			redirect('login/index?error=2');
    		}else{//do the login ,set the session and redirect dashboard
    			$session_data=array(
    				'sess_user_id'=>$user->u_id,
                    'sess_user_name'=>$user->u_name,
                    'sess_user_type'=>$user->user_type,
                    'sess_user_email'=>$user->u_email,
    				'sess_logged_in'=>true
    				);
                $form_data = array('u_loginfdate' => date('Y-m-d h:i:s'));
                $this->mdl_general->Manage('gh_user',$form_data, array('u_id' => $user->u_id),'');
    			$this->session->set_userdata($session_data);
                $redirectCurrent = $this->input->post('redirectCurrent');
                redirect($redirectCurrent);
    			
    		}
    	}else{//username not available in database
            $this->session->set_flashdata('msg_error', "username not available in database..");
    		redirect('login/index?error=1');
    	}
    }
    public function registration(){
        $form_data = array(
                'u_name' => $this->input->post('full_name'),
                'u_email' => $this->input->post('email'),
                'company_name' => $this->input->post('company_name'),
                'company_tin_no' => $this->input->post('company_tinNo'),
                'u_cellno' => $this->input->post('mobile_no'),
                'u_username' => $this->input->post('username'),
                'u_password' => sha1($this->input->post('password')),
                'u_enabled' => 1
        );
        $res = $this->mdl_general->SaveForm('gh_user', $form_data);
        if($res){
            $this->session->set_flashdata('msg_success', "Register successful..");
        }else{
            $this->session->set_flashdata('msg_error', "Failed To Save..");
        }

        redirect('login');
    }
    function logout(){
    	//unsetting data from session and redirect to login page
        $form_data = array('u_logoutdate' => date('Y-m-d h:i:s'));
        $this->mdl_general->Manage('gh_user',$form_data, array('u_id' => $this->session->userdata('sess_user_id')),'');
    	$session_data=array(
    		'sess_user_id'=>'',
    		'sess_user_name'=>'',
    		'sess_logged_in'=>false
    		);
    	$this->session->set_userdata($session_data);

    	redirect('login');
    }
}