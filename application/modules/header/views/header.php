<!DOCTYPE html>
<html>

<head>
    <title>GH-GLOBAL TRAINING CENTER | Home :: GH</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <meta name="keywords" content="Megacorp a Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />

    <script type="application/x-javascript">
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <link href="<?php echo base_url('assets_services/css/bootstrap.css')?>" rel='stylesheet' type='text/css' />
    <link href="<?php echo base_url('assets_services/css/easy-responsive-tabs.css')?>" rel='stylesheet' type='text/css' />
    <link href="<?php echo base_url('assets_services/css/style.css')?>" rel='stylesheet' type='text/css' />
    <link href="<?php echo base_url('assets_services/css/font-awesome.css')?>" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Roboto+Mono:300,300i,400,400i,500,500i,700" rel="stylesheet">
    <link href="<?php echo base_url('assets_services/css/blog.css')?>" rel="stylesheet" type='text/css' media="all" />
    <link href="<?php echo base_url('assets_services/css/single.css')?>" rel="stylesheet" type='text/css' media="all" />
    <link href="<?php echo base_url('assets_services/css/elements.css')?>" rel="stylesheet" type='text/css' media="all" />
    <link href="<?php echo base_url('assets_services/css/mail.css')?>" rel="stylesheet" type='text/css' media="all" />
    <!-- Custom CSS -->
    <link href="<?php echo base_url('assets_services/css/custom.css')?>" rel="stylesheet" type='text/css' media="all" />
    <!--  -->
    <link href="<?php echo base_url('assets_services/css/services.css')?>" rel="stylesheet" type='text/css' media="all" />
</head>

<body>