<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_extra_about extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    public function getDetailsperTable($table, $where, $group_by, $limit){
        $this->db->select('*');
        $this->db->from($table);
        if(!empty($where)){
            $this->db->where($where);
        }
        if(!empty($group_by)){
            $this->db->group_by($group_by);
        }
        if(!empty($order_by)){
            $this->db->order_by($order_by); 
        }
        if(!empty($limit)){
            $this->db->limit($limit);
        }
        return $this->db->get()->result_array();
    }

    private function debug($msg="", $exit = false)
    {
        $today = date("Y-m-d H:i:s");

        if (is_array($msg) || is_object($msg))
        {
            echo "<hr>DEBUG ::[".$today."]<pre>\n";
            print_r($msg);
            echo "\n</pre><hr>";
        }
        else
        {
            echo "<hr>DEBUG ::[".$today."] $msg <hr>\n";
        }

        if ($exit) {
            $this->load->library('profiler');
            echo $this->profiler->run();
            exit;
        }
    }
    
}