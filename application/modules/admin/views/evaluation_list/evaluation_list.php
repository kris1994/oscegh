 <!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">Manage Evaluation</h4>

                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="#">Products</a></li>
                            <li class="breadcrumb-item active">Manage Evaluation</li>
                        </ol>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <!-- Modal -->
                <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <form role="form" method="post" action="<?php echo base_url('admin/settings/add_evaluation_list/')?>" autocomplete="off" data-parsley-validate novalidate>
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="mySmallModalLabel">Add Evaluation</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="col-xs-12">
                                     <div class="p-20">
                                        <div class="form-group row">
                                            <label for="evaluation_type" class="col-sm-2 form-control-label">Evaluation Type<span class="text-danger">*</span></label>
                                            <div class="col-sm-9">
                                                <select name="evaluation_type" required class="form-control" id="evaluation_type">
                                                    <option value="">-Select-</option>
                                                    <?php foreach($evaluation_type as $et){?>
                                                    <option value="<?php echo $et['et_id']?>"><?php echo $et['et_name']?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="evaluation_name" class="col-sm-2 form-control-label">Evaluation Name<span class="text-danger">*</span></label>
                                            <div class="col-sm-9">
                                                <input type="text" name="evaluation_name" required class="form-control" id="evaluation_name" placeholder="Evaluation Name">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="evaluation_order" class="col-sm-2 form-control-label">Evaluation Order<span class="text-danger"></span></label>
                                            <div class="col-sm-4">
                                                <input type="number" name="evaluation_order" id="evaluation_order" required placeholder="1" class="form-control" readonly value="1">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="action" id="action" value="add">
                                <input type="hidden" name="hddEvaluation_id" id="hddEvaluation_id">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" name="bnt_save" id="bnt_save" class="btn btn-primary">Save Evaluation</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            <div class="row">
                <div class="col-12">
                    <div class="card-box table-responsive">
                        <h4 class="m-t-0 header-title"><b>Add Evaluation</b></h4>
                        <div class="card-block">
                            <a href="javascript:void(0)" onclick="add_data()" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-sm"><i class="fa fa-plus"></i>  Add</a>
                        </div>
                        <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Evaluation ID</th>
                                <th>Evaluation Type</th>
                                <th>Evaluation Name</th>
                                <th>Evaluation Order</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach($list as $row){ ?>
                            <tr>
                                <td><?php echo $row['el_id']?></td>
                                <td><?php echo $row['et_name']?></td>
                                <td><?php echo $row['el_name']?></td>
                                <td><?php echo $row['el_order']?></td>
                                <td>
                                    <a href="javascript:void(0)" onclick="edit_data('<?php echo $row["el_id"]?>')" class="btn-sm btn-info waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-sm"><i class="fa fa-edit"></i>  Edit</a>
                                    <a href="javascript:void(0)" onclick="delete_data('<?php echo $row["el_id"]?>')" class="btn-sm btn-danger waves-effect waves-light"><i class="fa fa-trash"></i>  Delete</a>
                                </td>
                            </tr>
                            <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end row -->


        </div> <!-- container -->

    </div> <!-- content -->

</div>
<!-- End content-page -->
<?php echo $this->load->view('admin_layout/footer');?>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
<script type="text/javascript">
$("#evaluation_type").on('change',function(){
    $.ajax({
        data:{
        id:$(this).val()
        },
        type: "post",
        url: "<?php echo base_url()?>"+"admin/settings/getEvaluationRec",
        dataType: "json",
        success: function(data) {
           // console.log(data[0]['max_order_id']);
           var ev_order= parseInt(data['max_order_id'])||1;
           $("#evaluation_order").val(ev_order-1);
        }
    });
})
function add_data(){
    $("#mySmallModalLabel").text('Manage Evaluation');
    $("#bnt_save").text('Save Evaluation');
    $('#evaluation_name').val("");
    $('#evaluation_order').val("");
    $('#hddEvaluation_id').val("");
    $('#evaluation_type option').prop('selected', false);
    $('#action').val('add');
    $("#hddEvaluation_id").val("");
}
function edit_data(id){
    $('#action').val('edit');
    // alert(id);
    $("#mySmallModalLabel").text('Manage Evaluation');
    $("#bnt_save").text('Update Evaluation');
        $.ajax({
            data:{
            id:id
            },
            type: "post",
            url: "<?php echo base_url()?>"+"admin/settings/getEvaluationEdit",
            dataType: "json",
            success: function(data) {
                jQuery.each(data, function(key, list) {
                    $("#evaluation_type option").each(function(){
                        if($(this).val()==list['et_id']){
                            $(this).prop('selected', true);
                        }else{
                            $(this).prop('selected', false);
                        }
                    })
                    $("#evaluation_name").val(list['el_name']);
                    $("#evaluation_order").val(list['el_order']);
                    $("#hddEvaluation_id").val(list['el_id']);
                 // console.log(list['et_id']);
                });
            }
        });
}
function delete_data(id){
    var r = confirm("Do you want to delete this record?");
    if(r== true){
        $.ajax({
            data:{
                id:id
            },
            type: "post",
            url: "<?php echo base_url()?>"+"admin/settings/deleteAdminEvaluation",
            success:function(data){
                location.reload();
                
                
            }
        });

    }else{
        return false;
    }
}
$(document).ready(function() {
    $('#datatable').DataTable();
    //Buttons examples
    var table = $('#datatable-buttons').DataTable({
        lengthChange: false,
        buttons: ['copy', 'excel', 'pdf', 'colvis']
    });

    table.buttons().container()
            .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
} );

</script>

</body>
</html>