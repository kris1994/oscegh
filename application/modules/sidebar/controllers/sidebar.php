<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sidebar extends MX_Controller{
	function __construct() {
        parent::__construct();
        $this->load->model('mdl_general');
        
        
    }
    function index(){
    	$data['main_title']=$this->mdl_general->GetInfoByRow('gh_configuration','config_id',array('config_id'=>1));
    	$this->load->view('sidebar',$data);
    }
}