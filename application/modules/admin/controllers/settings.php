<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Settings extends MX_Controller{
	public function __construct() {
        parent::__construct();
        $this->load->model('mdl_general');
        $this->load->library('session');
    }
    public function admin_menu(){
        $this->load->model('mdl_extra');
        $main_title=$this->mdl_general->GetInfoByRow('gh_configuration','config_id',array('config_id'=>1));
        $page_details['page_title']= $main_title->website_title.' | Product';
        if($this->session->userdata('asess_logged_in')==true){
            $this->load->view('admin_layout/header', $page_details);
            $this->load->view('admin_layout/sidebar');
            $data['list']=$this->mdl_extra->getMenuList('');
            $data['menu_type_list'] = $this->mdl_general->GetAllInfo('gh_menutype','mt_id','');
            $this->load->view('admin_menu/admin_menu',$data);
        }else{
            $this->load->view('login/login');
        }
    }
    public function admin_menu_process(){
        $action = $this->input->post('action');
        switch (strtoupper($action)) {
            case 'ADD':
                $form_data = array(
                    'menu_name' => $this->input->post('menu_name'),
                    'menu_url' => $this->input->post('menu_url'),
                    'menu_order' => $this->input->post('menu_order'),
                    'mt_id' => $this->input->post('menu_type')
                );
                $this->mdl_general->SaveForm('gh_menu',$form_data);
                break;
            case 'EDIT':
                $form_data = array(
                    'menu_name' => $this->input->post('menu_name'),
                    'menu_url' => $this->input->post('menu_url'),
                    'menu_order' => $this->input->post('menu_order'),
                    'mt_id' => $this->input->post('menu_type')
                );
                $hddmenu_id = $this->input->post('hddmenu_id');
                $where = array('menu_id' => $hddmenu_id);
                $this->mdl_general->Manage('gh_menu',$form_data, $where);
                break;
            default:
                # code...
                break;
        }
        redirect('admin/settings/admin_menu');
    }
    public function getEditMenuList(){
        $id=$this->input->post('id');
        $data=$this->mdl_general->GetAllInfo('gh_menu','menu_id',array('menu_id'=>$id));
        echo json_encode($data);
    }
    public function deleteAdminMenu(){
        $id = $this->input->post('id');
        $this->mdl_general->Delete('gh_menu',array('menu_id'=>$id));
        $response=array(
            'status'=>'success',
            'message'=>'Menu deleted successfully' 
            );
        $this->session->set_flashdata($response);
        redirect('setup/country');
    }
     public function manage_user($link=null){
        $id = $this->uri->segment(3);
        $this->load->model('mdl_extra');
        $main_title=$this->mdl_general->GetInfoByRow('gh_configuration','config_id',array('config_id'=>1));
        $page_details['page_title']= $main_title->website_title.' | Manage User';
        if($this->session->userdata('asess_logged_in')==true){
            if($link == 'add_user'){
                $this->load->view('admin_layout/header', $page_details);
                $this->load->view('admin_layout/sidebar');
                $data['user_list']=$this->mdl_general->GetAllInfo('gh_user','u_id','');
                $this->load->view('manage_user/add_user',$data);
            }elseif($link == 'edit_user'){
                $this->load->view('admin_layout/header', $page_details);
                $this->load->view('admin_layout/sidebar');
                $data['user_list']=$this->mdl_general->GetAllInfo('gh_user','u_id',array('u_id' => $id));
                $this->load->view('manage_user/edit_user',$data);
            }else{
                $this->load->view('admin_layout/header', $page_details);
                $this->load->view('admin_layout/sidebar');
                $data['list']=$this->mdl_general->GetAllInfo('gh_user','u_id','');
                $this->load->view('manage_user/manage_user',$data);
            }
        }else{
            $this->load->view('login/login');
        }
    }
    public function add_user_process(){
        $profile_image = '';
        if (isset($_FILES['profile_image'])) {
            $randomNum = uniqid();
            $files = basename($_FILES['profile_image']['name']);
            $tmp = explode('.', $files);
            $ext = end($tmp);
            $profile_image = $randomNum . '.' . $ext;
            
            $uploadDir = 'profile_image/';
            $uploadFile = $uploadDir . $profile_image;
            move_uploaded_file($_FILES['profile_image']['tmp_name'], $uploadFile);
        }
        $enable = $this->input->post('enable');
        $form_data = array(
            'u_username' => $this->input->post('username'),
            'u_password' => md5($this->input->post('password')),
            'u_name' => $this->input->post('full_name'),
            'u_email' => $this->input->post('user_email'),
            'u_cellno' => $this->input->post('contact_no'),
            'u_address' => $this->input->post('address'),
            'u_enabled' => ($enable==1)?1:0,
            'profile_pic' => $profile_image

        );
        $this->mdl_general->SaveForm('gh_user',$form_data);
        redirect('settings/manage_user');
    }
    public function edit_user_process(){
        $profile_image = '';
        if (isset($_FILES['profile_image'])) {
            $randomNum = uniqid();
            $files = basename($_FILES['profile_image']['name']);
            $tmp = explode('.', $files);
            $ext = end($tmp);
            $profile_image = $randomNum . '.' . $ext;
            
            $uploadDir = 'uploaded_image/';
            $uploadFile = $uploadDir . $profile_image;
            move_uploaded_file($_FILES['profile_image']['tmp_name'], $uploadFile);
        }
        $enable = $this->input->post('enable');
        $form_data = array(
            'u_username' => $this->input->post('username'),
            'u_password' => md5($this->input->post('password')),
            'u_name' => $this->input->post('full_name'),
            'u_email' => $this->input->post('user_email'),
            'u_cellno' => $this->input->post('contact_no'),
            'u_address' => $this->input->post('address'),
            'u_enabled' => ($enable==1)?1:0,
            'profile_pic' => $profile_image

        );
        $hdduId = $this->input->post('hdduId');
        $this->mdl_general->Manage('gh_user',$form_data, array('u_id'=>$hdduId));
        redirect('settings/manage_user');
    }
    public function random_password() 
    {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $password = array(); 
        $gen = array();
        $alpha_length = strlen($alphabet) - 1; 
        for ($i = 0; $i < 8; $i++) 
        {
            $n = rand(0, $alpha_length);
            $password[] = $alphabet[$n];
        }
        $gen['gen_pass'] = implode($password);
        echo json_encode($gen);
    }
    public function user_role(){
        $this->load->model('mdl_extra');
        $id = $this->uri->segment(3);
        if($this->session->userdata('asess_logged_in')==true){
        $main_title=$this->mdl_general->GetInfoByRow('gh_configuration','config_id',array('config_id'=>1));
        $page_details['page_title']= $main_title->website_title.' | Manage User Role';
        $this->load->view('admin_layout/header', $page_details);
        $this->load->view('admin_layout/sidebar');
        $data['hddId'] = $id;
        $data['list']=$this->mdl_extra->getUserRole('WHERE ud.u_id="'.$id.'"');
        $this->load->view('manage_user/manage_user_role',$data);
        }else{
            $this->load->view('login/login');
        }
    }
    public function user_role_process(){
        $menu_name = $this->input->post('menu_name');
        $hddId = $this->input->post('hddId');
        $u_add = $this->input->post('u_add');
        $u_edit = $this->input->post('u_edit');
        $u_view = $this->input->post('u_view');
        $u_del = $this->input->post('u_del');
        $form_data = array(
            'menu_id' => $menu_name,
            'u_id' => $hddId,
            'u_add' => ($u_add==1)?1:0,
            'u_edit' => ($u_edit==1)?1:0,
            'u_view' => ($u_view==1)?1:0,
            'u_del' => ($u_del==1)?1:0
        );
        $this->mdl_general->SaveForm('gh_userd',$form_data);
        redirect('admin/user_role/'.$hddId);
    }
    public function deleteUserRole(){
        $id = $this->input->post('id');
        $this->mdl_general->Delete('gh_userd',array('rec'=>$id));
        $response=array(
            'status'=>'success',
            'message'=>'Role deleted successfully' 
            );
        $this->session->set_flashdata($response);
        redirect('admin/user_role/'.$id);
    }
    public function evaluation_list(){
        $this->load->model('mdl_extra');
        $main_title=$this->mdl_general->GetInfoByRow('gh_configuration','config_id',array('config_id'=>1));
        $page_details['page_title']= $main_title->website_title.' | Evaluation List';
        if($this->session->userdata('asess_logged_in')==true){
            $this->load->view('admin_layout/header', $page_details);
            $this->load->view('admin_layout/sidebar');
            $data['list']=$this->mdl_extra->getEvaluationList('');
            $data['evaluation_type'] = $this->mdl_general->GetAllInfo('evaluation_type','et_order','');
            $this->load->view('evaluation_list/evaluation_list',$data);
        }else{
            $this->load->view('login/login');
        }
    }
    public function getEvaluationRec(){
        $this->load->model("mdl_extra");
        $id=$this->input->post('id');
        $data = $this->mdl_extra->getCountId('FROM evaluation_list', ' count(el_id)+1 max_order_id', 'WHERE et_id="'.$id.'"', 'GROUP BY et_id');
        echo json_encode($data);
    }
    public function add_evaluation_list(){
        $action = $this->input->post('action');
        switch (strtoupper($action)) {
            case 'ADD':
                 $form_data = array(
                    'el_name' => $this->input->post('evaluation_name'),
                    'el_order' => $this->input->post('evaluation_order'),
                    'et_id' => $this->input->post('evaluation_type')
                 );
                 $this->mdl_general->SaveForm('evaluation_list',$form_data);
                 redirect('settings/evaluation_list');
                break;
            case 'EDIT':
                $form_data = array(
                    'el_name' => $this->input->post('evaluation_name'),
                    'el_order' => $this->input->post('evaluation_order'),
                    'et_id' => $this->input->post('evaluation_type')
                 );
                $hddEvaluation_id = $this->input->post('hddEvaluation_id');
                $this->mdl_general->Manage('evaluation_list',$form_data, array('el_id'=>$hddEvaluation_id));
                redirect('settings/evaluation_list');
                break;
        }
    }
    public function getEvaluationEdit(){
        $this->load->model("mdl_extra");
        $id=$this->input->post('id');
        $data = $this->mdl_extra->getEvaluationList('WHERE el.el_id="'.$id.'"');
        echo json_encode($data);
    }
    public function deleteAdminEvaluation(){
        $id = $this->input->post('id');
        $this->mdl_general->Delete('evaluation_list',array('el_id'=>$id));
        $response=array(
            'status'=>'success',
            'message'=>'Evaluation List deleted successfully' 
            );
        $this->session->set_flashdata($response);
        redirect('settings/evaluation_list');
    }
    public function configuration(){
        $this->load->model('mdl_extra');
        $main_title=$this->mdl_general->GetInfoByRow('gh_configuration','config_id',array('config_id'=>1));
        $page_details['page_title']= $main_title->website_title.' | Configuration';
        if($this->session->userdata('asess_logged_in')==true){
            $this->load->view('admin_layout/header', $page_details);
            $this->load->view('admin_layout/sidebar');
            $data['list'] = $main_title;
            $this->load->view('configuration/configuration',$data);
        }else{
            $this->load->view('login/login');
        }
    }
    public function updateConfiguration(){
        $enable = $this->input->post('enable');
        $form_data = array(
            'website_title' => $this->input->post('website_name'),
            'phone' => $this->input->post('phone'),
            'website_acronym' => $this->input->post('website_acronym'),
            'website_desc' => $this->input->post('website_desc'),
            'email' => $this->input->post('user_email'),
            'website' => $this->input->post('website'),
            'address' => $this->input->post('address'),
            'approval_disabled' => ($enable==1)?1:0
        );
        $hddConfId = $this->input->post('hddConfId');
        $this->mdl_general->Manage('gh_configuration',$form_data, array('config_id'=>$hddConfId));
        redirect('settings/configuration');
    }
}