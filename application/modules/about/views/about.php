    <!--Header-->
    <div class="header" id="home">
        <?php  $this->load->Module('sidebar')->index();?>
        <!--top-bar-w3-agile-->
        <!--//top-bar-w3-agile-->
    </div>
    <!--/inner_banner-->
    <div class="inner_banner" >
    </div>
    <!--//inner_banner-->
    <!--/short-->
    <div class="services-breadcrumb">
        <div class="inner_breadcrumb">

            <ul class="short">
                <li><a href="index.html">Home</a><span>|</span></li>
                <li>About</li>
            </ul>
        </div>
    </div>
    <!--//short-->
    <!-- /inner_content -->
    <!-- /about -->
    <div class="banner_bottom">
        <div class="container">
            <h3 class="tittle">About Us</h3>
            <?php
                $mv = @$mission_vision[0];
                $mv_img_path = base_url().'uploaded_image/'.@$mv['pg_image'];
            ?>
            <div class="inner_sec_info_wthree_agile">
                <div class="ab_info">
                    <img src="<?php echo $mv_img_path ?>" alt=" " class="img-responsive">
                    <h4>GH Global Training Center Co</h4>
                        <p class="sub_p">We’re from GH Global Training Center Co. a seminar provider regarding of Pro Company’s Labor Law Management Seminar & Taxation Seminar/Accounting Services/Bookkeeping Services. We also provide an external service for company like HR Management Service, handling labor cases (NLRC/Dole), bookkeeping services and other mandatory compliance services.
                        </p>
                </div>

                <div class="news-main">
                    <div class="col-md-6 news-left">
                        <?php
                            $mv1 = @$mission_vision[1];
                            $mv1_img_path = base_url().'uploaded_image/'.@$mv1['pg_image'];
                        ?>
                        <div class="col-md-10 b_left">
                            <img src="<?php echo $mv1_img_path?>" alt=" " class="img-responsive">

                        </div>
                        <!-- <div class="col-md-6 b_right">
                            <img src="<?php echo base_url('assets_services/images/gh_banner7.png')?>" alt=" " class="img-responsive">

                        </div> -->
                    </div>
                    <div class="col-md-6 news-right">
                        <h4>Mission</h4>
                        <p class="sub_p">To help business executives achieve excellence in business. We help your business achieve excellence
                        </p>
                        <h4>Vision</h4>
                        <p class="sub_p">Partner of executives in achieving success in business. We provide comprehensive, practical trainings and seminars to help businesses navigate through the challenges of today. Our lecturers are recognized experts in their fields. We invite you to partner with us in strengthening the knowledge infrastructure of your business.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //about -->
    <<!-- div class="history">
        <div class="container">
            <h3 class="tittle two">Supporting Team</h3>
            <div class="inner_sec_info_wthree_agile">
                <div class="col-md-4 history-grid">
                    <h4>Idea</h4>
                    <div class="story-img">
                        <img src="images/b1.jpg" alt=" ">
                    </div>
                    <div class="caption_story_w3ls">

                        <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>

                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-4 history-grid">
                    <h4>Vision</h4>
                    <div class="story-img">
                        <img src="images/b3.jpg" alt=" ">
                    </div>
                    <div class="caption_story_w3ls">

                        <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>

                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-4 history-grid">
                    <h4>Production</h4>
                    <div class="story-img">
                        <img src="images/b4.jpg" alt=" ">
                    </div>
                    <div class="caption_story_w3ls">

                        <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>

                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div> -->
    <!-- /projects -->
    <!-- <div class="mid_slider">
        <div class="container">
            <h3 class="tittle">Supporting Team</h3>
            <div class="inner_sec_info_wthree_agile">
                <div class="col-md-4 team_grid_info">
                    <img src="images/t1.jpg" alt=" " class="img-responsive" />
                    <h3>Jane Mycle </h3>
                    <p>CEO</p>
                    <div class="team_icons">
                        <ul>
                            <li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>

                        </ul>
                    </div>

                </div>
                <div class="col-md-4 team_grid_info">
                    <img src="images/t2.jpg" alt=" " class="img-responsive" />
                    <h3>Amanda Seylon </h3>
                    <p>Manager</p>
                    <div class="team_icons">
                        <ul>
                            <li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>

                        </ul>
                    </div>
                </div>
                <div class="col-md-4 team_grid_info">
                    <img src="images/t3.jpg" alt=" " class="img-responsive" />
                    <h3>Laura Mark</h3>
                    <p>Contructor</p>
                    <div class="team_icons">
                        <ul>
                            <li><a href="#" class="w3_agile_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="agile_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="#" class="w3_agile_dribble"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>

                        </ul>
                    </div>

                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div> -->
    <!-- //projects -->