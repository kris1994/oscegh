 <!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<link href="<?php echo base_url('assets/plugins/bootstrap-daterangepicker/daterangepicker.css')?>" rel="stylesheet">
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">Set Seminar</h4>

                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="#">Products</a></li>
                            <li class="breadcrumb-item active">Set Seminar</li>
                        </ol>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <!-- Modal -->
                <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <form role="form" method="post" action="<?php echo base_url('admin/product/seminar_process/')?>" autocomplete="off" data-parsley-validate novalidate>
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="mySmallModalLabel">Set Seminar</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            </div>
                            <div class="modal-body">
                                <div class="col-xs-12">
                                     <div class="p-20">
                                        <div class="form-group row">
                                            <label for="seminar_name" class="col-sm-3 form-control-label">Seminar Name<span class="text-danger">*</span></label>
                                            <div class="col-sm-9">
                                                <input type="text" name="seminar_name" required class="form-control" id="seminar_name" placeholder="Seminar Name">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="date_seminar" class="col-sm-3 form-control-label">Date Seminar<span class="text-danger"></span></label>
                                            <div class="col-sm-9">
                                                <input type="text" name="date_seminar" id="date_seminar" required placeholder="Date Seminar" class="form-control input-daterange-datepicker" value="<?php echo date('m/d/y').' - '.date('m/d/y')?>">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="name_of_speacker" class="col-sm-3 form-control-label">Name of Speaker<span class="text-danger"></span></label>
                                            <div class="col-sm-9">
                                                <input type="text" name="name_of_speaker" id="name_of_speaker" required placeholder="Name of Speaker" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="passcode" class="col-sm-3 form-control-label">Passcode<span class="text-danger"></span></label>
                                            <div class="col-sm-4">
                                                <input type="text" name="passcode" id="passcode" required placeholder="Passcode" class="form-control" readonly="readonly" required="required">
                                            </div>
                                            <div class="col-sm-4">
                                                <button type="button" name="btn_gen_passcode" id="btn_gen_passcode" class="btn btn-danger">Generate</button>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-sm-3">Enable</label>
                                            <div class="col-sm-4">
                                                <div class="checkbox checkbox-primary">
                                                    <input id="status" name="status" type="checkbox" value="1">
                                                    <label for="status">Check me out</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="action" id="action" value="add">
                                <input type="hidden" name="hddSeminar_id" id="hddSeminar_id">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" name="bnt_save" id="bnt_save" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            <div class="row">
                <div class="col-12">
                    <div class="card-box table-responsive">
                        <h4 class="m-t-0 header-title"><b>Set Seminar</b></h4>
                        <div class="card-block">
                            <a href="javascript:void(0)" onclick="add_data()" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-sm"><i class="fa fa-plus"></i>  Add</a>
                        </div>
                        <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Seminar ID</th>
                                <th>Seminar Name</th>
                                <th>Date Seminar</th>
                                <th>Name of Speaker</th>
                                <th>Passcode</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php foreach($list as $row){ 
                                    if($row['status']==1){
                                        $status = '<span class="label-success label label-default">Active</span>';
                                    }else{
                                        $status = '<span class="label-default label">Inactive</span>';
                                    }
                                ?>
                            <tr>
                                <td><?php echo $row['seminar_id']?></td>
                                <td><?php echo $row['seminar_name']?></td>
                                <td><?php echo $row['date_seminar']?></td>
                                <td><?php echo $row['name_of_speaker']?></td>
                                <td><?php echo $row['passcode']?></td>
                                <td><?php echo $status?></td>
                                <td>
                                    <a href="javascript:void(0)" onclick="edit_data('<?php echo $row["seminar_id"]?>')" class="btn-sm btn-info waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-sm"><i class="fa fa-edit"></i>  Edit</a>
                                    <a href="javascript:void(0)" onclick="delete_data('<?php echo $row["seminar_id"]?>')" class="btn-sm btn-danger waves-effect waves-light"><i class="fa fa-trash"></i>  Delete</a>
                                </td>
                            </tr>
                            <?php }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end row -->


        </div> <!-- container -->

    </div> <!-- content -->

</div>
<!-- End content-page -->
<?php echo $this->load->view('admin_layout/footer');?>
<script src="<?php echo base_url('assets/plugins/bootstrap-daterangepicker/daterangepicker.js')?>"></script>

<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
<script type="text/javascript">
    $('.input-daterange-datepicker').daterangepicker({
        format: 'MM/DD/YYYY',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-custom',
        cancelClass: 'btn-secondary'
    });
$("#btn_gen_passcode").on('click',function(){
    $.ajax({
        data:{
        id:$(this).val()
        },
        type: "post",
        url: "<?php echo base_url()?>"+"admin/settings/random_password",
        dataType: "json",
        success: function(data) {
            var _encrypt = '-'+data['gen_pass'];
            var d = new Date();
            var month = d.getMonth()+1;
            var day = d.getDate();
            var output = d.getFullYear() + '' +(month<10 ? '0' : '') + month + '' +(day<10 ? '0' : '') + day;
            $("#passcode").val('CE-'+output+_encrypt);
           // console.log(data[0]['max_order_id']);
           
        }
    });
})
function add_data(){
    $("#mySmallModalLabel").text('Set Seminar');
    $("#bnt_save").text('Save');
    $('#seminar_name').val("");
    $('#date_seminar').val("");
    $('#name_of_speaker').val("");
    $('#passcode').val("");
    $('#action').val('add');
    $('#status').prop('checked', false);
    $("#hddSeminar_id").val("");
}
function edit_data(id){
    $('#action').val('edit');
    // alert(id);
    $("#mySmallModalLabel").text('Set Seminar');
    $("#bnt_save").text('Update');
        $.ajax({
            data:{
            id:id
            },
            type: "post",
            url: "<?php echo base_url()?>"+"admin/product/getSeminarEdit",
            dataType: "json",
            success: function(data) {
                jQuery.each(data, function(key, list) {
                 // console.log(list);
                    $('#seminar_name').val(list['seminar_name']);
                    $('#date_seminar').val(list['date_seminar']);
                    $('#name_of_speaker').val(list['name_of_speaker']);
                    $('#passcode').val(list['passcode']);
                    if(list['status']==1){
                        $('#status').prop('checked', true);
                    }else{
                        $('#status').prop('checked', false);
                    }
                    $('#hddSeminar_id').val(list['seminar_id']);
                });
            }
        });
}
function delete_data(id){
    var r = confirm("Do you want to delete this record?");
    if(r== true){
        $.ajax({
            data:{
                id:id
            },
            type: "post",
            url: "<?php echo base_url()?>"+"admin/product/deleteSeminar",
            success:function(data){
                location.reload();
                
                
            }
        });

    }else{
        return false;
    }
}
$(document).ready(function() {
    $('#datatable').DataTable();
    //Buttons examples
    var table = $('#datatable-buttons').DataTable({
        lengthChange: false,
        buttons: ['copy', 'excel', 'pdf', 'colvis']
    });

    table.buttons().container()
            .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
} );

</script>

</body>
</html>