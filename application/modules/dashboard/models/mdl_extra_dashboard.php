<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_extra_dashboard extends CI_Model {

	public function getDetailsperTable($table, $where, $group_by, $limit){
		$this->db->select('*');
		$this->db->from($table);
		if(!empty($where)){
			$this->db->where($where);
		}
		if(!empty($group_by)){
			$this->db->group_by($group_by);
		}
		if(!empty($order_by)){
			$this->db->order_by($order_by);	
		}
		if(!empty($limit)){
			$this->db->limit($limit);
		}
		return $this->db->get()->result_array();
	}
}