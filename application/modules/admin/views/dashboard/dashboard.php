
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">

            <div class="row">
				<div class="col-xl-12">
					<div class="page-title-box">
                        <h4 class="page-title float-left">Dashboard</h4>

                        <ol class="breadcrumb float-right">
                            <!-- <li class="breadcrumb-item"><a href="#">Uplon</a></li> -->
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <!-- <li class="breadcrumb-item active">Dashboard</li> -->
                        </ol>

                        <div class="clearfix"></div>
                    </div>
				</div>
			</div>
            <!-- end row -->
            <div class="row">
                <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                    <div class="card-box tilebox-one">
                        <i class="icon-layers float-right text-muted"></i>
                        <h6 class="text-muted text-uppercase m-b-20">Appointment Today</h6>
                        <h2 class="m-b-20" data-plugin="counterup">0</h2>
                        <!-- <span class="label label-success"> +11% </span> <span class="text-muted">From previous period</span> -->
                    </div>
                </div>
                <div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
                    <div class="card-box tilebox-one">
                        <i class="icon-layers float-right text-muted"></i>
                        <h6 class="text-muted text-uppercase m-b-20">Appointment This Week</h6>
                        <h2 class="m-b-20" data-plugin="counterup">0</h2>
                        <!-- <span class="label label-success"> +11% </span> <span class="text-muted">From previous period</span> -->
                    </div>
                </div>

            </div>
            <!-- end row -->
        </div> <!-- container -->

    </div> <!-- content -->
</div>
<!-- End content-page -->
