    <!--Header-->
    <div class="header" id="home">
        <!--top-bar-w3-agile-->
        <?php  $this->load->Module('sidebar')->index();?>
        <!--//top-bar-w3-agile-->
        <!-- banner-text -->
        <div class="slider">
            <div class="callbacks_container">
                <ul class="rslides callbacks callbacks1" id="slider4">
                    <li>
                        <div class="banner-top">
                            <div class="banner-info_agileits_w3ls">
                                <h3>Accounting Services</h3>
                                <p>- For Industrial Development</p>
                                <a href="<?php echo base_url('services')?>">Read More <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                <a href="<?php echo base_url('contact_us')?>">Contact Us <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                            </div>

                        </div>
                    </li>
                    <li>
                        <div class="banner-top1">
                            <div class="banner-info_agileits_w3ls">
                                <h3>Compliance Services</h3>
                                <p>- For Industrial Development</p>
                                <a href="<?php echo base_url('services')?>">Read More <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                <a href="<?php echo base_url('contact_us')?>">Contact Us <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                            </div>

                        </div>
                    </li>
                    <li>
                        <div class="banner-top2">
                            <div class="banner-info_agileits_w3ls">
                                <h3>Labor Law</h3>
                                <p>- For Industrial Development</p>
                                <a href="<?php echo base_url('services')?>">Read More <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                <a href="<?php echo base_url('contact_us')?>">Contact Us <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                            </div>

                        </div>
                    </li>
                    <li>
                        <div class="banner-top3">
                            <div class="banner-info_agileits_w3ls">
                                <h3>Bookkeeping Services</h3>
                                <p>- For Industrial Development</p>
                                <a href="<?php echo base_url('services')?>">Read More <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                <a href="<?php echo base_url('contact_us')?>">Contact Us <i class="fa fa-caret-right" aria-hidden="true"></i></a>
                            </div>

                        </div>
                    </li>
                </ul>
            </div>
            <div class="clearfix"> </div>
        </div>
        <!--//Slider-->
    </div>
    <!-- /about -->
    <div class="banner_bottom">
        <div class="container">
            <div class="tittle_head">
                <h3 class="tittle">Our <span>Services </span></h3>
            </div>
                <div class="ser-first">
                    <div class="inner_sec_info_wthree_agile">
                        <div class="col-md-3 ser-first-grid text-center">
                            <span class="fa fa-shield" aria-hidden="true"></span>
                            <h3>Temporibus Autem</h3>
                            <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                        </div>
                        <div class="col-md-3 ser-first-grid text-center">
                            <span class="fa fa-pencil" aria-hidden="true"></span>
                            <h3>Temporibus Autem</h3>
                            <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                        </div>
                        <div class="col-md-3 ser-first-grid text-center">
                            <span class="fa fa-star" aria-hidden="true"></span>
                            <h3>Temporibus Autem</h3>
                            <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                        </div>
                        <div class="col-md-3 ser-first-grid text-center">
                            <span class="fa fa-thumbs-up" aria-hidden="true"></span>
                            <h3>Temporibus Autem</h3>
                            <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    <!-- //about -->
    <div class="history">
        <div class="container">
            <!-- <h3 class="tittle two">Main Services</h3> -->
            <!-- <div class="inner_sec_info_wthree_agile">
                <?php
                foreach ($main_services as $sv) {
                ?>
                <div class="col-md-4 history-grid">
                    <h4><?php echo $sv['services_name']?></h4>
                    <div class="story-img">
                        <img src="<?php echo base_url('uploaded_image/services').'/'.$sv['services_image']?>" alt=" ">
                    </div>
                    <div class="caption_story_w3ls">

                        <p><?php echo substr(strip_tags(rawurldecode($sv['services_description'])),0,100).'...'?><a href="<?php echo base_url('services/services_view').'/'.$sv['services_id']?>" style="color: orange;font-weight: bold; font-size: 13px;">Readmore</a></p>

                    </div>
                    <div class="clearfix"></div>
                </div>
                <?php }?>
                <div class="clearfix"></div>
            </div> -->
        </div>
    </div>
    <div class="banner_bottom">
        <div class="container">
            <div class="services-info">
                <h3 class="tittle">Featured Services</h3>
            </div>
            <div class="services-grids">
                <div class="inner_sec_info_wthree_agile">
                    <?php
                    foreach ($main_services as $sv) {
                    ?>
                    <div class="col-md-6 services-grid">
                        <div class="ser-img">
                            <img src="<?php echo base_url('uploaded_image/services').'/'.$sv['services_image']?>" alt=" ">
                        </div>
                        <div class="caption">
                            <h4><?php echo $sv['services_name']?></h4>
                            <p><?php echo substr(strip_tags(rawurldecode($sv['services_description'])),0,100).'...'?><a href="<?php echo base_url('services/services_view').'/'.$sv['services_id']?>" style="color: orange;font-weight: bold; font-size: 13px;">Readmore</a></p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <?php }?>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- /projects -->
    <div class="mid_slider">
        <div class="mid_slider_info">
            <h3 class="tittle">Our Seminar</h3>
            <div class="inner_sec_info_wthree_agile">
                <!-- banner -->
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1" class=""></li>
                        <li data-target="#myCarousel" data-slide-to="2" class=""></li>
                        <li data-target="#myCarousel" data-slide-to="3" class=""></li>
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        <?php
                        $list = $our_seminar_image;
                        $count_img = count($list);
                        ?>
                        <div class="item active">
                            <div class="row">
                                <?php
                                $counter=1;
                                foreach ($list as $im) {
                                    if($counter%5==0){
                                        echo "</div>";
                                    echo "</div>";
                                    echo '<div class="item ">
                                         <div class="row">';
                                    }
                                    $img_path = base_url().'uploaded_image/'.$im['pg_image'];
                                ?>
                                <div class="col-md-3 col-sm-3 col-xs-3 slidering">
                                    <div class="thumbnail"><img src="<?php echo $img_path;?>" alt="Image" style="max-width:100%;"></div>
                                </div>
                                <?php
                                    $counter++;
                                    }
                                ?>
                            </div>
                        </div>
                        
                    </div>
                    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="fa fa-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="fa fa-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                    <!-- The Modal -->
                </div>
            </div>
        </div>
    </div>
    <!-- //projects -->
