    <!--Header-->
    <div class="header" id="home">
        <?php  $this->load->Module('sidebar')->index();?>
        <!--top-bar-w3-agile-->
        <!--//top-bar-w3-agile-->
    </div>
    <!--/inner_banner-->
    <div class="inner_banner">
    </div>
    <!--//inner_banner-->
    <!--/short-->
    <div class="services-breadcrumb">
        <div class="inner_breadcrumb">

            <ul class="short">
                <li><a href="index.html">Home</a><span>|</span></li>
                <li>Services</li>
            </ul>
        </div>
    </div>
    <!--//short-->
    <!-- /inner_content -->
    <div class="banner_bottom">
        <div class="container">
            <div class="tittle_head">
                <h3 class="tittle">Our <span>Services </span></h3>
            </div>
            <div class="inner_sec_info_wthree_agile">
                <div class="col-md-8 blog_section">
                    <?php
                    foreach ($list as $r) {
                    ?>
                    <div class="col-md-6 blog_img">
                        <div class="blog_con">
                            <a href="<?php echo base_url('services/services_view').'/'.$r['services_id']?>"><img src="<?php echo base_url('uploaded_image/services').'/'.$r['services_image']?>" alt=" " class="img-responsive"></a>
                            <div class="blog_info">
                                <!-- <h4>2nd / Nov 2017</h4> -->
                                <h4><?php echo number_format($r['services_price'],2)?> / <?php echo $r['price_type']?></h4>
                                <h5><a href="<?php echo base_url('services/services_view').'/'.$r['services_id']?>"><?php echo $r['services_name']?></a></h5>
                               <!--  <ul class="blog_list">
                                    <li><span class="fa fa-user" aria-hidden="true"></span><a href="#">Jan Mark</a><i>|</i></li>
                                    <li><span class="fa fa-heart" aria-hidden="true"></span><a href="#">10</a><i>|</i></li>
                                    <li><span class="fa fa-tag" aria-hidden="true"></span><a href="#">5</a></li>

                                </ul> -->
                                <p><?php echo substr(rawurldecode($r['services_description']),0,260)?></p>
                            </div>
                        </div>
                    </div>
                    <?php }?>
                    <div class="clearfix"> </div>
                    <!-- <div class="col-md-12 blog_img lost">
                        <div class="blog_con">
                            <a href="single.html"><img src="<?php echo base_url('assets_services/images/Accounting-Services.png')?>" alt=" " class="img-responsive"></a>
                            <div class="blog_info">
                                <h4>1st / Dec 2017</h4>
                                <h5><a href="single.html">Consectetur adipiscing elit</a></h5>
                                <ul class="blog_list">
                                    <li><span class="fa fa-user" aria-hidden="true"></span><a href="#">Jan Mark</a><i>|</i></li>
                                    <li><span class="fa fa-heart" aria-hidden="true"></span><a href="#">10</a><i>|</i></li>
                                    <li><span class="fa fa-tag" aria-hidden="true"></span><a href="#">2</a></li>

                                </ul>
                                <p>Pellentesque eleifend ultricies tellus,varius risus, id dignissim sapien velit id felis ac cursus eros.</p>
                            </div>
                        </div>
                    </div> -->
                   <!--  <nav class="paging1">
                        <ul class="pagination">
                            <li>
                                <a href="#" aria-label="Previous">
                        <span aria-hidden="true">«</span>
                      </a>
                            </li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li>
                                <a href="#" aria-label="Next">
                        <span aria-hidden="true">»</span>
                      </a>
                            </li>
                        </ul>
                    </nav> -->
                </div>
                <div class="col-md-4 blog_left">
                    <div class="search left_bar">
                        <h3>Subscribe <span>here</span></h3>
                        <form action="#" method="post">
                            <input class="email" type="email" name="Email" placeholder="Email here" required="">
                            <input type="submit" value="Send">
                        </form>
                    </div>
                    <div class="faq left_bar">
                        <h3>FAQ's</h3>
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title asd">
                                        <a class="pa_italic" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true"
                                            aria-controls="collapseOne">
                              <span class="glyphicon glyphicon-plus" aria-hidden="true"></span><i class="glyphicon glyphicon-minus" aria-hidden="true"></i>test
                            </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body panel_text">
                                       test
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title asd">
                                        <a class="pa_italic collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false"
                                            aria-controls="collapseTwo">
                              <span class="glyphicon glyphicon-plus" aria-hidden="true"></span><i class="glyphicon glyphicon-minus" aria-hidden="true"></i>test1
                            </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body panel_text">
                                        test1
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="categories left_bar">
                        <h3>Categories</h3>
                        <ul>
                            <?php foreach ($list as $c) { ?>
                            <li><a href="<?php echo base_url('services/services_view').'/'.$c['services_id']?>"><span class="fa fa-angle-right" aria-hidden="true"></span><?php echo $c['services_name']?></a></li>
                            <?php }?>
                        </ul>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
    </div>

