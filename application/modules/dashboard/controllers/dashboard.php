<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MX_Controller{

	function __construct() {
        parent::__construct();
        $this->load->model('mdl_general');
        $this->load->model('mdl_extra_dashboard');
        $this->load->library('session');
       //  if($this->session->userdata('sess_logged_in')!=true){
    			// redirect('login/index?error=4');
       //  }
        
    }

    function index(){
        $user_type = $this->session->userdata('sess_user_type');
        $main_title=$this->mdl_general->GetInfoByRow('gh_configuration','config_id',array('config_id'=>1));
    	$page_details['page_title']= $main_title->website_title.' | Dashboard';
            $this->load->Module('header')->index($page_details);
            // $this->load->Module('sidebar')->index();
            // $this->load->Module('carousel')->index();
            $data['our_seminar_image'] = $this->mdl_general->GetAllInfo('page_image','pg_id',array('pg_image_type'=>'Our Seminars','pg_status'=>1));
            $where = array('status'=>1,'services_type'=>'Other Services');
            $data['main_services'] = $this->mdl_extra_dashboard->getDetailsperTable('services', $where, '', '');
            $wheremv = array('pg_status'=>1,'pg_image_type'=>'Mission & Vision');
            $data['mission_vision'] = $this->mdl_extra_dashboard->getDetailsperTable('page_image', $wheremv, '', 2);
            $this->load->view('dashboard',$data);
            $this->load->Module('footer')->index();
        
    }
    private function debug($msg="", $exit = false)
    {
        $today = date("Y-m-d H:i:s");

        if (is_array($msg) || is_object($msg))
        {
            echo "<hr>DEBUG ::[".$today."]<pre>\n";
            print_r($msg);
            echo "\n</pre><hr>";
        }
        else
        {
            echo "<hr>DEBUG ::[".$today."] $msg <hr>\n";
        }

        if ($exit) {
            $this->load->library('profiler');
            echo $this->profiler->run();
            exit;
        }
    }
}