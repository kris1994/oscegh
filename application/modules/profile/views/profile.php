<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="inner_banner">
    </div>
    <!--//inner_banner-->
    <!--/short-->
    <div class="services-breadcrumb">
        <div class="inner_breadcrumb">

            <ul class="short">
                <li><a href="<?php echo base_url('')?>">Home</a><span>|</span></li>
                <li>Profile</li>
            </ul>
        </div>
    </div>
    <?php
    $p = $profile;
    ?>
    <div class="banner_bottom">
        <div class="container">
            <div class="mail_form">
                <h3 class="tittle mail">My <span>Profile </span></h3>
                <div class="inner_sec_info_wthree_agile">
                    <?php
                        if($this->session->flashdata('msg_error')){
                            echo "<div class='alert alert-danger' role='alert'>".$this->session->flashdata('msg_error')."<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
                        }
                        elseif($this->session->flashdata('msg_success')){
                            echo "<div class='alert alert-success' role='alert'>".$this->session->flashdata('msg_success')."<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
                        }
                    ?>
                    <form action="<?php echo base_url('profile/update_profile')?>" method="post">
                        <span class="input input--chisato">
                        <input class="input__field input__field--chisato" name="Name" type="text" id="input-13" placeholder=" " required="" value="<?php echo @$p->u_name?>" />
                        <label class="input__label input__label--chisato" for="input-13">
                            <span class="input__label-content input__label-content--chisato" data-content="Full Name">Full Name</span>
                        </label>
                        </span>
                        <span class="input input--chisato">
                        <input class="input__field input__field--chisato" name="company_name" type="text" id="input-13" placeholder=" " required="" value="<?php echo @$p->company_name?>"/>
                        <label class="input__label input__label--chisato" for="input-13">
                            <span class="input__label-content input__label-content--chisato" data-content="Company Name">Company Name</span>
                        </label>
                        </span>
                        <span class="input input--chisato">
                        <input class="input__field input__field--chisato" name="company_tin_no" type="text" id="input-13" placeholder=" " required="" value="<?php echo @$p->company_tin_no?>"/>
                        <label class="input__label input__label--chisato" for="input-13">
                            <span class="input__label-content input__label-content--chisato" data-content="Company TIN #">Company TIN #</span>
                        </label>
                        </span>
                        <span class="input input--chisato">
                        <input class="input__field input__field--chisato" name="contact_no" type="text" id="input-13" placeholder=" " required="" value="<?php echo @$p->contact_no?>"/>
                        <label class="input__label input__label--chisato" for="input-13">
                            <span class="input__label-content input__label-content--chisato" data-content="Contact No">Contact No</span>
                        </label>
                        </span>
                        <span class="input input--chisato">
                        <input class="input__field input__field--chisato" name="mobile_no" type="text" id="input-13" placeholder=" " required="" value="<?php echo @$p->u_cellno?>"/>
                        <label class="input__label input__label--chisato" for="input-13">
                            <span class="input__label-content input__label-content--chisato" data-content="Mobile No">Mobile No</span>
                        </label>
                        </span>
                        <span class="input input--chisato">
                        <input class="input__field input__field--chisato" name="email" type="email" id="input-14" placeholder=" " required="" value="<?php echo @$p->u_email?>"/>
                        <label class="input__label input__label--chisato" for="input-14">
                            <span class="input__label-content input__label-content--chisato" data-content="Email">Email</span>
                        </label>
                        </span>
                        <span class="input input--chisato">
                        <input class="input__field input__field--chisato" name="username" type="text" id="input-15" placeholder=" " required="" value="<?php echo @$p->u_username?>"/>
                        <label class="input__label input__label--chisato" for="input-15">
                            <span class="input__label-content input__label-content--chisato" data-content="Username">Username</span>
                        </label>
                        </span>
                        <span class="input input--chisato">
                        <input class="input__field input__field--chisato" name="password" type="password" id="input-15" placeholder=" " required="" />
                        <label class="input__label input__label--chisato" for="input-15">
                            <span class="input__label-content input__label-content--chisato" data-content="Password">Password</span>
                        </label>
                        </span>
                        <textarea name="address" placeholder="Address" required=""><?php echo @$p->u_address?></textarea>
                        <div class="g-recaptcha" data-sitekey="6LdYmkkUAAAAAJxwDN7YfL0wrjUXaAbiphey0eEF"></div>
                        <input type="hidden" name="hdd_Uid" value="<?php echo @$p->u_id?>">
                        <input type="submit" value="Update">
                    </form>

                </div>
            </div>
            <div class="clearfix"> </div>

        </div>
    </div>
    </div>
