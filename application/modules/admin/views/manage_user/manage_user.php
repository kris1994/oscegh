 <!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">Manage User</h4>

                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="#">Settings</a></li>
                            <li class="breadcrumb-item active">Manage User</li>
                        </ol>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-12">
                    <div class="card-box table-responsive">
                        <h4 class="m-t-0 header-title"><b>Manage User</b></h4>
                        <div class="card-block">
                            <a href="<?php echo base_url('admin/add_user')?>" class="btn btn-primary waves-effect waves-light"><i class="fa fa-plus"></i>  Add User</a>
                        </div>
                        <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>Rec#</th>
                                <th>Full Name</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Contact No</th>
                                <th>Address</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                $counter=1;
                                foreach($list as $row){
                                    if($row['u_enabled']==1){
                                        $status = '<span class="label-success label label-default">Active</span>';
                                    }else{
                                        $status = '<span class="label-default label">Inactive</span>';
                                    }
                                ?>
                            <tr>
                                <td><?php echo $counter?></td>
                                <td><?php echo $row['u_name']?></td>
                                <td><?php echo $row['u_username']?></td>
                                <td><?php echo $row['u_email']?></td>
                                <td><?php echo $row['u_cellno']?></td>
                                <td><?php echo $row['u_address']?></td>
                                <td><?php echo $status?></td>
                                <td>
                                    <a href="<?php echo base_url('admin/edit_user').'/'.$row["u_id"]?>" class="btn-sm btn-info waves-effect waves-light"><i class="fa fa-edit"></i>  Edit</a>
                                    <a href="<?php echo base_url('admin/user_role').'/'.$row["u_id"]?>" class="btn-sm btn-warning waves-effect waves-light"><i class="fa fa-edit"></i>  Roles</a>
                                    <a href="javascript:void(0)" onclick="delete_data('<?php echo $row["u_id"]?>')" class="btn-sm btn-danger waves-effect waves-light"><i class="fa fa-trash"></i>  Delete</a>
                                </td>
                            </tr>
                            <?php $counter++; }?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end row -->


        </div> <!-- container -->

    </div> <!-- content -->

</div>
<!-- End content-page -->
<?php echo $this->load->view('admin_layout/footer');?>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
<script type="text/javascript">
function add_data(){
    $("#mySmallModalLabel").text('Add Manage User');
    $("#bnt_save").val('Save Menu');
    $('#menu_name').val("");
    $('#menu_url').val("");
    $('#menu_order').val("");
    $('#menu_type option').prop('selected', false);
    $('#action').val('add');
    $("#hddmenu_id").val("");
}
function edit_data(id){
    $('#action').val('edit');
    // alert(id);
    $("#mySmallModalLabel").text('Edit Manage User');
    $("#bnt_save").val('Update Manage User');
        $.ajax({
            data:{
            id:id
            },
            type: "post",
            url: "<?php echo base_url()?>"+"admin/settings/getEditMenuList",
            dataType: "json",
            success: function(data) {
                jQuery.each(data, function(key, list) {
                    // alert(list['no_of_picture']);
                $('#menu_name').val(list['menu_name']);
                $('#menu_url').val(list['menu_url']);
                $('#menu_order').val(list['menu_order']);
                    $('#menu_type option').each(function(){
                       if($(this).val() == list['mt_id']){
                            $(this).prop('selected', true);
                       }else{
                            $(this).prop('selected', false);
                       }
                    });
                $("#hddmenu_id").val(list['menu_id']);
                });
            }
        });
}
function delete_data(id){
    var r = confirm("Do you want to delete this record?");
    if(r== true){
        $.ajax({
            data:{
                id:id
            },
            type: "post",
            url: "<?php echo base_url()?>"+"admin/settings/deleteAdminMenu",
            success:function(data){
                location.reload();
                
                
            }
        });

    }else{
        return false;
    }
}
$(document).ready(function() {
    $('#datatable').DataTable();
    //Buttons examples
    var table = $('#datatable-buttons').DataTable({
        lengthChange: false,
        buttons: ['copy', 'excel', 'pdf', 'colvis']
    });

    table.buttons().container()
            .appendTo('#datatable-buttons_wrapper .col-md-6:eq(0)');
} );

</script>

</body>
</html>