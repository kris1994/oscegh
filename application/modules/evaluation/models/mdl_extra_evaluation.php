<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_extra_evaluation extends CI_Model {

	public function getDetailsperTable($table, $where, $group_by, $sort_by){
		$this->db->select('*');
		$this->db->from($table);
		if(!empty($where)){
			$this->db->where($where);
		}
		if(!empty($group_by)){
			$this->db->group_by($group_by);
		}
		if(!empty($order_by)){
			$this->db->order_by($order_by);	
		}
		
		return $this->db->get()->result_array();
	}
	public function getEvaluationTest($customer_id, $passcode){
		$query = $this->db->query("
			SELECT ce.*, ced.reason, ced.comments_suggestion, ced.f_seminar, ced.ifYesEmail, ced.passcode, ss.*
			FROM `customer_evaluation` ce
			LEFT JOIN cust_evaluation_dtls ced ON ce.customer_id = ced.customer_id
			LEFT JOIN set_seminar ss ON ce.passcode = ss.passcode
			AND ce.passcode = ced.passcode
			WHERE ce.customer_id = '".$customer_id."'
			AND ce.passcode = '".$passcode."'
			AND ss.status =1;
			")->result_array();
		return $query;
	}
	private function debug($msg="", $exit = false)
    {
        $today = date("Y-m-d H:i:s");

        if (is_array($msg) || is_object($msg))
        {
            echo "<hr>DEBUG ::[".$today."]<pre>\n";
            print_r($msg);
            echo "\n</pre><hr>";
        }
        else
        {
            echo "<hr>DEBUG ::[".$today."] $msg <hr>\n";
        }

        if ($exit) {
            $this->load->library('profiler');
            echo $this->profiler->run();
            exit;
        }
    }
}