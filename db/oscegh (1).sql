-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 19, 2018 at 05:48 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oscegh`
--

-- --------------------------------------------------------

--
-- Table structure for table `gh_configuration`
--

CREATE TABLE `gh_configuration` (
  `config_id` int(11) NOT NULL,
  `website_title` varchar(30) NOT NULL,
  `address` varchar(150) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `fax` varchar(15) NOT NULL,
  `email` varchar(254) NOT NULL,
  `website` varchar(30) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `approval_disabled` tinyint(2) NOT NULL,
  `signatureimage` varchar(255) NOT NULL,
  `config_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gh_configuration`
--

INSERT INTO `gh_configuration` (`config_id`, `website_title`, `address`, `phone`, `fax`, `email`, `website`, `logo`, `approval_disabled`, `signatureimage`, `config_date`) VALUES
(1, 'GH-Ordering System and Custome', '18 Holybrook Street, Glasgow , G42 7EH', '07854327040', '', 'info@onyxtech.co.uk', 'www.onyxtech.co.uk', 'favicon.ico', 1, 'RajakSign.jpg', '2014-08-23');

-- --------------------------------------------------------

--
-- Table structure for table `gh_menu`
--

CREATE TABLE `gh_menu` (
  `menu_id` int(11) NOT NULL,
  `menu_name` varchar(50) NOT NULL,
  `menu_url` varchar(50) NOT NULL,
  `menu_order` int(11) NOT NULL,
  `mt_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gh_menu`
--

INSERT INTO `gh_menu` (`menu_id`, `menu_name`, `menu_url`, `menu_order`, `mt_id`) VALUES
(1, 'configuration', 'configuration', 1, 3),
(2, 'Manage Products', 'manage_products', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `gh_menutype`
--

CREATE TABLE `gh_menutype` (
  `mt_id` int(11) NOT NULL,
  `mt_name` varchar(50) NOT NULL,
  `mt_url` varchar(45) NOT NULL,
  `menu_icon` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gh_menutype`
--

INSERT INTO `gh_menutype` (`mt_id`, `mt_name`, `mt_url`, `menu_icon`) VALUES
(1, 'Dashboard', 'dashboard', 'zmdi zmdi-view-dashboard'),
(2, 'Products Category', 'product', 'zmdi zmdi-collection-item'),
(3, 'Settings', 'settings', 'zmdi zmdi-settings-square');

-- --------------------------------------------------------

--
-- Table structure for table `gh_user`
--

CREATE TABLE `gh_user` (
  `u_id` int(11) NOT NULL,
  `u_username` varchar(15) NOT NULL,
  `u_password` varchar(50) NOT NULL,
  `u_name` varchar(20) NOT NULL,
  `u_email` varchar(254) NOT NULL,
  `u_remarks` varchar(200) NOT NULL,
  `u_cellno` varchar(15) NOT NULL,
  `u_loginfdate` datetime NOT NULL,
  `u_logoutdate` datetime NOT NULL,
  `u_enabled` tinyint(2) NOT NULL,
  `user_type` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gh_user`
--

INSERT INTO `gh_user` (`u_id`, `u_username`, `u_password`, `u_name`, `u_email`, `u_remarks`, `u_cellno`, `u_loginfdate`, `u_logoutdate`, `u_enabled`, `user_type`) VALUES
(3, 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'admin', 'admin@gmail.com', 'admin', '', '2018-01-18 12:21:41', '2018-01-04 04:30:35', 1, 'admin'),
(21, 'guest', '084e0343a0486ff05530df6c705c8bb4', 'guest', 'test@gmail.com', 'guest', '902390234', '2017-10-22 00:00:00', '2017-10-22 00:00:00', 1, 'customer');

-- --------------------------------------------------------

--
-- Table structure for table `gh_userd`
--

CREATE TABLE `gh_userd` (
  `rec` int(11) NOT NULL,
  `u_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `u_add` tinyint(2) NOT NULL,
  `u_edit` tinyint(2) NOT NULL,
  `u_del` tinyint(2) NOT NULL,
  `u_view` tinyint(2) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gh_userd`
--

INSERT INTO `gh_userd` (`rec`, `u_id`, `menu_id`, `u_add`, `u_edit`, `u_del`, `u_view`) VALUES
(1, 3, 1, 1, 1, 1, 1),
(2, 21, 1, 1, 1, 1, 1),
(3, 3, 2, 1, 1, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gh_configuration`
--
ALTER TABLE `gh_configuration`
  ADD PRIMARY KEY (`config_id`);

--
-- Indexes for table `gh_menu`
--
ALTER TABLE `gh_menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `gh_menutype`
--
ALTER TABLE `gh_menutype`
  ADD PRIMARY KEY (`mt_id`);

--
-- Indexes for table `gh_user`
--
ALTER TABLE `gh_user`
  ADD PRIMARY KEY (`u_id`);

--
-- Indexes for table `gh_userd`
--
ALTER TABLE `gh_userd`
  ADD PRIMARY KEY (`rec`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gh_configuration`
--
ALTER TABLE `gh_configuration`
  MODIFY `config_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `gh_menu`
--
ALTER TABLE `gh_menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `gh_menutype`
--
ALTER TABLE `gh_menutype`
  MODIFY `mt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `gh_user`
--
ALTER TABLE `gh_user`
  MODIFY `u_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `gh_userd`
--
ALTER TABLE `gh_userd`
  MODIFY `rec` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
