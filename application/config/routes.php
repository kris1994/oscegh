<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "dashboard";
$route['404_override'] = 'error';
$route['translate_uri_dashes'] = FALSE;

$route['view_evaluation'] = "evaluation/view_evaluation";

$route['admin/add_user'] = "admin/settings/manage_user/add_user";
$route['admin/add_user_process'] = "admin/settings/add_user_process";
$route['admin/edit_user'] = 'admin/settings/manage_user/edit_user';
$route['admin/edit_user'] = 'admin/settings/manage_user/edit_user';
$route['admin/edit_user/(:any)'] = 'admin/settings/manage_user/edit_user';
$route['admin/edit_user/(:num)'] = 'admin/settings/manage_user/edit_user';
$route['admin/edit_user_process'] = "admin/settings/edit_user_process";
$route['admin/user_role/(:num)'] = "admin/settings/user_role";
$route['admin/user_role_process'] = "admin/settings/user_role_process";

$route['product/manage_products'] = "admin/product/manage_products/";
$route['product/manage_services'] = "admin/product/manage_services/";
$route['products/services_process'] = "admin/product/services_process/";
$route['product/page_image'] = "admin/product/page_image";
$route['product/page_image_process'] = "admin/product/page_image_process";
$route['product/set_evaluation'] = "admin/product/set_evaluation";
$route['product/appointment_list'] = "admin/product/appointment_list";

$route['settings/admin_menu'] = "admin/settings/admin_menu";
$route['settings/admin_menu'] = "admin/settings/admin_menu";
$route['settings/manage_user'] = "admin/settings/manage_user";
$route['settings/evaluation_list'] = "admin/settings/evaluation_list";
$route['settings/add_evaluation_list'] = "admin/settings/add_evaluation_list";
$route['settings/configuration'] = "admin/settings/configuration";
$route['settings/update-config'] = "admin/settings/updateConfiguration";


/* End of file routes.php */
/* Location: ./application/config/routes.php */