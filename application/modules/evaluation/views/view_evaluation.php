    <!--Header-->
    <div class="header" id="home">
        <?php  $this->load->Module('sidebar')->index();?>
        <!--top-bar-w3-agile-->
        <!--//top-bar-w3-agile-->
    </div><!--/inner_banner-->
    <!--/inner_banner-->
    <div class="inner_banner">
    </div>
    <!--//inner_banner-->
    <!--/short-->
    <div class="services-breadcrumb">
        <div class="inner_breadcrumb">

            <ul class="short">
                <li><a href="index.html">Home</a><span>|</span></li>
                <li>View Customer Evaluation</li>
            </ul>
        </div>
    </div>
    <!--//short-->
    <!-- /inner_content -->
    <div class="banner_bottom">
        <div class="container">
            <h3 class="tittle">View Customer Evaluation</h3>
            <?php if ($this->session->flashdata('status') =='success') { ?>
                <div class="alert alert-success" role="alert">
                    <strong>Success!</strong><?php echo $this->session->flashdata('message')?>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('status') =='error') { ?>
                <div class="alert alert-danger" role="alert">
                    <strong>Error!</strong><?php echo $this->session->flashdata('message')?>
                </div>
            <?php } ?>
                <!-- <h3>Enter Passcode <span>here</span></h3> -->
            <div class="inner_sec_info_wthree_agile">
                <div class="elements_sec">
                    <?php if($this->session->userdata('sess_logged_in')!=true){ ?>
                        <p>Need to Login First!!!<a href="<?php echo base_url('login')?>">Login</a></p>
                    <?php }else{?>
                    <!--lists -->
                    <!--/table-->
                    <?php if(empty($set_seminar)){?>
                    <form action="<?php echo base_url('view_evaluation')?>" method="post" autocomplete="off">
                       <div class="form-group">
                        <label for="focusedinput" class="col-sm-3 control-label">Enter Passcode here</label>
                            <div class="col-sm-4">
                                <input type="password" name="passcode" class="form-control1" id="focusedinput" placeholder="Passcode">
                            </div>
                            <div class="col-sm-2 jlkdfj1">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                    <?php }else{?>
                    <!-- <form action="<?php echo base_url('evaluation/evaluation_process')?>" method="post"> -->
                    <div class="Pages bg4">
                        <div class="grid_3 grid_5 tables">
                            <h3 class="ele-tittle" style="text-align: center;display: block; color:blue"><?php echo @$set_seminar->seminar_name?></h3>
                            <br>
                            <div class="ele_info_text">
                            <div class="ele_info_text login-top">
                                <div class="col-md-6 list-grids">
                                    <?php
                                    // $list = $evaluation_test;
                                    $semD = explode("-",$set_seminar->date_seminar);
                                    $fromSemD = date('F j, Y',strtotime($semD[0]));
                                    $toSemD = date('F j, Y',strtotime($semD[1]));
                                    ?>
                                    <input type="text" class="dateOfSeminar" name="dateOfSeminar" placeholder="Date of Seminar" required="" value="<?php echo $fromSemD.' - '.$toSemD?>" readonly>
                                    <input type="text" class="nameOfSpeaker" name="nameOfSpeaker" placeholder="Name of Speaker" required="" value="<?php echo $set_seminar->name_of_speaker?>" readonly>
                                    <input type="hidden" name="customer_id" value="<?php echo $this->session->userdata('sess_user_id')?>">
                                    <input type="text" class="nameOfParticipant" name="nameOfParticipant" placeholder="Name of Participant" required="" value="<?php echo $this->session->userdata('sess_user_name')?>" readonly>
                                </div>
                                <div class="col-md-12 list-grids">
                                    <input type="text" class="Remarks" name="reason" placeholder="Reasons for attending this seminar" required="" readonly="readonly" value="<?php echo @$evaluation_dtls->reason?>">
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                                <div class="col-md-12 llist-grids llist-grids-btm2">
                                    <p>*Please use scale 1-5 where <span style="color:red">1</span> is <span style="color:blue; font-weight: bold">“Strongly Disagree”</span>, <span style="color:red">2</span> is <span style="color:blue;font-weight: bold">“Disagree”</span>, <span style="color:red">3</span> is <span style="color:blue; font-weight: bold">“Neutral”</span>, <span style="color:red">4</span> is<span style="color:blue; font-weight: bold"> “Agree”</span> and <span style="color:red">5</span> is
                                        <span style="color:blue; font-weight: bold">“Strongly Agree”</span>..</p>
                                    <div class="bs-docs-example table_mini">
                                        <?php
                                        foreach ($evaluation_type as $et) {
                                        ?>
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th style="color:blue">
                                                    <?php echo $et['et_name']?></th>
                                                    <th>1</th>
                                                    <th>2</th>
                                                    <th>3</th>
                                                    <th>4</th>
                                                    <th>5</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $evaluation_list = $this->mdl_general->GetAllInfo('evaluation_list','el_id', array('et_id' => $et['et_id']));
                                                $counter=0;
                                                foreach ($evaluation_list as $el) {
                                                    $t = $this->mdl_general->GetAllInfo('customer_evaluation','et_id', array('et_id' => $et['et_id'], 'el_id' => $el['el_id'], 'passcode' => @$set_seminar->passcode));
                                                    $Tch = @$t[0];
                                                ?>
                                                <tr>
                                                    <td scope="row"><?php echo $el['el_name']?></td>
                                                    <td width="10px;">
                                                        <label class="radio_custom"> <input type="radio" name="ratings[<?php echo $et['et_id']?>][<?php echo $el['el_id']?>]" value="1" <?php echo ($Tch['ratings'] == 1) ? 'checked':''?> disabled="disabled" >
                                                          <span class="checkmark"></span></label>
                                                    </td>
                                                    <td width="10px;">
                                                        <label class="radio_custom"><input type="radio" name="ratings[<?php echo $et['et_id']?>][<?php echo $el['el_id']?>]" value="2" <?php echo ($Tch['ratings'] == 2) ? 'checked':''?> disabled="disabled" >
                                                          <span class="checkmark"></span></label>
                                                    </td>
                                                    <td width="10px;">
                                                        <label class="radio_custom"><input type="radio" name="ratings[<?php echo $et['et_id']?>][<?php echo $el['el_id']?>]" value="3" <?php echo ($Tch['ratings'] == 3) ? 'checked':''?> disabled="disabled" >
                                                          <span class="checkmark"></span></label>
                                                    </td>
                                                    <td width="10px;">
                                                        <label class="radio_custom"><input type="radio" name="ratings[<?php echo $et['et_id']?>][<?php echo $el['el_id']?>]" value="4" <?php echo ($Tch['ratings'] == 4) ? 'checked':''?> disabled="disabled" >
                                                          <span class="checkmark"></span></label>
                                                    </td>
                                                    <td width="10px;">
                                                        <label class="radio_custom"><input type="radio" name="ratings[<?php echo $et['et_id']?>][<?php echo $el['el_id']?>]" value="5" <?php echo ($Tch['ratings'] == 5) ? 'checked':''?> disabled="disabled" >
                                                          <span class="checkmark"></span></label>
                                                        </td>
                                                </tr>
                                                <?php $counter++; }?>
                                            </tbody>
                                        </table>
                                        <?php }?>
                                        <div class="form-group">
                                            <textarea name="comments_suggestion" id="txtarea1" cols="50" rows="40" class="form-control1" placeholder="ADDITIONAL COMMENTS/SUGGESTIONS" style="height:100px" disabled="disabled"><?php echo @$evaluation_dtls->comments_suggestion?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <p>Would you like to be contacted about future seminars?</p>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <label class="radio_custom"><span style="margin-left: 20px"> Yes</span>
                                                  <input type="radio" name="f_seminar" id="f_seminar" value="Yes" <?php echo (@$evaluation_dtls->f_seminar == 'Yes') ? 'checked':''?> disabled="disabled" >
                                                  <span class="checkmark"></span>
                                                </label>
                                                <label class="radio_custom"><span style="margin-left: 20px"> No</span>
                                                  <input type="radio" name="f_seminar" id="f_seminar" value="No" <?php echo (@$evaluation_dtls->f_seminar == 'No') ? 'checked':''?> disabled="disabled" >
                                                  <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div class="form-group" id="ifYesEmail" <?php echo (@$evaluation_dtls->f_seminar == 'Yes') ? '':'style="display: none;"'?>>
                                                <label for="smallinput" class="col-sm-6 control-label label-input-sm">If yes, what is your email address?</label>
                                                <div class="col-sm-8">
                                                    <input type="email" name="ifYesEmail" class="form-control1 input-sm" id="email" placeholder="Email" value="<?php echo @$evaluation_dtls->ifYesEmail?>" readonly="readonly">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="form-group" style="float: right">
                                            <input type="hidden" name="passcode" value="<?php echo @$set_seminar->passcode?>">
                                            <button type="button" class="btn btn-warning sharp" onclick="return printEvaluation()">Print</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                    </div>
                <!-- </form> -->
                <?php } }?>
                    <!--//table-->
                </div>
            </div>
        </div>
    </div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script>
    $('.radio_custom #f_seminar').click(function(){
        // alert($(this).val());
        if($(this).val()=='Yes'){
            $("#ifYesEmail").css('display','block');
        }else{
            $("#ifYesEmail").css('display','none');
        }
    });

    function confirm_sub(){
        var _agree = confirm("Are you sure you want to submit?");
        if(_agree){
            return true;
        }else{
            return false;
        }
    }
</script>