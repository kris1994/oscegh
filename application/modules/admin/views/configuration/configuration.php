<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<style type="text/css">
    .btn-file {
        position: relative;
        overflow: hidden;
        background-color: #4f5151;
        color: white;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        outline: none;
        background: white;
        cursor: inherit;
        display: block;
    }
    #img-upload{
        position: absolute;
        width: 100%;
    }
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">

            <div class="row">
                <div class="col-xl-12">
                    <div class="page-title-box">
                        <h4 class="page-title float-left">Configuration</h4>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item"><a href="#">Settings</a></li>
                            <li class="breadcrumb-item active">Configuration</li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-12">
                    <div class="card-box">
                        <h4 class="header-title m-t-0 m-b-30">Configuration</h4>
                        <form method="post" action="<?php echo base_url('settings/update-config')?>" autocomplete="off" enctype="multipart/form-data">
                            <div class="form-group row">
                                <label for="website_name" class="col-1 col-form-label">Website Name</label>
                                <div class="col-3">
                                    <input class="form-control" type="text" name="website_name" id="website_name" value="<?php echo $list->website_title?>">
                                </div>
                                <label for="phone" class="col-1 col-form-label">Phone</label>
                                <div class="col-3">
                                    <input class="form-control" type="text" name="phone" id="phone" value="<?php echo $list->phone?>">
                                </div>
                                <label for="website_acronym" class="col-2 col-form-label">Website Acronym</label>
                                <div class="col-2">
                                    <input class="form-control" type="text" name="website_acronym" id="website_acronym"  value="<?php echo $list->website_acronym?>">
                                </div>
                                <!-- <label for="logo" class="col-1 col-form-label">Logo</label>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-btn">
                                                <span class="btn btn-default btn-file">
                                                    Browse… <input type="file" id="imgInp" name="logo">
                                                </span>
                                            </span>
                                            <input type="text" class="form-control" readonly>
                                        </div>
                                        <img id='img-upload'/>
                                    </div>
                                </div> -->
                            </div>
                            <div class="form-group row">
                                <label for="website_desc" class="col-1 col-form-label">Website Description</label>
                                <div class="col-7">
                                    <input class="form-control" type="text" name="website_desc" id="website_desc" value="<?php echo $list->website_desc?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="user_email" class="col-1 col-form-label">Email</label>
                                <div class="col-3">
                                    <input class="form-control" type="email" name="user_email" id="user_email" value="<?php echo $list->email?>">
                                </div>
                                <label for="website" class="col-1 col-form-label">website</label>
                                <div class="col-7">
                                    <input class="form-control" type="url" name="website" id="website" value="<?php echo $list->website?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="address" class="col-1 col-form-label">Address</label>
                                <div class="col-7">
                                    <textarea class="form-control" type="email" rows="4" name="address" id="address"><?php echo $list->address?></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-1">Enable</label>
                                <div class="col-sm-4">
                                    <div class="checkbox checkbox-primary">
                                        <input id="enable" name="enable" type="checkbox" value="1" <?php echo ($list->approval_disabled == 1)?'checked':'';?> >
                                        <label for="enable">Check me out</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-1">&nbsp;</label>
                                <input type="hidden" name="hddConfId" value="<?php echo $list->config_id?>">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </form>
                    </div>
                </div><!-- end col -->
            </div>
            <!-- end row -->
        </div> <!-- container -->
    </div> <!-- content -->

</div>
<!-- End content-page -->
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->
<?php echo $this->load->view('admin_layout/footer');?>
<script type="text/javascript">
$(document).ready( function() {
        $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function(event, label) {
            
            var input = $(this).parents('.input-group').find(':text'),
                log = label;
            
            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }
        
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();                
                reader.onload = function (e) {
                    $('#img-upload').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function(){
            readURL(this);
        });     
    });

</script>