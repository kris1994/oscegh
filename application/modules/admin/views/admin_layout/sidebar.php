<?php $this->load->model('admin/mdl_admin_sidebar','sidebar_model');?>
            <!-- ========== Left Sidebar Start ========== -->
            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">

                    <!--- Sidemenu -->
                    <div id="sidebar-menu">
                        <ul>
                        	<li class="text-muted menu-title">Navigation</li>
                            <?php
                            $get_main_menu = $this->mdl_general->GetAllInfo('gh_menutype', 'mt_id');
                            foreach ($get_main_menu as $m) {
                                if($this->session->userdata('asess_logged_in') == true){
                                    $menu=$this->sidebar_model->GetPermittedMenuItem($m['mt_id'],$this->session->userdata('asess_user_id'));
                                }else{
                                    $menu=$this->sidebar_model->GetPermittedMenuItem($m['mt_id'],21);
                                }
                            ?>
                            <?php if(count($menu) > 0){?>
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="<?php echo $m['menu_icon']?>"></i> <span> <?php echo $m['mt_name']?> </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <?php foreach($menu as $s){ ?>
                                    <li><a href="<?php echo base_url($m['mt_url'].'/'.$s['menu_url'])?>"><?php echo $s['menu_name']?></a></li>
                                    <?php }?>
                                </ul>
                            </li>
                            <?php }else{?>
                            <li class="has_sub">
                                <a href="<?php echo base_url($m['mt_url'])?>"" class="waves-effect"><i class="<?php echo $m['menu_icon']?>"></i><span><?php echo $m['mt_name']?></span> </a>
                            </li>
                            <?php }?>
                            <?php }?>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <!-- Sidebar -->
                    <div class="clearfix"></div>

                </div>

            </div>
            <!-- Left Sidebar End -->

