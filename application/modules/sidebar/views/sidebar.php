<div class="top-bar_w3agileits">
    <div class="top-logo_info_w3layouts">
        <div class="col-md-3 logo">
            <h1><a class="navbar-brand" href="<?php echo base_url('dashboard')?>"><?php echo $main_title->website_title?><span><?php echo $main_title->website_desc?></span></a></h1>
        </div>
        <div class="col-md-9 adrress_top">
            <!-- <div class="adrees_info">
                <div class="col-md-6 visit">
                    <div class="col-md-2 col-sm-2 col-xs-2 contact-icon">
                        <span class="fa fa-home" aria-hidden="true"></span>
                    </div>
                    <div class="col-md-10 col-sm-10 col-xs-10 contact-text">
                        <h4>Visit us</h4>
                        <p><?php echo $main_title->address?></p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-6 mail-us">
                    <div class="col-md-2 col-sm-2 col-xs-2 contact-icon">
                        <span class="fa fa-envelope" aria-hidden="true"></span>
                    </div>
                    <div class="col-md-10 col-sm-10 col-xs-10 contact-text">
                        <h4>Mail us</h4>
                        <p><a href="mailto:<?php echo $main_title->email?>"><?php echo $main_title->email?></a></p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div> -->
            <ul class="top-right-info_w3ls">
                <li><a href="#">&nbsp;</a></li>
                <!-- <li><a href="https://www.facebook.com/pg/GHGLOBALTRAININGCENTERCO/photos/?ref=page_internal" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li> -->
                <!-- <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li> -->
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="header-nav">
        <div class="inner-nav_wthree_agileits">
            <nav class="navbar navbar-default">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
                    <nav>
                        <ul class="nav navbar-nav">
                            <?php
                            $url = $this->uri->segment(1);
                            ?>
                            <li><a href="<?php echo base_url()?>" <?php echo empty($url)?'class="active"' : ''?> >Home</a></li>
                            <li><a href="<?php echo base_url('about')?>" <?php echo ($url=='about')?'class="active"' : ''?> >About</a></li>
                            <li><a href="<?php echo base_url('services')?>" <?php echo ($url=='services')?'class="active"' : ''?> >Services</a></li>
                            <!-- <li><a href="<?php echo base_url('evaluation')?>" <?php echo ($url=='evaluation')?'class="active"' : ''?> >Customer Evaluation</a></li> -->
                            <!-- <li class="dropdown">
                                <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">Services <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="javascript:void(0)">Services</a></li>
                                    <li class="divider"></li>
                                    <li><a href="javascript:void(0)">Other Services</a></li>
                                    <li class="divider"></li>
                                    <li><a class="s-menu" href="#">Other Services <b class="fa fa-caret-right"></b></a>
                                        <ul class="dropdown-menu sub-menu">
                                            <li><a href="javascript:void(0)">Accounting Services</a></li>
                                            <li class="divider"></li>
                                            <li><a href="elements.html">Bookkeeping Services</a></li>
                                            <li class="divider"></li>
                                            <li><a href="javascript:void(0)">Handling Labor Cases (NLRC/Dole)</a></li>
                                            <li class="divider"></li>
                                            <li><a href="javascript:void(0)">Tax Compliance</a></li>
                                            <li class="divider"></li>
                                            <li><a href="javascript:void(0)">Mayors/Business permit(New and Renewal)</a></li>
                                            <li class="divider"></li>
                                            <li><a href="javascript:void(0)">SEC/DTI</a></li>
                                            <li class="divider"></li>
                                            <li><a href="javascript:void(0)">External HR outsourcing</a></li>
                                        </ul>
                                    </li>

                                </ul>
                            </li> -->
                            <li class="dropdown">
                                <a href="javascript:void(0)" class="dropdown-toggle <?php echo ($url=='evaluation')?'active' : ''?>" data-toggle="dropdown">Evaluation <b class="caret"></b></a>
                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo base_url('evaluation')?>" <?php echo ($url=='evaluation')?'class="active"' : ''?>>Customer Evaluation</a></li>
                                    <li><a href="<?php echo base_url('view_evaluation')?>" <?php echo ($url=='view_evaluation')?'class="active"' : ''?>>View Evaluation</a></li>
                                    <li class="divider"></li>
                                </ul>
                            </li>
                            <li><a href="<?php echo base_url('contact_us')?>" <?php echo ($url=='contact_us')?'class="active"' : ''?>>Contact Us</a></li>
                            <?php if(!$this->session->userdata('sess_logged_in')){ ?>
                            <li><a href="<?php echo base_url('login')?>" <?php echo ($url=='login')?'class="active"' : ''?>>Login</a></li>
                            <?php }else{?>
                            <li><a href="<?php echo base_url('profile')?>" <?php echo ($url=='profile')?'class="active"' : ''?> >Profile</a></li>
                            <li><a href="<?php echo base_url('login/logout')?>" <?php echo ($url=='logout')?'class="active"' : ''?>>Logout</a></li>
                            <?php }?>
                        </ul>
                    </nav>

                </div>
            </nav>
            <div class="search">
                <div class="cd-main-header">
                   <!--  <ul class="cd-header-buttons">
                        <li><a class="cd-search-trigger" href="#cd-search"> <span></span></a></li>
                    </ul> -->
                    <!-- cd-header-buttons -->
                </div>
                <div id="cd-search" class="cd-search">
                    <form action="#" method="post">
                        <input name="Search" type="search" placeholder="Click enter after typing...">
                    </form>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>