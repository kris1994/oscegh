<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Mdl_extra extends CI_model{
	public function __construct() {
        parent::__construct();
    }
	public function getMenuList($where){
		if(!empty($where)){ $wheres = $where;
		}else{ $wheres = ''; }
		$query= $this->db->query(" SELECT * FROM `gh_menu` m LEFT JOIN gh_menutype mt ON m.mt_id = mt.mt_id $where ORDER BY m.menu_order
						")->result_array();
		return $query;

	}
	public function getUserRole($where){
		if(!empty($where)){ $wheres = $where;
		}else{ $wheres = ''; }
		$query= $this->db->query(" SELECT * FROM `gh_menu` m LEFT JOIN gh_userd ud ON m.menu_id = ud.menu_id $where GROUP BY m.menu_id
						 ORDER BY m.menu_id")->result_array();
		return $query;

	}
	public function getEvaluationList($where){
		if(!empty($where)){ $wheres = $where;
		}else{ $wheres = ''; }
		$query= $this->db->query(" SELECT * FROM `evaluation_list` el LEFT JOIN evaluation_type et ON el.et_id = et.et_id $where ORDER BY et.et_order, el.el_order ")->result_array();
		return $query;
	}
	public function getCountId($table=null, $field, $where=null, $group_by=null){
		$res = $this->db->query("SELECT $field $table $where $group_by")->row();
		return $res;
	}
	public function getAppointmentList($where=null){
		$res = $this->db->query("SELECT ga.*, s.services_name FROM `gh_appointment` ga
						 LEFT OUTER JOIN services s ON ga.a_services_type = s.services_id
						 $where")->result_array();
		return $res;
	}
}