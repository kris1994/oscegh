<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Services extends MX_Controller{

	function __construct() {
        parent::__construct();
        $this->load->model('mdl_general');
        $this->load->model('mdl_extra_services');
        $this->load->library('session');
       //  if($this->session->userdata('sess_logged_in')!=true){
    			// redirect('login/index?error=4');
       //  }
        
    }

    public function index(){
        $user_type = $this->session->userdata('sess_user_type');
        $main_title=$this->mdl_general->GetInfoByRow('gh_configuration','config_id',array('config_id'=>1));
    	$page_details['page_title']= $main_title->website_title.' | Services';
        $this->load->Module('header')->index($page_details);
        // $this->load->Module('carousel')->index();
        $data['list'] = $this->mdl_general->GetAllInfo('services','services_id',array('status'=>1,'services_type'=>'Other Services'));
        $data['list_Oservices'] = $this->mdl_general->GetAllInfo('services','services_id',array('status'=>1,'services_type'=>'Services'));
        $this->load->view('services',$data);
        $this->load->Module('footer')->index();
    
    }
    public function services_view($id=null){
        $user_type = $this->session->userdata('sess_user_type');
        $main_title=$this->mdl_general->GetInfoByRow('gh_configuration','config_id',array('config_id'=>1));
        $page_details['page_title']= $main_title->website_title.' | Services';
        $this->load->Module('header')->index($page_details);
        // $this->load->Module('carousel')->index();
        $data['getComments'] = $this->mdl_general->GetAllInfo('gh_comments','comment_id', array('services_id' => $id));
        $data['categ_list'] = $this->mdl_general->GetAllInfo('services','services_id',array('status'=>1));
        $data['list'] = $this->mdl_general->GetInfoByRow('services','services_id',array('services_id' => $id,'status'=>1));
        $u_id = $this->session->userdata('sess_user_id');
        $data['getUserLogDetails'] = $this->mdl_general->GetInfoByRow('gh_user','u_id',array('u_id' => $u_id));
        $data['Oservices'] = $this->mdl_general->GetAllInfo('services','services_id',array('status'=>1,'services_type'=>'Other Services'));
        $data['services_id_view'] = $id;
        $data['transIDGen'] = $this->getRandomTransactID($id);
        $this->load->view('services_view',$data);
        $this->load->Module('footer')->index();
    
    }
    public function getRandomTransactID($serv=null){
        $prefTrans = 'GH'.date('mdy').'_'.$serv.'_';
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $dataArray = array(); 
        $gen = array();
        $alpha_length = strlen($alphabet) - 1; 
        for ($i = 0; $i < 6; $i++) 
        {
            $n = rand(0, $alpha_length);
            $dataArray[] = $alphabet[$n];
        }

        return $prefTrans.implode($dataArray);
    }
    public function avail_services(){
        $payment_session = array();
        $form_data = array(
            'u_id' => $this->input->post('hddLogID'),
            'transaction_id' => $this->input->post('transaction_id'),
            'services_id' => $this->input->post('hddServicesID'),
            'amount' => $this->input->post('av_amount')
        );
        $res = $this->mdl_general->SaveForm('payment_list',$form_data);
        if($res){
            $payment_session = array(
                'sess_pay_u_id' => $this->input->post('hddLogID'),
                'sess_pay_trans_id' => $this->input->post('transaction_id'),
                'sess_pay_services_id' => $this->input->post('hddServicesID'),
                'sess_pay_amount' => $this->input->post('av_amount')
            );
            $this->session->set_userdata($payment_session);
            redirect('payumoney');
        }else{
            $payment_session = array(
                'sess_pay_u_id' => '',
                'sess_pay_trans_id' => '',
                'sess_pay_services_id' => '',
                'sess_pay_amount' => ''
            );
            $this->session->set_userdata($payment_session);
            redirect('services/services_view/'.$this->input->post('hddServicesID'));
        }

    }
    public function Lcomments(){
        $form_data = array(
            'comment_name' => $this->input->post('userName'),
            'comment_email' => $this->input->post('userEmail'),
            'comment_contact_no' => $this->input->post('userPhone'),
            'message' => $this->input->post('Message'),
            'services_id' => $this->input->post('hddServicesID')
        );
        $this->mdl_general->SaveForm('gh_comments',$form_data);
        $hddServicesID = $this->input->post('hddServicesID');
        redirect('services/services_view/'.$hddServicesID);
    }
    public function set_appointment_proc(){
        $form_data = array(
            'a_services_type' => $this->input->post('services_type'),
            'a_name' => $this->input->post('appointment_name'),
            'a_company' => $this->input->post('appointment_company'),
            'a_email' => $this->input->post('appointment_email'),
            'a_contact_no' => $this->input->post('appointment_contact_no'),
            'a_location_sel' => $this->input->post('location_rad'),
            'a_location' => $this->input->post('appointment_location'),
            'a_date' => $this->input->post('appointment_date'),
            'a_time' => $this->input->post('appointment_time'),
            'a_status' => 'pending'
        );
        $this->mdl_general->SaveForm('gh_appointment',$form_data);
        $response=array(
            'status'=>'success',
            'message'=>'Appointment Send successfully' 
        );
        echo json_encode($response);
    }
}